# sealflow 现代化工作流搭建框架

-   基于 [Warm-Flow国产工作流引擎](http://warm-flow.cn/) , 仿钉钉工作流， 纯个人爱好，开源分享， 请勿二开同类产品。
-   本项目很多地方只做了简单处理， 主要时间用于工作流程的功能开发
   
    
### 开发环境  

> <font size='4' color=gree>springboot 2.7.18 </font> ， 
> <font size='4' color=gree>java 17  </font>，
> <font size='4' color=gree>warm-flow  </font>，
> <font size='4' color=gree>satoken  </font>，
> <font size='4' color=gree>hutool  </font>，
> <font size='4' color=gree>mybatis-plus  </font>，
> <font size='4' color=gree>easy-trans </font>

    

### 功能介绍
- 首页 [-]
- 系统模块
  - 用户管理 [√]
  - 角色管理 [√]
  - 菜单管理 [√]
  - 部门管理 [√]
  - 职位管理 [√]
- oa办公
  - 发起申请 [√]
  - 我发起的 [√]
  - 待办任务 [√]
  - 已办任务 [√]
  - 抄送任务 [-]
- 流程管理
  - 流程分类 [√]
  - 流程设计 [乄]
    - 自定义表单 [√]
    - 表单设计器 [-]
    - 流程配置 [乄]
    - 流程发布 [√]
    - 版本管理 [-]

- 更多功能有序新增中...

### 使用方式
 后端： 
 > mysql 执行 seaflow.sql  
 > 修改application. yml 数据库连接信息  
 > 启动类SeaFlowRunApp.java  
 
 前端：  
>  yarn  
>  yarn dev

### 工作流解析
  > 工作流使用 warm-flow 框架，需要深入了解的同学，请先移步[官网](http://warm-flow.cn/)熟悉基本结构，下面为代码解析部分，如有错误请随时指正


#### 1. 流程逻辑
  流程设计 ->  流程json -> 转换为流程定义 -> 发布
#### 2. 发起流程申请逻辑

1. 提交申请流程
2. 找到开始节点 和 下一个审批节点（非网关）
3. 执行开始监听器
4. 创建流程实例对象
5. 执行权限监听器（官方不推荐使用了）
6. 创建历史任务对象
7. 创建下一个审批节点的task对象
8. 替换task中的办理人变量（表单数据变量替换）
9. 执行分派监听器(动态设置审批人的地方)
10. 保存（流程实例， 任务（根据参数permissionlist创建任务办理人），历史任务）
11. 执行结束监听器和下一个节点的创建监听器

#### 审批逻辑
 
  后续分析了再添加
 


### 前端代码

[https://gitee.com/qq75547276/seaflow](https://gitee.com/qq75547276/seaflow)
            
            
  交流群
   
  <img src="./qq.png">

 

