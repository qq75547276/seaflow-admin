package com.sykj.seaflow;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.sykj.seaflow.warm.core.convert2.NodeConvertHandler;
import org.dromara.warm.flow.core.entity.Node;

import java.util.List;

public class SimpleTest {
    public static void main(String[] args) {
        String json = "{\"nodeId\":\"801e3c73-e665-4f6f-8ea7-d4de0927214c\",\"nodeType\":\"start\",\"nodeName\":\"开始节点\",\"value\":\"所有人\",\"properties\":{\"buttons\":[{\"type\":\"aggren\",\"checked\":true,\"text\":\"同意\"},{\"type\":\"reject\",\"checked\":true,\"text\":\"撤销\"}],\"value\":\"所有人\"},\"childNode\":{\"nodeId\":\"f1c63412-5710-446d-a5ac-f441f37a22c2\",\"nodeName\":\"审批人1\",\"nodeType\":\"between\",\"value\":\"seven\",\"type\":\"between\",\"properties\":{\"nodeRatioType\":\"1\",\"backType\":\"1\",\"nodeRatio\":0,\"permissions\":[\"user\"],\"combination\":{\"role\":[],\"user\":[{\"id\":\"1814307624207470594\",\"name\":\"seven\"}]},\"emptyApprove\":{\"type\":\"AUTO\",\"value\":[]},\"value\":\"seven\",\"buttons\":[{\"type\":\"aggren\",\"checked\":true,\"text\":\"同意\"},{\"type\":\"reject\",\"checked\":true,\"text\":\"拒绝\"},{\"type\":\"back\",\"checked\":true,\"text\":\"回退\"},{\"type\":\"transfer\",\"checked\":false,\"text\":\"转办\"},{\"type\":\"depute\",\"checked\":false,\"text\":\"委派\"},{\"type\":\"signAdd\",\"checked\":false,\"text\":\"加签\"},{\"type\":\"signRedu\",\"checked\":false,\"text\":\"减签\"}],\"backTypeNode\":\"801e3c73-e665-4f6f-8ea7-d4de0927214c\"},\"childNode\":{\"nodeId\":\"3483a491-0765-4b54-8c35-dbacb29e5bbf\",\"nodeType\":\"serial\",\"nodeName\":\"分支选择\",\"conditionNodes\":[{\"nodeId\":\"b1d40308-9bab-4aaa-a1b0-7a362e24844f\",\"nodeType\":\"serial-node\",\"type\":\"serial-node\",\"nodeName\":\"条件\",\"sortNum\":0,\"value\":\"days 大于等于 3\",\"properties\":{\"conditions\":{\"simple\":true,\"group\":\"and\",\"simpleData\":[{\"key\":\"days\",\"cond\":\"ge\",\"value\":\"3\",\"next\":\"and\"}]},\"value\":\"days 大于等于 3\"},\"levelValue\":\"优先级1\",\"childNode\":{\"nodeId\":\"f36a09c6-08ab-4027-85f9-db1f8a55f57c\",\"nodeType\":\"parallel\",\"nodeName\":\"并行分支\",\"properties\":{},\"conditionNodes\":[{\"nodeId\":\"a4095f0e-1950-4d4c-b886-4a3035b645c2\",\"nodeName\":\"审批人1\",\"nodeType\":\"between\",\"type\":\"between\",\"properties\":{\"nodeRatioType\":\"1\",\"backType\":\"1\",\"nodeRatio\":0,\"permissions\":[\"user\"],\"combination\":{\"role\":[],\"user\":[{\"id\":\"1\",\"name\":\"超管\"}]},\"emptyApprove\":{\"type\":\"AUTO\",\"value\":[]},\"value\":\"超管\",\"buttons\":[{\"type\":\"aggren\",\"checked\":true,\"text\":\"同意\"},{\"type\":\"reject\",\"checked\":true,\"text\":\"拒绝\"},{\"type\":\"back\",\"checked\":false,\"text\":\"回退\"},{\"type\":\"transfer\",\"checked\":false,\"text\":\"转办\"},{\"type\":\"depute\",\"checked\":false,\"text\":\"委派\"},{\"type\":\"signAdd\",\"checked\":false,\"text\":\"加签\"},{\"type\":\"signRedu\",\"checked\":false,\"text\":\"减签\"}],\"backTypeNode\":\"801e3c73-e665-4f6f-8ea7-d4de0927214c\"},\"value\":\"超管\"},{\"nodeId\":\"091944fb-4f6f-4eeb-82d9-1ea3da22bd52\",\"nodeName\":\"审批人2\",\"nodeType\":\"between\",\"type\":\"between\",\"properties\":{\"nodeRatioType\":\"1\",\"backType\":\"1\",\"nodeRatio\":0,\"permissions\":[\"user\"],\"combination\":{\"role\":[],\"user\":[{\"id\":\"2\",\"name\":\"测试\"}]},\"emptyApprove\":{\"type\":\"AUTO\",\"value\":[]},\"value\":\"测试\",\"buttons\":[{\"type\":\"aggren\",\"checked\":true,\"text\":\"同意\"},{\"type\":\"reject\",\"checked\":true,\"text\":\"拒绝\"},{\"type\":\"back\",\"checked\":false,\"text\":\"回退\"},{\"type\":\"transfer\",\"checked\":false,\"text\":\"转办\"},{\"type\":\"depute\",\"checked\":false,\"text\":\"委派\"},{\"type\":\"signAdd\",\"checked\":false,\"text\":\"加签\"},{\"type\":\"signRedu\",\"checked\":false,\"text\":\"减签\"}],\"backTypeNode\":\"801e3c73-e665-4f6f-8ea7-d4de0927214c\"},\"value\":\"测试\"}]}},{\"nodeId\":\"1f678933-91b3-43c2-814c-26722a25d8f4\",\"nodeType\":\"serial-node\",\"type\":\"serial-node\",\"nodeName\":\"条件\",\"value\":\"其他条件默认走此流程\",\"sortNum\":1,\"default\":true,\"properties\":{},\"levelValue\":\"优先级2\",\"enableDestory\":true,\"childNode\":{\"nodeId\":\"32610c7f-21f1-43c5-821b-f65e3fcb917b\",\"nodeName\":\"审批人1\",\"nodeType\":\"between\",\"value\":\"测试\",\"type\":\"between\",\"properties\":{\"nodeRatioType\":\"1\",\"backType\":\"1\",\"nodeRatio\":0,\"permissions\":[\"user\"],\"combination\":{\"role\":[],\"user\":[{\"id\":\"2\",\"name\":\"测试\"}]},\"emptyApprove\":{\"type\":\"AUTO\",\"value\":[]},\"value\":\"测试\",\"buttons\":[{\"type\":\"aggren\",\"checked\":true,\"text\":\"同意\"},{\"type\":\"reject\",\"checked\":true,\"text\":\"拒绝\"},{\"type\":\"back\",\"checked\":false,\"text\":\"回退\"},{\"type\":\"transfer\",\"checked\":false,\"text\":\"转办\"},{\"type\":\"depute\",\"checked\":false,\"text\":\"委派\"},{\"type\":\"signAdd\",\"checked\":false,\"text\":\"加签\"},{\"type\":\"signRedu\",\"checked\":false,\"text\":\"减签\"}],\"backTypeNode\":\"801e3c73-e665-4f6f-8ea7-d4de0927214c\"}}}],\"childNode\":{\"nodeId\":\"fa13aeeb-f794-43c8-a2aa-27da340b9696\",\"nodeType\":\"end\",\"nodeName\":\"结束\",\"properties\":{}}}}}" ;
        JSONObject jsonNode = JSONUtil.parseObj(json);
        String endNodeId = getEndNodeId(jsonNode);
        System.out.printf("endNodeId=%s\n", endNodeId);
        List<Node> convert = NodeConvertHandler.convert(null, jsonNode, null, endNodeId, endNodeId);
        System.out.printf(JSONUtil.toJsonStr(convert));


    }

   public static String getEndNodeId(JSONObject jsonNode){
       JSONObject childNode = jsonNode.getJSONObject("childNode");
       if(childNode != null){
           return getEndNodeId(childNode);
       }
       return jsonNode.getStr("nodeId");
   }
}
