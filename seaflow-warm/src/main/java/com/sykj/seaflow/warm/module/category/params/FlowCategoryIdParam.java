package com.sykj.seaflow.warm.module.category.params;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class FlowCategoryIdParam {

    @NotNull(message = "id不能为空")
    private Long id;
}
