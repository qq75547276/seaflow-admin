package com.sykj.seaflow.warm.module.task.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlowTaskQueryParam {
    private String flowName;
    private String flowCode;

    private String initiatorName;


}
