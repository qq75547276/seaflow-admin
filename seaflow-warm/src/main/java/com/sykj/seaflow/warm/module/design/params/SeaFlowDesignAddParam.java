package com.sykj.seaflow.warm.module.design.params;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Setter
@Getter
public class SeaFlowDesignAddParam {

    /**
     * 流程code
     */
    @NotEmpty(message = "流程code不能为空")
    private String flowCode;
    /**
     * 流程名称
     */
    @NotEmpty(message = "流程名称不能为空")
    private String flowName;
    /**
     * 流程版本号
     */
//    private String flowVersion;
    /**
     * 分类id
     */
    @NotEmpty(message = "业务分类不能为空")
    private String categoryId;
    /**
     * 流程定义id
     */
    private String defId;


    private String icon;
    /**
     * 表单是否自定义 Y是，N否
     */
    private String formCustom;
    /**
     * 表单路径
     */
    private String formPath;
//    private String flowJson;

    /**
     * 流程管理员
     */
    @NotEmpty(message = "流程管理员不能为空")
    private String managerId;
}
