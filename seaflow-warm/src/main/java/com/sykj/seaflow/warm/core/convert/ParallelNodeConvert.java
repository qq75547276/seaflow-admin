package com.sykj.seaflow.warm.core.convert;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener;
import com.sykj.seaflow.warm.core.listener.GlobalCreateListener;
import com.sykj.seaflow.warm.module.def.bean.SeaFlowNode;
import org.dromara.warm.flow.core.entity.Node;
import org.dromara.warm.flow.core.entity.Skip;
import org.dromara.warm.flow.core.enums.NodeType;
import org.dromara.warm.flow.core.enums.SkipType;
import org.dromara.warm.flow.core.listener.Listener;
import org.dromara.warm.flow.orm.entity.FlowSkip;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ParallelNodeConvert extends NodeConvertAbstract{

    @Override
    public List<Node> convert(JSONObject jsonObject, String startNodeId, String endNodeId, String nextNodeId){
        // 并行网关，会在子节点 最前面加一个条件分支， 否则会出问题
        List<Node> seaflowNodeList = new ArrayList<>();
        String startParallelNodeId = jsonObject.getStr("nodeId");
        SeaFlowNode serialNode = new SeaFlowNode();
        serialNode.setNodeCode(startParallelNodeId);
        serialNode.setNodeType(NodeType.PARALLEL.getKey());
        serialNode.setDirection("1");
        //加一个并行网关
        seaflowNodeList.add(serialNode);




        //前
//        seaflowNode startNode = getNode(jsonObject);
        // 后
        SeaFlowNode endNode = getNode(jsonObject);
        endNode.setNodeCode(UUID.randomUUID().toString());

//        seaflowNodeList.add(startNode);
        JSONArray childNodes = jsonObject.getJSONArray("childNodes");
        List<Skip>  startSkips = new ArrayList<>();
        for (int j = 0; j < childNodes.size(); j++) {
            JSONArray jsonArray = childNodes.getJSONArray(j);
            jsonArray.getJSONObject(0).set("nodeType", NodeType.BETWEEN.getValue());
            // 跳转 不变
            FlowSkip startNodeSkip = new FlowSkip();
            startNodeSkip.setSkipType(SkipType.PASS.getKey());
            // 表示所有条件分支都执行
//            String expressionType = "@@pass@@|true";
//            startNodeSkip.setSkipCondition(expressionType);
            startNodeSkip.setNextNodeCode(jsonArray.getJSONObject(0).getStr("nodeId"));
            startSkips.add(startNodeSkip);

            List<Node> convert = NodeConvertHandler.convertold(jsonArray, startNodeId, endNodeId);
            //  子节点的最后一个 还要处理下连线问题
            Node seaflowNode = convert.get(convert.size() - 1);
            FlowSkip flowSkip = new FlowSkip();
            flowSkip.setSkipType(SkipType.PASS.getKey());
            flowSkip.setNextNodeCode(endNode.getNodeCode());
            seaflowNode.getSkipList().add(flowSkip);

            seaflowNodeList.addAll(convert);

        }
        serialNode.setSkipList(startSkips);

        String emptyNodeCode  = UUID.randomUUID().toString();
        //并行网关（出） 连接空节点
        FlowSkip parallelOutSkip = new FlowSkip();
        parallelOutSkip.setSkipType(SkipType.PASS.getKey());
        parallelOutSkip.setNextNodeCode(emptyNodeCode);
//        endNode.setSkipList(List.of(parallelOutSkip));
        endNode.setSkipList(Arrays.asList(parallelOutSkip));

        // 并行网关（出） 后面再加一个空节点
        SeaFlowNode emptyNode = new SeaFlowNode();
        emptyNode.setNodeName("自动通过");
        emptyNode.setNodeCode(emptyNodeCode);
        emptyNode.setNodeType(NodeType.BETWEEN.getKey());
        // 设置空类别自动通过， 区分前端审批人为空的配置
        String emptyApprove = "{\"type\":\"EMPTY\",\"value\":[]}";
        emptyNode.setEmptyApprove(emptyApprove);
        // 多个用逗号分割， 这里先写死
        emptyNode.setListenerType(String.join(",", Listener.LISTENER_ASSIGNMENT, Listener.LISTENER_CREATE));
        emptyNode.setListenerPath(String.join("@@", GlobalAssignmentListener.class.getName(),
                GlobalCreateListener.class.getName()));



        if(nextNodeId != null){
            List<Skip> endSkipList = new ArrayList<>();
            for (String nodeId : nextNodeId.split(",")) {
                FlowSkip endSkip = new FlowSkip();
                endSkip.setSkipType(SkipType.PASS.getKey());
                endSkip.setNextNodeCode(nodeId);
                endSkipList.add(endSkip);

            }
            emptyNode.setSkipList(endSkipList);

        }


        seaflowNodeList.add(endNode);
        seaflowNodeList.add(emptyNode);
        return seaflowNodeList;
    }

    @Override
    public String getType() {
        return "parallel";
    }
}
