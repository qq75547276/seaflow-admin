package com.sykj.seaflow.warm.module.nodeExt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sykj.seaflow.warm.module.nodeExt.entity.FlowNodeExt;

public interface FlowNodeExtService extends IService<FlowNodeExt> {

    public FlowNodeExt getNodeExt(FlowNodeExt flowNodeExt);
}
