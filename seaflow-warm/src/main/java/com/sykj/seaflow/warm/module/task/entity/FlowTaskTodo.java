package com.sykj.seaflow.warm.module.task.entity;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.sykj.seaflow.warm.module.instance.entity.SeaFlowUser;
import lombok.Getter;
import lombok.Setter;
import org.noear.snack.ONode;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class FlowTaskTodo {
    /**
     * 任务id
     */
    private Long id;
    /**
     * 流程编码
     */
    private String flowCode;

    /**
     * 流程名称
     */
    private String flowName;

    /**
     * 流程版本
     */
    private String version;
    /**
     * 流程实例当前节点
     */
    private String currNodeName;

    private Long instanceId;

    private Long definitionId;

    /**
     * 待办任务节点名称
     */
    private String nodeName;

    /**
     * 任务开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 发起人名称
     */
    private String initiatorName;
    /**
     * 发起人id
     */
    private String initiatorId;
    /**
     * 流程实例状态
     */
    private Integer flowStatus;


    private String formPath;

    private String currFormPath;

    private String formCustom;

    private String variable;

    private String flowJson;

    private String formJson;

    private String fieldSetting;

    private Map<String, Object> formData;

    private String buttonJson;


    public String getFormPath(){
        return ObjectUtil.isEmpty(currFormPath) ? formPath : currFormPath;
    }

    // 转为list
    public JSONArray getBtnList () {
        if(ObjectUtil.isNotEmpty(buttonJson)){
            return JSONUtil.parseArray(buttonJson);
        }
        return null;
    };


    public Map<String, Object> getFormData() {
        if (ObjectUtil.isNotEmpty(variable)) {
//            formData =  ONode.deserialize(variable);
            Map<String,Object >  data =  ONode.deserialize(variable);
            formData = (Map<String, Object>) data.get("formData");
        }
        return formData;
    }

    /**
     * 办理人列表
     */
    private List<SeaFlowUser> assigneeList;

    public String getAssigneeStr() {
        if (ObjectUtil.isNotEmpty(assigneeList)) {
            StringBuilder sb = new StringBuilder();
            for (SeaFlowUser seaflowUser : assigneeList) {
                if(ObjectUtil.isNotEmpty(seaflowUser.getName())){
                    sb.append(seaflowUser.getName()).append(",");
                }else{
                    sb.append(seaflowUser.getId()).append(", ");
                }

            }
            return sb.substring(0, sb.length() - 1);
        }
        return null;
    }
}

