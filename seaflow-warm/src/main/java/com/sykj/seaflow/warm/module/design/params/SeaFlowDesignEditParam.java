package com.sykj.seaflow.warm.module.design.params;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SeaFlowDesignEditParam {

    private Long id;
    /**
     * 流程code
     */
    private String flowCode;
    /**
     * 流程名称
     */
    private String flowName;
    /**
     * 流程版本号
     */
//    private String flowVersion;
    /**
     * 分类id
     */
    private String categoryId;
    /**
     * 流程定义id
     */
    private String defId;


    private String icon;

    /**
     * 表单是否自定义 Y是，N否
     */
    private String formCustom;
    /**
     * 表单路径
     */
    private String formPath;
//    private String flowJson;

    /**
     * 流程管理员
     */
    private String managerId;
}
