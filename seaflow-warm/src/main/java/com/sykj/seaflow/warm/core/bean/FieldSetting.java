package com.sykj.seaflow.warm.core.bean;

import lombok.Data;

@Data
public class FieldSetting {
    private String label;
    private String field;
    private Boolean required;
    private String permission;  // 1：编辑 2 只读 3 隐藏

}

