package com.sykj.seaflow.warm.module.design.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.constant.TransType;
import com.fhs.core.trans.vo.TransPojo;
import lombok.Getter;
import lombok.Setter;
import com.sykj.seaflow.common.base.entity.BaseEntity;
import com.sykj.seaflow.warm.module.category.entity.FlowCategory;

@Getter
@Setter
@TableName("flow_design")
public class SeaFlowDesign extends BaseEntity implements TransPojo {
    /**
     * 流程code
     */
    private String flowCode;
    /**
     * 流程名称
     */
    private String flowName;
    /**
     * 流程版本号
     */
    private String flowVersion;
    /**
     * 分类id
     */
    @Trans(type = TransType.SIMPLE, target = FlowCategory.class, fields = "name", ref = "categoryName")
    private Long categoryId;

    /**
     * 分类名称
     */
    @TableField(exist = false)
    private String categoryName;
    /**
     * 流程定义id
     */
    private Long defId;

    /**
     * 图标
     */
    private String icon;

    /**
     * 流程json
     */
    private String flowJson;

    /**
     * 表单json配置
     */
    private String formJson;
    /**
     * 状态 1正常，0 停用
     */
    private String status;

    private Integer sortNum;

    /**
     * 表单是否自定义 Y是，N否
     */
    private String formCustom;
    /**
     * 表单路径
     */
    private String formPath;

    private Long managerId;

    @TableField(exist = false)
    private String managerName;


//    private String fieldSetting;
}
