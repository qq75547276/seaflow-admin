package com.sykj.seaflow.warm.core.handler;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.sykj.seaflow.warm.core.constant.FlowConstant;
import com.sykj.seaflow.warm.module.nodeExt.entity.FlowNodeExt;
import org.dromara.warm.flow.core.dto.FlowParams;
import org.dromara.warm.flow.core.entity.Node;
import org.dromara.warm.flow.core.entity.Task;
import org.dromara.warm.flow.core.enums.FlowStatus;
import org.dromara.warm.flow.core.enums.SkipType;
import org.dromara.warm.flow.core.enums.UserType;
import org.dromara.warm.flow.core.listener.ListenerVariable;
import org.dromara.warm.flow.core.service.TaskService;
import org.dromara.warm.flow.core.service.UserService;
import lombok.AllArgsConstructor;
import com.sykj.seaflow.common.base.exception.CommonException;
import com.sykj.seaflow.warm.module.def.entity.FlowDefExt;
import com.sykj.seaflow.warm.module.def.service.FlowDefExtService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class EmptyApproveHandler {
    public static final String AUTO = "AUTO";
    public static final String USER = "USER";
    public static final String REJECT = "REJECT";
    public static final String MANAGER = "MANAGER";
    public static final String EMPTY = "EMPTY";


    private final FlowDefExtService flowDefExtService;
    private final TaskService taskService;
    private UserService userService;

    /**
     * 审批人为空处理
     *
     * @param nodeExt 审批人为空的配置项
     */
    public void handle(FlowNodeExt nodeExt, ListenerVariable variable, Task task) {
        String emptyApproveJson = nodeExt.getEmptyApprove();
        JSONObject emptyApprove = JSONUtil.parseObj(emptyApproveJson);
        Node node = variable.getNode();
        String type = emptyApprove.getStr("type");
        //  清理 上个分支节点 的变量 ， （变量存不存在都清理， 不做判断了）
        variable.getVariable().remove(FlowConstant.SERIAL_NODE_BEFORE_COMPLETED);
        switch (type) {
            case AUTO:
            {
                taskService.skip(task.getId(), FlowParams.build().handler("0").flowStatus(FlowStatus.AUTO_PASS.getKey()).variable(variable.getVariable()).skipType(SkipType.PASS.getKey()).message(SkipType.PASS.getKey()));
            }
            break;
            case EMPTY:
            {
                taskService.skip(task.getId(), FlowParams.build().handler("-1").variable(variable.getVariable()).skipType(SkipType.PASS.getKey()).message(SkipType.PASS.getKey()));
            }
            break;
            case USER: {
                JSONArray jsonArray = emptyApprove.getJSONArray("value");
                List<String> nodePermissionList = jsonArray.stream().map(r -> ((JSONObject) r).getStr("id")).collect(Collectors.toList());  //.toList();

//                associated – 关联人id
//                permissions – 权限人
//                type – 权限人类型
//                clear – 是否清空代办任务的计划审批人
//                handler – 存储委派时的办理人
                userService.updatePermission(task.getId(), nodePermissionList, UserType.APPROVAL.getKey(), true, null);
            }
            break;
            case REJECT:
                taskService.skip(task.getId(), FlowParams.build().handler("-1").variable(variable.getVariable()).skipType(SkipType.REJECT.getKey()).message(SkipType.REJECT.getKey()));
                break;
            case MANAGER: {
                FlowDefExt flowDefExt = flowDefExtService.getById(node.getDefinitionId());
                Long managerId = flowDefExt.getManagerId();
                userService.updatePermission(task.getId(), Collections.singletonList(String.valueOf(managerId)),
                        UserType.APPROVAL.getKey(), true, null);
            }
            break;
            default:
                throw new CommonException("审批人为空配置异常， 不支持的类型：{}", type);

        }

    }

    ;


}

