package com.sykj.seaflow.warm.module.task.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.warm.module.task.entity.*;
import com.sykj.seaflow.warm.module.task.params.*;
import org.dromara.warm.flow.orm.entity.FlowTask;

import java.util.List;

public interface FlowTaskService {
    /**
     * 待办
     * @param queryParam
     * @return
     */
//    public IPage<Task> todoPage(FlowTaskQueryParam queryParam, PageQuery<FlowTask> pageQuery);

    /**
     * 待办
     * @param pageQuery
     * @param flowTaskQueryParam
     * @return
     */
    public IPage<FlowTaskTodo> todoPage(PageQuery<FlowTask> pageQuery, FlowTaskQueryParam flowTaskQueryParam);


    /**
     * 办理
     */
    public void skip(FlowSkipParam flowSkipParam);

    /**
     * 我已办的
     * @param pageQuery
     * @param flowDoneTaskQueryParam
     * @return
     */
    IPage<FlowTaskDone> donePage(PageQuery<FlowTask> pageQuery, FlowDoneTaskQueryParam flowDoneTaskQueryParam);

    /**
     * 我发起的
     * @param pageQuery
     * @param flowMyTaskQueryParam
     * @return
     */
    IPage<FlowTaskMy> myPage(PageQuery<FlowTask> pageQuery, FlowMyTaskQueryParam flowMyTaskQueryParam);

    List<SeaFlowHisTask> hisList(FlowHisTaskQueryParam flowHisTaskQueryParam);

    void back(FlowSkipParam flowSkipParam);

    /**
     * 转办
     * @param flowSkipParam
     */
    void transfer(FlowSkipParam flowSkipParam);

    /**
     * 委派
     * @param flowSkipParam
     */
    void depute(FlowSkipParam flowSkipParam);

    /**
     * 加签
     * @param flowSkipParam
     */
    void addSignature(FlowSkipParam flowSkipParam);

    /**
     * 减签
     * @param flowSkipParam
     */
    void reduSignature(FlowSkipParam flowSkipParam);

    /**
     * 当前任务的待办人
     * @param FlowSkipParam
     * @return
     */
    List<FlowTaskUser> users(FlowSkipParam FlowSkipParam);

    List<FlowTaskUser> selectUsers(FlowTaskUserQueryParam flowSkipParam);


    public String flowStatus(Long instId, String flowJson);
}
