package com.sykj.seaflow.warm.module.def.bean;

import org.dromara.warm.flow.orm.entity.FlowNode;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class SeaFlowNode extends FlowNode {


    /**
     * 审批人员为空时的配置
     */
    private String emptyApprove;

    /**
     * 回退类型
     */
    private String backType ;

    /**
     * 回退节点
     */
    private String backTypeNode;

    /**
     * 入
     */
    private String direction;


    private String buttonJson;


    private boolean isLast = false;

    /**
     * 字段设置（主要用于设计表单字段的显示只读隐藏配置）
     */
    private String fieldSetting;

}
