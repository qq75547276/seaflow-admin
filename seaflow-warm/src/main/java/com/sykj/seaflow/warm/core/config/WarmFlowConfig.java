package com.sykj.seaflow.warm.core.config;

import com.sykj.seaflow.warm.core.condition.*;
import com.sykj.seaflow.warm.ext.dao.SeaFlowHisTaskDaoImpl;
import org.dromara.warm.flow.core.dao.FlowHisTaskDao;
import org.dromara.warm.flow.core.utils.ExpressionUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class WarmFlowConfig implements InitializingBean {

    @Bean
    @Primary
    public FlowHisTaskDao flowHisTaskDao() {
        return new SeaFlowHisTaskDaoImpl();
    }

    /**
     * 自定义填充 （可配置文件注入，也可用@Bean/@Component方式）
     */
//    @Bean
//    public DataFillHandler dataFillHandler() {
//        return new CustomDataFillHandler();
//    }

    /**
     * mybatis plus 注入了
     * 全局租户处理器（可配置文件注入，也可用@Bean/@Component方式）
     */
//    @Bean
//    public TenantHandler tenantHandler() {
//        return new CustomTenantHandler();
//    }


    @Override
    public void afterPropertiesSet() throws Exception {
        ConditionStrategySimpleAnd expressionStrategySimpleAnd = new ConditionStrategySimpleAnd();
        ConditionStrategySimpleOr expressionStrategySimpleOr = new ConditionStrategySimpleOr();
        ConditionStrategyDefault expressionStrategyDefault = new ConditionStrategyDefault();
        ConditionStrategyReject expressionStrategyReject = new ConditionStrategyReject();
        ConditionStrategyPass expressionStrategyPass = new ConditionStrategyPass();
        ExpressionUtil.setExpression(expressionStrategySimpleAnd);
        ExpressionUtil.setExpression(expressionStrategySimpleOr);
        ExpressionUtil.setExpression(expressionStrategyDefault);
        ExpressionUtil.setExpression(expressionStrategyReject);
        ExpressionUtil.setExpression(expressionStrategyPass);
    }
}
