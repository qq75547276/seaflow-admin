package com.sykj.seaflow.warm.module.category.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.common.base.exception.CommonException;
import com.sykj.seaflow.warm.module.category.params.FlowCategoryAddParam;
import com.sykj.seaflow.warm.module.category.params.FlowCategoryEditParam;
import com.sykj.seaflow.warm.module.category.params.FlowCategoryIdParam;
import com.sykj.seaflow.warm.module.category.service.FlowCategoryService;
import com.sykj.seaflow.warm.module.category.entity.FlowCategory;
import com.sykj.seaflow.warm.module.category.mapper.FlowCategoryMapper;
import com.sykj.seaflow.warm.module.category.params.FlowCategoryPageParam;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 75547
* @description 针对表【flow_category(流程分类)】的数据库操作Service实现
* @createDate 2024-07-07 09:08:10
*/
@Service
public class FlowCategoryServiceImpl extends ServiceImpl<FlowCategoryMapper, FlowCategory>


implements FlowCategoryService {
    @Override
    public void add(FlowCategoryAddParam flowCategoryAddParam) {
        FlowCategory flowCategory = BeanUtil.copyProperties(flowCategoryAddParam, FlowCategory.class);
        long count = count(Wrappers.<FlowCategory>lambdaQuery().eq(FlowCategory::getCode, flowCategoryAddParam.getCode()));
        Assert.isTrue(count >= 0,"分类编码重复");
        this.getBaseMapper().insert(flowCategory);
    }

    @Override
    public void edit(FlowCategoryEditParam flowCategoryEditParam) {
        FlowCategory flowCategory = BeanUtil.copyProperties(flowCategoryEditParam, FlowCategory.class);
        long count = count(Wrappers.<FlowCategory>lambdaQuery()
                .eq(FlowCategory::getCode, flowCategoryEditParam.getCode())
                .ne(FlowCategory::getId, flowCategoryEditParam.getId()));
        Assert.isTrue(count >= 0,"分类编码重复");
        this.getBaseMapper().updateById(flowCategory);
    }

    @Override
    public void del(FlowCategoryIdParam flowCategoryIdParam) {
        this.getBaseMapper().deleteById(flowCategoryIdParam.getId());
    }

    @Override
    public void delBatch(List<FlowCategoryIdParam> flowCategoryIdParams) {
        List<Long> ids = CollectionUtil.map(flowCategoryIdParams, FlowCategoryIdParam::getId, true);
        this.getBaseMapper().deleteBatchIds(ids);
    }

    @Override
    public Page<FlowCategory> page(PageQuery pageQuery, FlowCategoryPageParam flowCategoryPageParam) {
        QueryWrapper<FlowCategory> queryWrapper =  buildQueryWrap(flowCategoryPageParam);
        queryWrapper.lambda().orderByAsc(FlowCategory::getSortNum);
        return this.page(pageQuery.getPage(), queryWrapper);
    }

    @Override
    public List<FlowCategory> list(FlowCategoryPageParam flowCategoryPageParam) {
        return this.list(buildQueryWrap(flowCategoryPageParam));
    }

    private QueryWrapper<FlowCategory> buildQueryWrap(FlowCategoryPageParam flowCategoryPageParam) {
        QueryWrapper<FlowCategory> query = Wrappers.query();
        query.lambda()
                .like(ObjectUtil.isNotEmpty(flowCategoryPageParam.getCode()), FlowCategory::getCode, flowCategoryPageParam.getCode())
                .like(ObjectUtil.isNotEmpty(flowCategoryPageParam.getName()), FlowCategory::getName, flowCategoryPageParam.getName());
        return query;
    }

}




