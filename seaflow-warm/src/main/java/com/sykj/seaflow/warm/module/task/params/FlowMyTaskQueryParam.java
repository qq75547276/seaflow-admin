package com.sykj.seaflow.warm.module.task.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlowMyTaskQueryParam {
    private String flowName;
    //流程发起人
    private String initiatorName;

    private String flowCode;


}
