package com.sykj.seaflow.warm.module.task.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * 任务节点办理人
 */
@Getter
@Setter
public class FlowTaskUser {
    /**
     * 用户id
     */
    private Long id;

    /**
     * 用户名称
     */
    private String nick;

    private String username;


    private String phone;
}
