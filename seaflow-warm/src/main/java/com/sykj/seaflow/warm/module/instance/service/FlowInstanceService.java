package com.sykj.seaflow.warm.module.instance.service;


import com.sykj.seaflow.warm.module.instance.entity.SeaFlowUser;
import com.sykj.seaflow.warm.module.instance.params.FlowInstanceStartParam;

import java.util.List;


public interface FlowInstanceService {

    /**
     * 开启流程
     * @param flowInstanceStartParam
     */
    public void start(FlowInstanceStartParam flowInstanceStartParam);

    public List<SeaFlowUser> approveList(List<Long> instanceIds);
}
