package com.sykj.seaflow.warm.module.task.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sykj.seaflow.warm.module.task.entity.FlowTaskDone;
import com.sykj.seaflow.warm.module.task.entity.FlowTaskMy;
import com.sykj.seaflow.warm.module.task.entity.FlowTaskTodo;
import org.apache.ibatis.annotations.Param;

public interface SeaFlowTaskMapper  {

    public IPage<FlowTaskTodo> todoPage(IPage page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    IPage<FlowTaskDone> donePage(IPage page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    IPage<FlowTaskMy> myPage(IPage page, @Param(Constants.WRAPPER) QueryWrapper query);
}
