package com.sykj.seaflow.warm.module.task.controller;


import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.common.base.bean.Result;
import com.sykj.seaflow.warm.module.task.entity.*;
import com.sykj.seaflow.warm.module.task.params.*;
import com.sykj.seaflow.warm.module.task.service.FlowTaskService;
import org.dromara.warm.flow.core.enums.SkipType;
import org.dromara.warm.flow.orm.entity.FlowTask;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@RestController
@RequestMapping("/flow/task")
public class FlowTaskController {
    private final FlowTaskService flowTaskService;

    /**
     * 待办
     * @param pageQuery
     * @param flowTaskQueryParam
     * @return
     */
    @GetMapping("/todo")
    public Result<FlowTaskTodo> todoPage(PageQuery<FlowTask> pageQuery, FlowTaskQueryParam flowTaskQueryParam) {
        return Result.build(flowTaskService.todoPage(pageQuery, flowTaskQueryParam));
    }

    @GetMapping("/done")
    public Result<FlowTaskDone> donePage(PageQuery<FlowTask> pageQuery, FlowDoneTaskQueryParam flowDoneTaskQueryParam) {
        return Result.build(flowTaskService.donePage(pageQuery, flowDoneTaskQueryParam));
    }

    @GetMapping("/my")
    public Result<FlowTaskMy> myPage(PageQuery<FlowTask> pageQuery, FlowMyTaskQueryParam flowMyTaskQueryParam) {
        return Result.build(flowTaskService.myPage(pageQuery, flowMyTaskQueryParam));
    }

    /**
     * 办理
     * @param flowSkipParam
     * @return
     */
    @PostMapping("/skip")
    public Result<String> skip(@RequestBody FlowSkipParam flowSkipParam) {
        Map<String, Object> variable = new HashMap<>();
        variable.put("formData", flowSkipParam.getVariable());
        flowSkipParam.setVariable(variable);
        flowTaskService.skip(flowSkipParam);
        return Result.build();
    }
    @PostMapping("/pass")
    public Result<String> pass(@RequestBody FlowSkipParam flowSkipParam) {
        Map<String, Object> variable = new HashMap<>();
        variable.put("formData", flowSkipParam.getVariable());
        flowSkipParam.setVariable(variable);
        flowTaskService.skip(flowSkipParam);
        return Result.build();
    }
    @PostMapping("/reject")
    public Result<String> reject(@RequestBody FlowSkipParam flowSkipParam) {
        flowSkipParam.setSkipType(SkipType.REJECT.getKey());
        Map<String, Object> variable = new HashMap<>();
        variable.put("_of_reject", "1");
        variable.put("formData", flowSkipParam.getVariable());
        flowSkipParam.setVariable(variable);
        flowSkipParam.setFlowStatus("99");
        flowTaskService.skip(flowSkipParam);
        return Result.build();
    }

    @PostMapping("/back")
    public Result<String> back(@RequestBody FlowSkipParam flowSkipParam) {

        Map<String, Object> variable = new HashMap<>();
        variable.put("_of_reject", "2");
        variable.put("formData", flowSkipParam.getVariable());
        flowSkipParam.setVariable(variable);
        flowTaskService.back(flowSkipParam);
        return Result.build();
    }


    /**
     *转办
     * @param flowSkipParam
     * @return
     */
    @PostMapping("/transfer")
    public Result<String> transfer(@RequestBody FlowSkipParam flowSkipParam) {
        flowTaskService.transfer(flowSkipParam);
        return Result.build();
    }

    /**
     * 委派
     * @param flowSkipParam
     * @return
     */
    @PostMapping("/depute")
    public Result<String> depute(@RequestBody FlowSkipParam flowSkipParam) {
        flowTaskService.depute(flowSkipParam);
        return Result.build();
    }
    /**
     * 加签
     * @param flowSkipParam
     * @return
     */
    @PostMapping("/addSign")
    public Result<String> addSignature(@RequestBody FlowSkipParam flowSkipParam) {
        flowTaskService.addSignature(flowSkipParam);
        return Result.build();
    }
    /**
     * 减签
     * @param flowSkipParam
     * @return
     */
    @PostMapping("/reduSign")
    public Result<String> reductionSignature(@RequestBody FlowSkipParam flowSkipParam) {
        flowTaskService.skip(flowSkipParam);
        return Result.build();
    }


    @GetMapping("/hisList")
    public Result<SeaFlowHisTask> hisList(FlowHisTaskQueryParam flowHisTaskQueryParam) {
        return Result.build(flowTaskService.hisList(flowHisTaskQueryParam));
    }

    @GetMapping("/users")
    public Result<FlowTaskUser> users(FlowSkipParam flowSkipParam) {
        return Result.build(flowTaskService.users(flowSkipParam));
    }

    @GetMapping("/selectUsers")
    public Result<FlowTaskUser> selectUsers(FlowTaskUserQueryParam flowSkipParam) {
        return Result.build(flowTaskService.selectUsers(flowSkipParam));
    }
}
