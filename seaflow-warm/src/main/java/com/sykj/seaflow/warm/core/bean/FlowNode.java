package com.sykj.seaflow.warm.core.bean;

import cn.hutool.json.JSONObject;

public class FlowNode {


    // 父节点
    private JSONObject parentNode;


    // 当前节点
    private JSONObject currNode;


    // 上个节点
    private JSONObject preNode;

    // 下个节点
    private JSONObject nextNode;


}
