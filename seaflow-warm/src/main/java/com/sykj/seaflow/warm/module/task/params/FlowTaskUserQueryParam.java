package com.sykj.seaflow.warm.module.task.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlowTaskUserQueryParam {

    private Long taskId;

    private String nick;

    private String username;

    private String phone;
    //...
}
