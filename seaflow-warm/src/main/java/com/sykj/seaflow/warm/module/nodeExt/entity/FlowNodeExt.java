package com.sykj.seaflow.warm.module.nodeExt.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import com.sykj.seaflow.common.base.entity.BaseEntity;

@Setter
@Getter
@TableName("flow_node_ext")
public class FlowNodeExt extends BaseEntity {


    private String nodeCode;

    private Long defId;
    /**
     * 审批人为空时的配置 jsonstr
     */
    private String emptyApprove;

    private Integer nodeType;
    @TableField(fill = FieldFill.INSERT)
    @TableLogic
    private String delFlag;
    /**
     * 回退类型
     */
    private String backType ;

    /**
     * 回退节点
     */
    private String backTypeNode;
    /**
     * 并行网关才有 入:1 表示进入网关, 出暂未设置
     */
    private String direction;
    /**
     * 节点的按钮， 前端使用
     */
    private String buttonJson;
    /**
     *  设计表单对应的字段编辑只读隐藏设置
     */
    private String fieldSetting;

}
