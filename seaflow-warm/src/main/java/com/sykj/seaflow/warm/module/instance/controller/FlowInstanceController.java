package com.sykj.seaflow.warm.module.instance.controller;

import com.sykj.seaflow.warm.module.instance.params.FlowInstanceStartParam;
import lombok.AllArgsConstructor;
import com.sykj.seaflow.common.base.bean.Result;
import com.sykj.seaflow.warm.module.instance.service.FlowInstanceService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/flow/instance")
public class FlowInstanceController {
    private final FlowInstanceService instanceService;

    @PostMapping("/start")
    public Result<String> start(@RequestBody FlowInstanceStartParam FlowInstanceStartParam){
        instanceService.start(FlowInstanceStartParam);
        return Result.success("流程发起成功");
    }
}
