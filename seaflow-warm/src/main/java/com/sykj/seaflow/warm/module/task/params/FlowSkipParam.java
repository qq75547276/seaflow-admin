package com.sykj.seaflow.warm.module.task.params;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Setter
@Getter
public class FlowSkipParam {
    /**
     * 任务id
     */
    private Long id;
    /**
     * 审批动作
     */
    private String skipType;
    /**
     * 审批意见
     */
    private String message;
    /**
     * 流程变量 具体看如何做
     */
    private Map<String, Object> variable;

    /**
     * 办理人， 转办， 加签 减签
     */
    private List<Long> handlers;

    private String flowStatus;

}
