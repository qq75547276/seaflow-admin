package com.sykj.seaflow.warm.module.def.params;


import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SeaFlowDefinitionIdParam {
    @NotNull(message = "id不能为空")
    private Long id;
}
