package com.sykj.seaflow.warm.core.convert;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener;
import com.sykj.seaflow.warm.core.listener.GlobalCreateListener;
import com.sykj.seaflow.warm.module.def.bean.SeaFlowNode;
import org.dromara.warm.flow.core.entity.Node;
import org.dromara.warm.flow.core.entity.Skip;
import org.dromara.warm.flow.core.enums.NodeType;
import org.dromara.warm.flow.core.enums.SkipType;
import org.dromara.warm.flow.core.listener.Listener;
import org.dromara.warm.flow.orm.entity.FlowSkip;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SerialNodeConvert extends NodeConvertAbstract{


    @Override
    public List<Node> convert(JSONObject jsonObject, String startNodeId, String endNodeId, String nextNodeId){

        List<Node> seaflowNodeList = new ArrayList<>();
        SeaFlowNode node = getNode(jsonObject);
        seaflowNodeList.add(node);
        List<Skip> serialSkip = new ArrayList<>();
        // 获取子节点
        JSONArray childNodes = jsonObject.getJSONArray("childNodes");
        // childNodes是两层数组
        for (int j = 0; j < childNodes.size(); j++) {
            JSONArray jsonArray = childNodes.getJSONArray(j);
            // 跳转节点

            if(jsonArray.size() > 1){


                String  subNextNodeId = jsonArray.getJSONObject(1).getStr("nodeId");
                String  subNextNodeType = jsonArray.getJSONObject(1).getStr("nodeType");
                // 子节点直接连的还是网关节点，需要加一个node
                if(NodeType.isGateWay(NodeType.getKeyByValue(subNextNodeType))){
                    String emptyNodeCode = UUID.randomUUID().toString();
                    SeaFlowNode emptyNode = new SeaFlowNode();
                    emptyNode.setNodeCode(emptyNodeCode);
                    emptyNode.setNodeType(NodeType.BETWEEN.getKey());
                    // 设置空类别自动通过， 区分前端审批人为空的配置
                    String emptyApprove = "{\"type\":\"EMPTY\",\"value\":[]}";
                    emptyNode.setEmptyApprove(emptyApprove);
                    // 多个用逗号分割， 这里先写死
                    emptyNode.setListenerType(String.join(",", Listener.LISTENER_ASSIGNMENT, Listener.LISTENER_CREATE));
                    emptyNode.setListenerPath(String.join("@@", GlobalAssignmentListener.class.getName(),
                            GlobalCreateListener.class.getName()));

                    List<Skip> emptySkip = new ArrayList<>();
                    Skip emSkip = new FlowSkip();
                    emSkip.setSkipType(SkipType.PASS.getKey());
                    emSkip.setNextNodeCode(subNextNodeId);
                    emptySkip.add(emSkip);
                    emptyNode.setSkipList(emptySkip);
                    seaflowNodeList.add(emptyNode);

                    FlowSkip flowSkip =  createSkip(jsonArray.getJSONObject(0), emptyNodeCode);
                    serialSkip.add(flowSkip);
                    // 替换
//                    subNextNodeId = emptyNodeCode;
                }else{
                    FlowSkip flowSkip =  createSkip(jsonArray.getJSONObject(0), subNextNodeId);
                    serialSkip.add(flowSkip);
                }


                // 去掉第一个
                JSONArray subArray = new JSONArray(jsonArray.subList(1, jsonArray.size()));
                List<Node> convert = NodeConvertHandler.convertold(subArray, startNodeId, endNodeId);
                // 如果是每个子节点的最后一个 跳转到父节点的下个节点
                Node seaflowNode = convert.get(convert.size() - 1);

                List<Skip> skipList = seaflowNode.getSkipList();

                String[] nodeIds = nextNodeId.split(",");
                for(String nodeId : nodeIds){
                    //  设置跳转
                    FlowSkip skipParentNext = new FlowSkip();
                    skipParentNext.setSkipType(SkipType.PASS.getKey());
                    skipParentNext.setNextNodeCode(nodeId);
                    skipList.add(skipParentNext);
                }


                seaflowNodeList.addAll(convert);
            }else {
                String emptyNodeCode = UUID.randomUUID().toString();

                // 加个空任务, 多个网关直连走不通
                SeaFlowNode emptyNode = new SeaFlowNode();
                emptyNode.setNodeName("自动通过");
                emptyNode.setNodeCode(emptyNodeCode);
                emptyNode.setNodeType(NodeType.BETWEEN.getKey());
                // 设置空类别自动通过， 区分前端审批人为空的配置
                String emptyApprove = "{\"type\":\"EMPTY\",\"value\":[]}";
                emptyNode.setEmptyApprove(emptyApprove);
                // 多个用逗号分割， 这里先写死
                emptyNode.setListenerType(String.join(",", Listener.LISTENER_ASSIGNMENT, Listener.LISTENER_CREATE));
                emptyNode.setListenerPath(String.join("@@", GlobalAssignmentListener.class.getName(),
                        GlobalCreateListener.class.getName()));

                // 节点跳转
                String[] nodeIds = nextNodeId.split(",");
                List<Skip> emptySkip = new ArrayList<>();
                for(String nodeId : nodeIds){
                    //  设置跳转
                    FlowSkip skipParentNext = new FlowSkip();
                    skipParentNext.setSkipType(SkipType.PASS.getKey());
                    skipParentNext.setNextNodeCode(nodeId);
                    emptySkip.add(skipParentNext);
                }
                emptyNode.setSkipList(emptySkip);
                seaflowNodeList.add(emptyNode);

                //  分支网关  连到空网关上
                FlowSkip flowSkip =  createSkip(jsonArray.getJSONObject(0), emptyNodeCode);
                serialSkip.add(flowSkip);

            }

        }
        node.setSkipList(serialSkip);
        return seaflowNodeList;
    }

    private FlowSkip createSkip(JSONObject subNode, String nextNodeId) {
        //default ：默认跳转配置， 只有条件分支才有
        FlowSkip skip = new FlowSkip();
        Boolean defaultSerial = subNode.getBool("default");
        if(defaultSerial != null && defaultSerial){
            // 设置表单时类别 简单模式 或 复杂模式
            String expressionType = "@@default@@|true";
            skip.setSkipCondition( expressionType );
            skip.setSkipName(subNode.getStr("nodeName"));
            skip.setSkipType(SkipType.PASS.getKey());
            skip.setNextNodeCode(nextNodeId);

        }else{
            //设置跳转
            JSONObject config = subNode.getJSONObject("config");
            JSONObject conditions = config.getJSONObject("conditions");
            // 只有条件分支才有conditions
            if(conditions != null){
                Boolean simple = conditions.getBool("simple");
                String group = conditions.getStr("group");

                String simpleData = conditions.getStr("simpleData");
                // 设置表单时类别 简单模式 或 复杂模式
                String expressionType = "@@" +  (simple? "simple_"+ group : "complex") + "@@|";
                skip.setSkipCondition( expressionType +  simpleData );
                skip.setSkipName(subNode.getStr("nodeName"));
                skip.setSkipType(SkipType.PASS.getKey());
                skip.setNextNodeCode(nextNodeId);

            }
        }
        return skip;

    }

    @Override
    public String getType() {
        return "serial";
    }
}
