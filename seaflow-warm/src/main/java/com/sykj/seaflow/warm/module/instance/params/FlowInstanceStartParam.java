package com.sykj.seaflow.warm.module.instance.params;

import com.sykj.seaflow.sys.remote.bean.LoginUserPosition;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 *  流程启动参数
 */
@Getter
@Setter
public class FlowInstanceStartParam {
    /**
     * 流程定义id
     */
    private String defId;

    /**
     *  表单数据
     */
    private Map<String, Object> formData;

    // 职位 部门信息
    private LoginUserPosition userPosition;
    
}
