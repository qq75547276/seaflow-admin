package com.sykj.seaflow.warm.core.convert;

import cn.hutool.json.JSONObject;
import org.dromara.warm.flow.core.entity.Node;

import java.util.List;

public interface NodeConvert {

    /**
     *  允许返回多个node
     * @param node
     * @param startNodeId
     * @param endNodeId
     * @param nextNodeId
     * @return
     */
    public List<Node> convert(JSONObject node, String startNodeId, String endNodeId, String nextNodeId);


    public String getType();

}
