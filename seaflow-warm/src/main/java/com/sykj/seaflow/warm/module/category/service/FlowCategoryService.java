package com.sykj.seaflow.warm.module.category.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.warm.module.category.params.FlowCategoryAddParam;
import com.sykj.seaflow.warm.module.category.params.FlowCategoryEditParam;
import com.sykj.seaflow.warm.module.category.params.FlowCategoryIdParam;
import com.sykj.seaflow.warm.module.category.entity.FlowCategory;
import com.sykj.seaflow.warm.module.category.params.FlowCategoryPageParam;

import java.util.List;

/**
* @author 75547
* @description 针对表【flow_category(流程分类)】的数据库操作Service
* @createDate 2024-07-07 09:08:10
*/
public interface FlowCategoryService extends IService<FlowCategory> {

    void add(FlowCategoryAddParam flowCategoryAddParam);

    void edit(FlowCategoryEditParam flowCategoryEditParam);

    void del(FlowCategoryIdParam flowCategoryIdParam);

    void delBatch(List<FlowCategoryIdParam> flowCategoryIdParams);

    Page<FlowCategory> page(PageQuery pageQuery, FlowCategoryPageParam flowCategoryPageParam);

    List<FlowCategory> list(FlowCategoryPageParam flowCategoryPageParam);
}

