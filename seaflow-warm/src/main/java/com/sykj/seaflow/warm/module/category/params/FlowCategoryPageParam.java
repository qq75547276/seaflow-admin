package com.sykj.seaflow.warm.module.category.params;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlowCategoryPageParam {

    private String name;

    private String code;

    private Integer sortNum;
}
