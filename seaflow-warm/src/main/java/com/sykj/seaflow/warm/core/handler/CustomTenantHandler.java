package com.sykj.seaflow.warm.core.handler;

import org.dromara.warm.flow.core.handler.TenantHandler;
import com.sykj.seaflow.common.base.holder.TenantContextHolder;

public class CustomTenantHandler implements TenantHandler {
    @Override
    public String getTenantId() {
        Long tenantId = TenantContextHolder.getTenantId();
        return  String.valueOf(tenantId);
    }
}
