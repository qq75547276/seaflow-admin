package com.sykj.seaflow.warm.module.def.params;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 流程配置
 */
@Data
public class SeaFlowDefConfigParam {
    /**
     * 流程定义id
     */
    @NotNull(message = "id不能为空")
    private Long id;

    /**
     * json数据
     */
    @NotNull(message = "流程配置数据不能为空")
    private String flowJosn;

    /**
     * 需要再添加
     */
    private String other;
}

