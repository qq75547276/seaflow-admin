package com.sykj.seaflow.warm.module.def.entity;

import com.sykj.seaflow.warm.module.category.entity.FlowCategory;
import com.sykj.seaflow.warm.module.design.entity.SeaFlowDesign;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
@AllArgsConstructor
@Getter
@Setter
public class FlowApproval {

    private FlowCategory flowCategory;

    private List<SeaFlowDesign> definitionList;
}
