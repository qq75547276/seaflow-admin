package com.sykj.seaflow.warm.module.task.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlowHisTaskQueryParam {
    /**
     * 流程实例id
     */
    private Long instId;
}

