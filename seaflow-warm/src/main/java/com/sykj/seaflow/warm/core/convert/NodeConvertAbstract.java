package com.sykj.seaflow.warm.core.convert;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener;
import com.sykj.seaflow.warm.core.listener.GlobalCreateListener;
import com.sykj.seaflow.warm.module.def.bean.SeaFlowNode;
import org.dromara.warm.flow.core.enums.NodeType;
import org.dromara.warm.flow.core.listener.Listener;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public abstract  class NodeConvertAbstract implements NodeConvert {


    public SeaFlowNode getNode(JSONObject jsonObject) {
        SeaFlowNode node1 = new SeaFlowNode();
        // 节点类别
        String nodeType = jsonObject.getStr("nodeType");
        // nodeId 作为code
        String nodeCode = jsonObject.getStr("nodeId");
        String nodeName = jsonObject.getStr("nodeName");

        //权限相关 额外配置项
        JSONObject config = jsonObject.getJSONObject("config");
        if(ObjectUtil.isNotEmpty(config)){
            String permission = convertPermissionMap(config);
            node1.setPermissionFlag(permission);
            BigDecimal nodeRatio = config.getBigDecimal("nodeRatio");
            // 多个用逗号分割， 这里先写死
            node1.setListenerType(String.join(",", Listener.LISTENER_ASSIGNMENT, Listener.LISTENER_CREATE));
            node1.setListenerPath(String.join("@@", GlobalAssignmentListener.class.getName(),
                    GlobalCreateListener.class.getName()));
            // 审批为空配置信息,  在 GlobalCreateListener中判断为空处理
            String emptyApprove = config.getStr("emptyApprove");
            node1.setEmptyApprove(emptyApprove);
            // 按钮配置
            node1.setButtonJson(config.getStr("buttons"));
        }

        node1.setNodeType(NodeType.getKeyByValue(nodeType));
        node1.setNodeCode(nodeCode);
        node1.setNodeName(nodeName);
        return node1;
    }


    private SeaFlowNode getEmptyNode(String emptyNodeCode) {
        SeaFlowNode emptyNode = new SeaFlowNode();
        emptyNode.setNodeCode(emptyNodeCode);
        emptyNode.setNodeType(NodeType.BETWEEN.getKey());
        // 设置空类别自动通过， 区分前端审批人为空的配置
        String emptyApprove = "{\"type\":\"EMPTY\",\"value\":[]}";
        emptyNode.setEmptyApprove(emptyApprove);
        // 多个用逗号分割， 这里先写死
        emptyNode.setListenerType(String.join(",", Listener.LISTENER_ASSIGNMENT, Listener.LISTENER_CREATE));
        emptyNode.setListenerPath(String.join("@@", GlobalAssignmentListener.class.getName(),
                GlobalCreateListener.class.getName()));
        return emptyNode;
    }

    private static String convertPermissionMap(JSONObject config) {
        String roleCode= "role";
        String userCode= "user";
        String permission = "";
        // 设置权限  role, user 等
        JSONArray permissions = config.getJSONArray("permissions");
        // 权限组合
        JSONObject combination = config.getJSONObject("combination");
        if(ObjectUtil.isNull(permissions)){
            return null;
        }
        List<String> list = permissions.stream().map(String::valueOf).collect(Collectors.toList());//.toList();
        if(ObjectUtil.isEmpty(list)){
            return null;
        }
        if(list.contains(roleCode)){
            JSONArray jsonArray = combination.getJSONArray(roleCode);
            List<String> roleIds = jsonArray.stream().map(r -> ((JSONObject) r).getStr("id")).collect(Collectors.toList());//.toList();
            if(ObjectUtil.isNotEmpty(roleIds)){
                String collect = roleIds.stream().collect(Collectors.joining(","));
                permission += "role:" + collect + "@@";
            }
        }

        if(list.contains(userCode)){
            JSONArray jsonArray = combination.getJSONArray(userCode);
            List<String> roleIds = jsonArray.stream().map(r -> ((JSONObject) r).getStr("id")).collect(Collectors.toList());// .toList();
            if(ObjectUtil.isNotEmpty(roleIds)){
                String collect = roleIds.stream().collect(Collectors.joining(","));
                permission += "user:" + collect + "@@";
            }
        }
        if(!permission.isEmpty()) {
            permission = permission.substring(0, permission.length() - 2);
        }
        return permission;
    }
}
