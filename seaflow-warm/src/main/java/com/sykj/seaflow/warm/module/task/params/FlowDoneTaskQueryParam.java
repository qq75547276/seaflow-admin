package com.sykj.seaflow.warm.module.task.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlowDoneTaskQueryParam {
    private String flowName;

    private String flowCode;

    private String initiatorName;


}
