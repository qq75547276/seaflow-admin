package com.sykj.seaflow.warm.core.convert2;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener;
import com.sykj.seaflow.warm.core.listener.GlobalCreateListener;
import com.sykj.seaflow.warm.module.def.bean.SeaFlowNode;
import org.dromara.warm.flow.core.constant.FlowCons;
import org.dromara.warm.flow.core.entity.Node;
import org.dromara.warm.flow.core.entity.Skip;
import org.dromara.warm.flow.core.enums.NodeType;
import org.dromara.warm.flow.core.enums.SkipType;
import org.dromara.warm.flow.core.listener.Listener;
import org.dromara.warm.flow.orm.entity.FlowSkip;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class StartNodeConvert extends NodeConvertAbstract {
    @Override
    public List<Node> convert(SeaFlowNode parentNode, JSONObject node, String startNodeId, String endNodeId, String lastLineNodeId) {
        // 节点
        SeaFlowNode startNode = getNode(node);
        // 第一个节点不能回退，所以后面要加一个between节点， 发起申请的时候再次办理between节点
        startNode.setNodeCode(UUID.randomUUID().toString());
        startNode.setNodeName("开始节点");


        // 设置发起人为自己
        startNode.setPermissionFlag(FlowCons.WARMFLOWINITIATOR);

        JSONObject childNode = node.getJSONObject("childNode");
        // nextNode的id ,如果子节点不存在，就连接结束节点
        String startEndNodeID = childNode == null? endNodeId: childNode.getStr("nodeId");


        startNode.setListenerType(String.join(",", Listener.LISTENER_ASSIGNMENT, Listener.LISTENER_CREATE));
        startNode.setListenerPath(String.join("@@", GlobalAssignmentListener.class.getName(),
                GlobalCreateListener.class.getName()));


        JSONObject config = node.getJSONObject("properties");
        // 开始节点和 审批节点默认一样吧
        startNode.setFieldSetting(config.getStr("fieldSetting"));


        // 跳转
        FlowSkip flowSkip = new FlowSkip();
        flowSkip.setNowNodeCode(startNode.getNodeCode());
        flowSkip.setSkipType(SkipType.PASS.getKey());
        // 跳转到开始节点，这里的开始节点id被between节点使用
        flowSkip.setNextNodeCode(startNodeId);

        List<Skip> skipList = new ArrayList<>();
        skipList.add(flowSkip);
        startNode.setSkipList(skipList);




        SeaFlowNode seaFlowNode = new SeaFlowNode();
        seaFlowNode.setNodeCode(startNodeId);
        seaFlowNode.setNodeType(NodeType.BETWEEN.getKey());
        seaFlowNode.setNodeName("发起申请");
        seaFlowNode.setPermissionFlag(FlowCons.WARMFLOWINITIATOR);
        seaFlowNode.setListenerType(Listener.LISTENER_ASSIGNMENT);
        seaFlowNode.setListenerPath(GlobalAssignmentListener.class.getName());
        //权限相关 额外配置项

        seaFlowNode.setButtonJson(config.getStr("buttons"));

        // 表单路径
        String formPath = config.getStr("formPath");
        if(ObjectUtil.isNotEmpty(formPath)){
            seaFlowNode.setFormCustom("y");
            seaFlowNode.setFormPath(formPath);
            startNode.setFormPath(formPath);
        }
        // 监听器
        JSONArray jsonArray = config.getJSONArray("listeners");
        StringBuilder listenerType = new StringBuilder(String.join(",", Listener.LISTENER_ASSIGNMENT, Listener.LISTENER_CREATE));
        StringBuilder listenerPath = new StringBuilder(String.join("@@", GlobalAssignmentListener.class.getName(),
                GlobalCreateListener.class.getName()));
        if(jsonArray != null && jsonArray.size() > 0){
            for (int i = 0; i < jsonArray.size(); i++) {
                listenerType.append(",").append(jsonArray.get(i, JSONObject.class).getStr("listenerType"));
                listenerPath.append(FlowCons.splitAt).append(jsonArray.get(i, JSONObject.class).getStr("listenerPath"));
            }
        }

        seaFlowNode.setFieldSetting(config.getStr("fieldSetting"));
        seaFlowNode.setListenerType(listenerType.toString());
        seaFlowNode.setListenerPath(listenerPath.toString());

        FlowSkip flowSkip1 = new FlowSkip();
        flowSkip1.setSkipType(SkipType.PASS.getKey());
        flowSkip1.setNextNodeCode(startEndNodeID);
        seaFlowNode.getSkipList().add(flowSkip1);

        FlowSkip rejectSkip = new FlowSkip();
        rejectSkip.setSkipType(SkipType.REJECT.getKey());
        rejectSkip.setNextNodeCode(endNodeId);

        seaFlowNode.getSkipList().add(rejectSkip);


        List<Node>  seaFlowNodes = nextNode(seaFlowNode, childNode, startNodeId, endNodeId,lastLineNodeId);
        seaFlowNodes.add(startNode);
        seaFlowNodes.add(seaFlowNode);
        return seaFlowNodes;  //List.of(seaflowNode);
    }

    @Override
    public String getType() {
        return "start";
    }


    private SeaFlowNode rejectNode(String rejectNode, String endNodeCode){

        SeaFlowNode serialNode = new SeaFlowNode();
        serialNode.setNodeCode(rejectNode);
        serialNode.setNodeRatio(BigDecimal.valueOf(0));
        serialNode.setNodeType(NodeType.SERIAL.getKey());

        List<Skip> skipList = new ArrayList<>();
        FlowSkip skipReject = new FlowSkip();
        skipReject.setSkipCondition( "@@reject@@|_of_reject@@eq@@1");
        skipReject.setSkipType(SkipType.REJECT.getKey());
        skipReject.setNextNodeCode(endNodeCode);
        skipList.add(skipReject);

        serialNode.setSkipList(skipList);
        return serialNode;
    }
}
