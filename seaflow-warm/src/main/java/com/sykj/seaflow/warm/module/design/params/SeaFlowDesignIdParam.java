package com.sykj.seaflow.warm.module.design.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SeaFlowDesignIdParam {
    private Long id;
}
