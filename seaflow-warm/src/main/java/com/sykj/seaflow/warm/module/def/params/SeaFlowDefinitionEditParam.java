package com.sykj.seaflow.warm.module.def.params;

import org.dromara.warm.flow.core.entity.Node;
import org.dromara.warm.flow.core.entity.User;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Data
public class SeaFlowDefinitionEditParam {
    @NotNull(message = "id不能为空")
    private Long id;

    /**
     * 流程编码
     */
    @NotNull(message = "流程编码不能为空")
    private String flowCode;

    /**
     * 流程名称
     */
    @NotNull(message = "流程名称不能为空")
    private String flowName;

    /**
     * 流程版本
     */
    private String version;

    /**
     * 审批表单是否自定义（Y是 2否）
     */
    private String formCustom;

    /**
     * 审批表单是否自定义（Y是 2否）
     */
    private String formPath;

}
