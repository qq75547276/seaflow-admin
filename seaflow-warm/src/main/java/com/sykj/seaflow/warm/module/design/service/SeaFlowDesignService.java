package com.sykj.seaflow.warm.module.design.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.warm.module.design.entity.SeaFlowDesign;
import com.sykj.seaflow.warm.module.design.params.*;

import java.util.List;

public interface SeaFlowDesignService extends IService<SeaFlowDesign>  {

    public void add(SeaFlowDesignAddParam addParam);


    public void edit(SeaFlowDesignEditParam editParam);

    public void del(List<SeaFlowDesignIdParam> idParam);


    public SeaFlowDesign get(SeaFlowDesignIdParam idParam);

    public IPage<SeaFlowDesign> page(SeaFlowDesignPageParam pageParam, PageQuery<SeaFlowDesign> pageQuery);

    public List<SeaFlowDesign> canUseFlow(SeaFlowDesignPageParam pageParam);

    public void publish(SeaFlowDesignIdParam idParam);

    public void unPublish(SeaFlowDesignIdParam idParam);

    void saveConfig(SeaFlowDesignConfigParam configParam);
}
