package com.sykj.seaflow.warm.module.instance.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlowInstanceIdParam {
    private Long defId;

}
