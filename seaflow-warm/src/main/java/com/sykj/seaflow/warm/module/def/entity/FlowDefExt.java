package com.sykj.seaflow.warm.module.def.entity;

import lombok.Getter;
import lombok.Setter;
import com.sykj.seaflow.common.base.entity.BaseEntity;

@Getter
@Setter
public class FlowDefExt extends BaseEntity {

    private String flowJson;

    private String formJson;

    /**
     * 管理员id
     */
    private Long managerId;
    /**
     * 流程设计id
     */
    private Long designId;



}
