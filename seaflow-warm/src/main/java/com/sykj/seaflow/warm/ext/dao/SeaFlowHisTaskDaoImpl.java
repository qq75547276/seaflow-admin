package com.sykj.seaflow.warm.ext.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.enums.SqlKeyword;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Mapper;
import org.dromara.warm.flow.core.enums.NodeType;
import org.dromara.warm.flow.core.orm.agent.WarmQuery;
import org.dromara.warm.flow.core.utils.ObjectUtil;
import org.dromara.warm.flow.core.utils.StringUtils;
import org.dromara.warm.flow.orm.dao.FlowHisTaskDaoImpl;
import org.dromara.warm.flow.orm.dao.FlowTaskDaoImpl;
import org.dromara.warm.flow.orm.entity.FlowHisTask;
import org.dromara.warm.flow.orm.entity.FlowTask;

import java.util.List;

public class SeaFlowHisTaskDaoImpl extends FlowHisTaskDaoImpl {


    @Override
    public List<FlowHisTask> selectList(FlowHisTask  entity, WarmQuery<FlowHisTask> query) {
        QueryWrapper<FlowHisTask> queryWrapper = new QueryWrapper(entity);
        if (ObjectUtil.isNotNull(query)) {
            queryWrapper.orderBy(StringUtils.isNotEmpty(query.getOrderBy()), query.getIsAsc().equals(SqlKeyword.ASC.getSqlSegment()), query.getOrderBy());
        }
        queryWrapper.lambda().ne(FlowHisTask::getApprover, "-1").ne(FlowHisTask::getNodeType, NodeType.START.getKey());
        return this.getMapper().selectList(queryWrapper);
    }

}
