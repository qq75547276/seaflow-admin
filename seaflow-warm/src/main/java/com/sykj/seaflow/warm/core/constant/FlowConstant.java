package com.sykj.seaflow.warm.core.constant;

public class FlowConstant {

    /**
     * 分支节点， 前置条件是否完成。   warm-flow 每个分支节点都会走， 这个参数表示 分支节点执行时判断前一个分支是否通过
     */
    public final static String SERIAL_NODE_BEFORE_COMPLETED = "_of_serial_node_before_completed";

    // 拒绝条件
    public final static String REJECT_CONDITION = "@@reject@@|_of_reject@@eq@@1";

    // 回退条件
    public final static String BACK_CONDITION = "@@reject@@|_of_reject@@eq@@2";
}
