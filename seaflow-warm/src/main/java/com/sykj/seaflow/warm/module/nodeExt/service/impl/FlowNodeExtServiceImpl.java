package com.sykj.seaflow.warm.module.nodeExt.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sykj.seaflow.warm.module.nodeExt.entity.FlowNodeExt;
import com.sykj.seaflow.warm.module.nodeExt.mapper.FlowNodeExtMapper;
import com.sykj.seaflow.warm.module.nodeExt.service.FlowNodeExtService;
import lombok.AllArgsConstructor;
import com.sykj.seaflow.common.base.exception.CommonException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FlowNodeExtServiceImpl extends ServiceImpl<FlowNodeExtMapper, FlowNodeExt> implements FlowNodeExtService
{


    @Override
    public FlowNodeExt getNodeExt(FlowNodeExt flowNodeExt) {
        if(ObjectUtil.isNull(flowNodeExt.getDefId()) || ObjectUtil.isNull(flowNodeExt.getNodeCode())){
            throw new CommonException("获取流程节点扩展信息异常： defId:{}, nodeCode:{}",flowNodeExt.getDefId(), flowNodeExt.getNodeCode() );
        }
        return this.getOne(Wrappers.<FlowNodeExt>lambdaQuery().eq(FlowNodeExt::getDefId, flowNodeExt.getDefId())
                .eq(FlowNodeExt::getNodeCode, flowNodeExt.getNodeCode()));
    }
}
