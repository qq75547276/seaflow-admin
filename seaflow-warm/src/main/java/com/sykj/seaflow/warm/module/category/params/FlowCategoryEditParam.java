package com.sykj.seaflow.warm.module.category.params;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class FlowCategoryEditParam {
    @NotNull(message = "id不能为空")
    private Long id;

    @NotNull(message = "名称不能为空")
    private String name;

    @NotNull(message = "编码不能为空")
    private String code;

    private Integer sortNum;

    @NotNull(message = "图标不能为空")
    private String icon;
}
