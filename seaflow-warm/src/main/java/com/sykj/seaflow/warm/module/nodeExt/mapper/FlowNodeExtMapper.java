package com.sykj.seaflow.warm.module.nodeExt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sykj.seaflow.warm.module.nodeExt.entity.FlowNodeExt;

public interface FlowNodeExtMapper extends BaseMapper<FlowNodeExt> {
}
