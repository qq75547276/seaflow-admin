package com.sykj.seaflow.warm.module.instance.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sykj.seaflow.warm.module.instance.entity.SeaFlowUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SeaFlowInstanceMapper {


    public List<SeaFlowUser> approveList(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);
}
