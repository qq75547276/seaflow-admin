package com.sykj.seaflow.warm.core.convert;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import com.sykj.seaflow.warm.module.def.bean.SeaFlowNode;
import org.dromara.warm.flow.core.entity.Node;
import org.dromara.warm.flow.core.entity.Skip;
import org.dromara.warm.flow.core.enums.NodeType;
import org.dromara.warm.flow.core.enums.SkipType;
import org.dromara.warm.flow.orm.entity.FlowNode;
import org.dromara.warm.flow.orm.entity.FlowSkip;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class BetweenNodeConvert extends NodeConvertAbstract{
    @Override
    public List<Node> convert(JSONObject jsonObject, String startNodeId, String endNodeId, String nextNodeId) {
        SeaFlowNode node = getNode(jsonObject);
        JSONObject config = jsonObject.getJSONObject("config");
        node.setBackType(config.getStr("backType"));
        node.setBackTypeNode(config.getStr("backTypeNode"));
        List<Skip> skipList = new ArrayList<>();
        if(ObjectUtil.isNotEmpty(nextNodeId)){

            String[] nodeIds = nextNodeId.split(",");
            for(String nodeId : nodeIds){
                //  设置跳转
                FlowSkip flowSkip = new FlowSkip();
                flowSkip.setNowNodeCode(node.getNodeCode());
                flowSkip.setNextNodeCode(nodeId);
                flowSkip.setSkipType(SkipType.PASS.getKey());
                skipList.add(flowSkip);
            }

        }




        // 未通过时跳转
//        String serialNodeCode = UUID.randomUUID().toString();
//        FlowSkip rejectSkip = new FlowSkip();
//        rejectSkip.setNowNodeCode(node.getNodeCode());
//        rejectSkip.setNextNodeCode(serialNodeCode);
//        rejectSkip.setSkipType(SkipType.REJECT.getKey());
//        skipList.add(rejectSkip);
//        node.setSkipList(skipList);
//
//
//        // 创建分支 用于跳转
//        seaflowNode serialNode = new seaflowNode();
//        serialNode.setNodeCode(serialNodeCode);
//        serialNode.setNodeRatio(BigDecimal.valueOf(0));
//        serialNode.setNodeType(NodeType.SERIAL.getKey());

        //  拒绝和回退处理
        // 一个跳转到开始 ，一个跳转到结束
//        FlowSkip skipReject = new FlowSkip();
//        skipReject.setSkipCondition( "@@reject@@|_of_reject@@eq@@1");
//        skipReject.setSkipType(SkipType.REJECT.getKey());
//        skipReject.setNextNodeCode(endNodeId);

//        FlowSkip skipBack = new FlowSkip();
//        skipBack.setSkipCondition( "@@reject@@|_of_reject@@eq@@2");
//        skipBack.setSkipType(SkipType.REJECT.getKey());
//        skipBack.setNextNodeCode(startNodeId);

//        serialNode.setSkipList(List.of(skipReject,skipBack));
        String str = config.getStr("backType");
        if(ObjectUtil.equal(str, "1")){
            FlowSkip skipBack = new FlowSkip();
            skipBack.setSkipType(SkipType.REJECT.getKey());
            skipBack.setNextNodeCode(startNodeId);
            skipList.add(skipBack);
            node.setSkipList(skipList);
            return Arrays.asList(node);
        }
        // 拒绝直接结束
        FlowSkip skipBack = new FlowSkip();
        skipBack.setSkipType(SkipType.REJECT.getKey());
        skipBack.setNextNodeCode(endNodeId);
        skipList.add(skipBack);
        node.setSkipList(skipList);
        return Arrays.asList(node);  //List.of( node);
    }

    @Override
    public String getType() {
        return "between";
    }
}
