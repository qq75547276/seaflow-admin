package com.sykj.seaflow.warm.module.category.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fhs.core.trans.vo.TransPojo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.sykj.seaflow.common.base.entity.BaseEntity;

/**
* 流程分类
* @TableName flow_category
*/
@EqualsAndHashCode(callSuper = true)
@TableName("flow_category")
@Data
//@ApiModel("流程分类")
public class FlowCategory extends BaseEntity implements TransPojo {

    private static final long serialVersionUID = 1L;

    private String code;

    private String name;

    private String sortNum;

    private String icon;
    /**
     * 删除标识
     */
    @TableField(fill = FieldFill.INSERT)
    @TableLogic
    private String delFlag;


}
