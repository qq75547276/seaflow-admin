package com.sykj.seaflow.warm.core.condition;

import com.sykj.seaflow.warm.core.constant.FlowConstant;
import org.dromara.warm.flow.core.constant.FlowCons;
import org.dromara.warm.flow.core.condition.ConditionStrategy;

import java.util.Map;

public class ConditionStrategyDefault implements ConditionStrategy {

    @Override
    public String getType() {
        return FlowCons.splitAt + "default" + FlowCons.splitAt;
    }

    /**
     *  重写eval  实现一组表达式的解析
     * @param expression
     * @param variable
     * @return
     */
    @Override
    public Boolean eval(String expression, Map<String, Object> variable) {
        // 默认条件，当其他流程无法满足的时候，进入此条件 直接返回true
        return ! (boolean) variable.getOrDefault(FlowConstant.SERIAL_NODE_BEFORE_COMPLETED, false);

    }

}
