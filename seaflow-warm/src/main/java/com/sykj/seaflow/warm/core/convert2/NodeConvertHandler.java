package com.sykj.seaflow.warm.core.convert2;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener;
import com.sykj.seaflow.warm.core.listener.GlobalCreateListener;
import com.sykj.seaflow.warm.module.def.bean.SeaFlowNode;
import org.dromara.warm.flow.core.entity.Node;
import org.dromara.warm.flow.core.entity.Skip;
import org.dromara.warm.flow.core.enums.NodeType;
import org.dromara.warm.flow.core.enums.SkipType;
import org.dromara.warm.flow.core.listener.Listener;
import org.dromara.warm.flow.orm.entity.FlowSkip;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class NodeConvertHandler {

    private static Map<String, NodeConvertAbstract> nodeConvertMap = new ConcurrentHashMap<>();

    static {
        nodeConvertMap.put(NodeType.START.getValue(), new StartNodeConvert());
        nodeConvertMap.put(NodeType.BETWEEN.getValue(), new BetweenNodeConvert());
        nodeConvertMap.put(NodeType.SERIAL.getValue(), new SerialNodeConvert());
        nodeConvertMap.put(NodeType.PARALLEL.getValue(), new ParallelNodeConvert());
        nodeConvertMap.put(NodeType.END.getValue(), new EndNodeConvert());
    }

    /**
     *
     * @param parentNode 父节点
     * @param jsonNode  当前节点
     * @param startNodeId  开始节点id
     * @param endNodeId  结束节点id
     * @return
     */
    public static List<Node> convert(SeaFlowNode parentNode, JSONObject jsonNode, String startNodeId, String endNodeId,String lastLineNodeId) {
        List<Node> seaflowNodeList = new ArrayList<>();
        if(jsonNode == null){
            return seaflowNodeList;
        }
        if(ObjectUtil.isEmpty(startNodeId)){
            startNodeId = jsonNode.getStr("nodeId");
        }
        String type = jsonNode.getStr("nodeType");

        // 当前 类别存在，就去做转换
        NodeConvertAbstract nodeConvert = nodeConvertMap.get(type);
        if(nodeConvert != null) {
            List<Node> convert =   nodeConvert.convert(parentNode,jsonNode, startNodeId, endNodeId, lastLineNodeId);
            // 存入转换结果
            seaflowNodeList.addAll(convert);

        }
        return seaflowNodeList;
    }


}
