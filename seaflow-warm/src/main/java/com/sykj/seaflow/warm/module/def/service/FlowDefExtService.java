package com.sykj.seaflow.warm.module.def.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sykj.seaflow.warm.module.def.entity.FlowDefExt;

public interface FlowDefExtService  extends IService<FlowDefExt> {

}
