package com.sykj.seaflow.warm.core.convert;

import cn.hutool.json.JSONObject;
import com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener;
import com.sykj.seaflow.warm.core.listener.GlobalCreateListener;
import com.sykj.seaflow.warm.module.def.bean.SeaFlowNode;
import org.dromara.warm.flow.core.constant.FlowCons;
import org.dromara.warm.flow.core.entity.Node;
import org.dromara.warm.flow.core.entity.Skip;
import org.dromara.warm.flow.core.enums.SkipType;
import org.dromara.warm.flow.core.listener.Listener;
import org.dromara.warm.flow.orm.entity.FlowSkip;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StartNodeConvert extends NodeConvertAbstract{
    @Override
    public List<Node> convert(JSONObject jsonObject, String startNodeId, String endNodeId, String nextNodeId) {
        // 节点
        SeaFlowNode seaflowNode = getNode(jsonObject);
        // 设置发起人为自己
        seaflowNode.setPermissionFlag(FlowCons.WARMFLOWINITIATOR);

        seaflowNode.setListenerType(String.join(",", Listener.LISTENER_ASSIGNMENT, Listener.LISTENER_CREATE));
        seaflowNode.setListenerPath(String.join("@@", GlobalAssignmentListener.class.getName(),
                GlobalCreateListener.class.getName()));
        // 跳转
        FlowSkip flowSkip = new FlowSkip();
        flowSkip.setNowNodeCode(seaflowNode.getNodeCode());
        flowSkip.setSkipType(SkipType.PASS.getKey());
        flowSkip.setNextNodeCode(nextNodeId);
        List<Skip> skipList = new ArrayList<>();
        skipList.add(flowSkip);
        seaflowNode.setSkipList(skipList);

        return Arrays.asList(seaflowNode);  //List.of(seaflowNode);
    }

    @Override
    public String getType() {
        return "start";
    }
}
