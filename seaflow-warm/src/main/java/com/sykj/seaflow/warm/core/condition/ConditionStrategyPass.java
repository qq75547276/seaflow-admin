package com.sykj.seaflow.warm.core.condition;


import org.dromara.warm.flow.core.condition.ConditionStrategy;
import org.dromara.warm.flow.core.constant.FlowCons;

import java.util.Map;

/**
 * 回退 / 拒绝 处理
 */
public class ConditionStrategyPass implements ConditionStrategy {


    @Override
    public Boolean eval(String expression, Map<String, Object> variable) {
        // 默认条件，当其他流程无法满足的时候，进入此条件 直接返回true
        return  true;

    }

    @Override
    public String getType() {
        return FlowCons.splitAt + "pass" + FlowCons.splitAt;
    }
}
