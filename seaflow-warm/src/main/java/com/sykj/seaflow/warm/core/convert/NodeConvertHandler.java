package com.sykj.seaflow.warm.core.convert;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener;
import com.sykj.seaflow.warm.core.listener.GlobalCreateListener;
import com.sykj.seaflow.warm.module.def.bean.SeaFlowNode;
import org.dromara.warm.flow.core.entity.Node;
import org.dromara.warm.flow.core.entity.Skip;
import org.dromara.warm.flow.core.enums.NodeType;
import org.dromara.warm.flow.core.enums.SkipType;
import org.dromara.warm.flow.core.listener.Listener;
import org.dromara.warm.flow.orm.entity.FlowSkip;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class NodeConvertHandler {

    private static Map<String, NodeConvertAbstract> nodeConvertMap = new ConcurrentHashMap<>();

    static {
        nodeConvertMap.put(NodeType.START.getValue(), new StartNodeConvert());
        nodeConvertMap.put(NodeType.BETWEEN.getValue(), new BetweenNodeConvert());
        nodeConvertMap.put(NodeType.SERIAL.getValue(), new SerialNodeConvert());
        nodeConvertMap.put(NodeType.PARALLEL.getValue(), new ParallelNodeConvert());
        nodeConvertMap.put(NodeType.END.getValue(), new EndNodeConvert());
    }
    @Deprecated()
    public static List<Node> convertold(JSONArray jsonArray, String startNodeId, String endNodeId) {
        List<Node> seaflowNodeList = new ArrayList<>();
        if(ObjectUtil.isEmpty(startNodeId)){
            startNodeId = jsonArray.getJSONObject(0).getStr("nodeId");
        }
        if(ObjectUtil.isEmpty(endNodeId)){
            endNodeId = jsonArray.getJSONObject(jsonArray.size() - 1).getStr("nodeId");
        }

//        String currentNodeId = null;
        String nextNodeId = null;
        for(int i = 0; i < jsonArray.size(); i++) {
            JSONObject node = jsonArray.getJSONObject(i);
            String type = node.getStr("nodeType");
            if((i + 1) < jsonArray.size()){
                JSONObject nextNode = jsonArray.getJSONObject(i + 1);
                nextNodeId = nextNode.getStr("nodeId");

                //当前节点 和下个节点 都为网关时候， 中间加个系欸但
                if(NodeType.isGateWay(NodeType.getKeyByValue(nextNode.getStr("nodeType")))
                    && NodeType.isGateWay(NodeType.getKeyByValue(type))){

                    String emptyNodeCode = UUID.randomUUID().toString();
                    SeaFlowNode emptyNode = new SeaFlowNode();
                    emptyNode.setNodeCode(emptyNodeCode);
                    emptyNode.setNodeType(NodeType.BETWEEN.getKey());
                    // 设置空类别自动通过， 区分前端审批人为空的配置
                    String emptyApprove = "{\"type\":\"EMPTY\",\"value\":[]}";
                    emptyNode.setEmptyApprove(emptyApprove);
                    // 多个用逗号分割， 这里先写死
                    emptyNode.setListenerType(String.join(",", Listener.LISTENER_ASSIGNMENT, Listener.LISTENER_CREATE));
                    emptyNode.setListenerPath(String.join("@@", GlobalAssignmentListener.class.getName(),
                            GlobalCreateListener.class.getName()));

                    List<Skip> emptySkip = new ArrayList<>();
                    Skip emSkip = new FlowSkip();
                    emSkip.setSkipType(SkipType.PASS.getKey());
                    emSkip.setNextNodeCode(nextNodeId);
                    emptySkip.add(emSkip);
                    emptyNode.setSkipList(emptySkip);
                    seaflowNodeList.add(emptyNode);
                    // 更新下个节点的nodeId
                    nextNodeId = emptyNodeCode;
                }
            }else{
                nextNodeId = null;
            }



            NodeConvertAbstract nodeConvert = nodeConvertMap.get(type);

            if(nodeConvert != null) {
//                currentNodeId = node.getStr("nodeId");
                List<Node> convert = nodeConvert.convert(node, startNodeId, endNodeId, nextNodeId);
                seaflowNodeList.addAll(convert);
            }
        }
        return seaflowNodeList;
    }

}
