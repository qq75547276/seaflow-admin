package com.sykj.seaflow.warm.module.def.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.warm.module.def.entity.FlowApproval;
import com.sykj.seaflow.warm.module.def.entity.SeaFlowDefinition;
import com.sykj.seaflow.warm.module.def.params.SeaFlowDefinitionPageParam;
import org.dromara.warm.flow.core.entity.Definition;

import java.util.List;

public interface FlowDefinitionService {
//
//    public void add(seaflowDefinitionAddParam param);


    public IPage<Definition> page(SeaFlowDefinitionPageParam flowDefinitionPageParam, PageQuery<SeaFlowDefinition> pageQuery);

    public List<FlowApproval>  list(SeaFlowDefinitionPageParam flowDefinitionPageParam);
}
