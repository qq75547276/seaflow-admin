package com.sykj.seaflow.warm.module.design.params;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SeaFlowDesignPageParam {

    /**
     * 流程code
     */
    private String flowCode;
    /**
     * 流程名称
     */
    private String flowName;
    /**
     * 流程版本号
     */
    private String flowVersion;
    /**
     * 分类id
     */
    private String categoryId;
    /**
     * 流程定义id
     */
    private String defId;


    private String icon;

//    private String flowJson;
}
