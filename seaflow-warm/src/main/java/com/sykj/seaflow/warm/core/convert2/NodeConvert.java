package com.sykj.seaflow.warm.core.convert2;

import cn.hutool.json.JSONObject;
import com.sykj.seaflow.warm.module.def.bean.SeaFlowNode;
import org.dromara.warm.flow.core.entity.Node;

import java.util.List;

public interface NodeConvert {


    /**
     *
     * @param parentNode  父节点，可以未null,
     * @param node        子节点，可以为null
     * @param startNodeId 开始节点id
     * @param endNodeId   结束节点id
     * @param lastLineNodeId 网关 最后一个节点连线问题。 因为时层级嵌套，conditionNodes最后一个一定要和 childNode的id连，如果没有childNode,往前查找，依次类推
     * @return
     */
    public List<Node> convert(SeaFlowNode parentNode, JSONObject node, String startNodeId, String endNodeId, String lastLineNodeId);


    public String getType();

}
