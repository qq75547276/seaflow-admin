package com.sykj.seaflow.warm.module.def.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sykj.seaflow.warm.module.def.entity.FlowDefExt;

public interface FlowDefExtMapper extends BaseMapper<FlowDefExt> {

}
