package com.sykj.seaflow.warm.core.enums;


import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@Getter
public enum FormFieldPermissionEnum {

    EDIT("1", "edit"),  READONLY("2","disabled"), HIDDEN("3","hidden");

    private final String code;

    private final String value;

    FormFieldPermissionEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    // 表单字段权限匹配
    public static FormFieldPermissionEnum getPermission(String code){
        return Arrays.stream(FormFieldPermissionEnum.values())
                .filter(r -> r.getCode().equals(code)).findFirst()
                .orElse(null);
    }

}
