package com.sykj.seaflow.warm.module.task.entity;

import cn.hutool.core.util.ObjectUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.noear.snack.ONode;

import java.util.Date;
import java.util.Map;

@Getter
@Setter
public class FlowTaskDone  {
    /**
     * 任务id
     */
    private Long id;
    /**
     * 流程编码
     */
    private String flowCode;

    /**
     * 流程名称
     */
    private String flowName;

    private String flowJson;
    /**
     * 表单json
     */
    private String formJson;

    /**
     * 流程版本
     */
    private String version;
    /**
     * 流程实例当前节点
     */
    private String currNodeName;

    private Long instanceId;

    private Long definitionId;


    private Long taskId;
    /**
     * 节点类型
     */
    private String nodeType;

    /**
     * 任务节点名称
     */
    private String nodeName;

    /**
     * 任务开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    private Long createBy;

    /**
     * 发起人名称
     */
    private String initiatorName;
    /**
     * 审批者
     */
    private Long approver;

    private String approverName;

    /**
     * 协作方式
     */
    private Integer cooperateType;
    /**
     * 协作者
     */
    private String collaborator;

    /**
     * 流程实例状态
     */
    private Integer flowStatus;

    private String formPath;

    private String formCustom;

    private String detailFormPath;

    /**
     * 业务详情 存业务表对象json字符串
     */
    private String ext;
    /**
     * 审批意见
     */
    private String message;

    private String variable;

    public Map<String ,Object> formData;

    public String getFormPath(){
        return detailFormPath;
    }

    public Map<String ,Object> getFormData() {
        if (ObjectUtil.isNotEmpty(variable)) {
             Map<String,Object >  data =  ONode.deserialize(variable);
            formData = (Map<String, Object>) data.get("formData");

        }
        return formData;
    }
}

