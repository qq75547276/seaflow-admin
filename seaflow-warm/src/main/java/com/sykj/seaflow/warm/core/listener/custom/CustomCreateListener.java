package com.sykj.seaflow.warm.core.listener.custom;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dromara.warm.flow.core.listener.Listener;
import org.dromara.warm.flow.core.listener.ListenerVariable;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Map;

@AllArgsConstructor
@Component
@Slf4j
public class CustomCreateListener implements Listener {

    @Override
    public void notify(ListenerVariable listenerVariable){
        listenerVariable.getDefinition().getFlowCode();
        listenerVariable.getDefinition().getVersion();
        Map<String, Object> variable = listenerVariable.getVariable();
        System.out.printf("监听器调用");
        // 自己写?
    }
}
