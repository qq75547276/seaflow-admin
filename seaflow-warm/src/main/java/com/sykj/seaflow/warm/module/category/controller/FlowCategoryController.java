package com.sykj.seaflow.warm.module.category.controller;

import com.sykj.seaflow.warm.module.category.params.FlowCategoryAddParam;
import com.sykj.seaflow.warm.module.category.params.FlowCategoryEditParam;
import com.sykj.seaflow.warm.module.category.params.FlowCategoryIdParam;
import com.sykj.seaflow.warm.module.category.service.FlowCategoryService;
import lombok.AllArgsConstructor;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.common.base.bean.Result;
import com.sykj.seaflow.warm.module.category.entity.FlowCategory;
import com.sykj.seaflow.warm.module.category.params.FlowCategoryPageParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/flow/category")
@Validated
public class FlowCategoryController {

    private final FlowCategoryService flowCategoryService;

    /**
     *  添加
     */
    @PostMapping("/add")
    public void add(@Valid @RequestBody FlowCategoryAddParam flowCategoryAddParam){
        flowCategoryService.add(flowCategoryAddParam);
    }
    /**
     *  编辑
     */
    @PostMapping("edit")
    public void edit(@Valid @RequestBody FlowCategoryEditParam flowCategoryEditParam){
        flowCategoryService.edit(flowCategoryEditParam);
    }

    /**
     *  删除
     */
    @PostMapping("del")
    public void del(@Valid @RequestBody FlowCategoryIdParam flowCategoryIdParam){
        flowCategoryService.del(flowCategoryIdParam);
    }
    /**
     *  批量删除
     */
    @PostMapping("delBatch")
    public void delBatch(@Valid List<FlowCategoryIdParam> flowCategoryIdParams){
        flowCategoryService.delBatch(flowCategoryIdParams);
    }

    /**
     * 分页查询
     */
    @GetMapping("/page")
    public Result<FlowCategory> page(PageQuery pageQuery, FlowCategoryPageParam flowCategoryPageParam){
        return  Result.build(flowCategoryService.page(pageQuery, flowCategoryPageParam)) ;
    }

    @GetMapping("/list")
    public Result<FlowCategory> list( FlowCategoryPageParam flowCategoryPageParam){
        return  Result.build(flowCategoryService.list(flowCategoryPageParam)) ;
    }
}
