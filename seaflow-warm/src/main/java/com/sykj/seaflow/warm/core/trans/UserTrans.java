package com.sykj.seaflow.warm.core.trans;

import com.fhs.core.trans.anno.AutoTrans;
import com.fhs.core.trans.vo.VO;
import com.fhs.trans.service.AutoTransable;
import lombok.AllArgsConstructor;
import com.sykj.seaflow.sys.remote.api.SysUserApi;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 好像bean要实现 TransPojo
 */
@AllArgsConstructor
@Service
@AutoTrans(namespace = "userTrans",fields = {"nick"})
public class UserTrans implements AutoTransable {
    private final SysUserApi sysUserApi;

    @Override
    public List selectByIds(List ids) {
        List users = sysUserApi.getUsers(ids);
        return  users;
    }

    @Override
    public VO selectById(Object id) {
        return null;
    }
}
