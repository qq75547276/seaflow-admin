package com.sykj.seaflow.warm.module.design.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sykj.seaflow.warm.module.design.entity.SeaFlowDesign;

public interface SeaFlowDesignMapper extends BaseMapper<SeaFlowDesign> {
}
