package com.sykj.seaflow.warm.module.design.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SeaFlowDesignConfigParam {
    private Long id;

    /**
     * 流程json配置
     */
    private String flowJson;

    /**
     * 表单json配置
     */
    private String formJson;

}
