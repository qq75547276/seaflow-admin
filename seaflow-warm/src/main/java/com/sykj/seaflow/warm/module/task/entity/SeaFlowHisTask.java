package com.sykj.seaflow.warm.module.task.entity;


import cn.hutool.core.util.ObjectUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.dromara.warm.flow.orm.entity.FlowHisTask;

import java.util.Date;

public class SeaFlowHisTask extends FlowHisTask {
    //审批人名
    private String approveName;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    public String getApproveName() {
        if(ObjectUtil.equal(getApprover(), "-1")){
            return "系统自动处理";
        }
        return approveName;
    }

    public void setApproveName(String approveName) {
        this.approveName = approveName;
    }
}
