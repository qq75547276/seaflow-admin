package com.sykj.seaflow.warm.core.convert2;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener;
import com.sykj.seaflow.warm.core.listener.GlobalCreateListener;
import com.sykj.seaflow.warm.module.def.bean.SeaFlowNode;
import org.dromara.warm.flow.core.entity.Node;
import org.dromara.warm.flow.core.entity.Skip;
import org.dromara.warm.flow.core.enums.NodeType;
import org.dromara.warm.flow.core.enums.SkipType;
import org.dromara.warm.flow.core.listener.Listener;
import org.dromara.warm.flow.orm.entity.FlowSkip;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SerialNodeConvert extends NodeConvertAbstract {


    @Override
    public List<Node> convert(SeaFlowNode parentNode, JSONObject jsonObject, String startNodeId, String endNodeId, String lastLineNodeId){

        List<Node> seaflowNodeList = new ArrayList<>();
        SeaFlowNode node = getNode(jsonObject);
        seaflowNodeList.add(node);
        List<Skip> serialSkip = new ArrayList<>();

        JSONObject childNode = jsonObject.getJSONObject("childNode");
        String serialEndNodeId = childNode == null ? lastLineNodeId : childNode.getStr("nodeId");

        // 获取子节点
        JSONArray conditionNodes = jsonObject.getJSONArray("conditionNodes");
        for(Object conditionNode: conditionNodes){
            JSONObject condition = (JSONObject) conditionNode;
            JSONObject conditionChildNode = condition.getJSONObject("childNode");
            FlowSkip flowSkip = createSkip(condition, conditionChildNode == null ? serialEndNodeId : conditionChildNode.getStr("nodeId"));
            serialSkip.add(flowSkip);
            // 继续查找下个节点
            List<Node> nodes = nextNode(node, conditionChildNode, startNodeId, endNodeId, serialEndNodeId);
            seaflowNodeList.addAll(nodes);



        }

        List<Node> nodes = nextNode(node, childNode, startNodeId, endNodeId, lastLineNodeId);
        seaflowNodeList.addAll(nodes);

        node.setSkipList(serialSkip);
        return seaflowNodeList;
    }

    private FlowSkip createSkip(JSONObject subNode, String nextNodeId) {
        //default ：默认跳转配置， 只有条件分支才有
        FlowSkip skip = new FlowSkip();
        Boolean defaultSerial = subNode.getBool("default");
        if(defaultSerial != null && defaultSerial){
            // 设置表单时类别 简单模式 或 复杂模式
            String expressionType = "@@default@@|true";
            skip.setSkipCondition( expressionType );
            skip.setSkipName(subNode.getStr("nodeName"));
            skip.setSkipType(SkipType.PASS.getKey());
            skip.setNextNodeCode(nextNodeId);

        }else{
            //设置跳转
            JSONObject config = subNode.getJSONObject("properties");
            JSONObject conditions = config.getJSONObject("conditions");
            // 只有条件分支才有conditions
            if(conditions != null){
                Boolean simple = conditions.getBool("simple");
                String group = conditions.getStr("group");

                String simpleData = conditions.getStr("simpleData");
                // 设置表单时类别 简单模式 或 复杂模式
                String expressionType = "@@" +  (simple? "simple_"+ group : "complex") + "@@|";
                skip.setSkipCondition( expressionType +  simpleData );
                skip.setSkipName(subNode.getStr("nodeName"));
                skip.setSkipType(SkipType.PASS.getKey());
                skip.setNextNodeCode(nextNodeId);

            }
        }
        return skip;

    }

    @Override
    public String getType() {
        return "serial";
    }
}
