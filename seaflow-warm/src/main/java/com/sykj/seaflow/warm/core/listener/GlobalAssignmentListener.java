package com.sykj.seaflow.warm.core.listener;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.sykj.seaflow.sys.remote.api.SysUserApi;
import org.dromara.warm.flow.core.constant.FlowCons;
import org.dromara.warm.flow.core.entity.Node;
import org.dromara.warm.flow.core.entity.Task;
import org.dromara.warm.flow.core.listener.Listener;
import org.dromara.warm.flow.core.listener.ListenerVariable;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Component
@Slf4j
public class GlobalAssignmentListener implements Listener {

    @Override
    public void notify(ListenerVariable variable) {
        log.debug("权限监听开始: {}", variable);

        // 查看源码 需要传入 NodePermission 才行， 所以这里不能直接设置task
        List<String> userIds = null;
        // 获取后面的节点
//        Node node = variable.getNode();
        List<Node> nextNodes = variable.getNextNodes();
        List<Task> nextTasks = variable.getNextTasks();
        if(nextTasks == null){
            return;
        }
        for(Task nextTsk : nextTasks) {
            List<String> permissionList = nextTsk.getPermissionList();
            // 重新配置用户， 查询匹配的用户id
            if(ObjectUtil.isNotEmpty(permissionList)){
                 userIds = convertUserIds(permissionList, variable.getInstance().getCreateBy());
//                for(String id: userIds) {
//                    NodePermission nodePermission = new NodePermission();
//                    nodePermission.setPermissionFlag(id);
//                    nodePermission.setNodeCode(nextNode.getNodeCode());
//                    nodePermissionList.add(nodePermission);
//                }
                nextTsk.setPermissionList(userIds);
            }

//        List<Task> nextTasks = variable.getNextTasks();

//            if(ObjectUtil.isEmpty(userIds)){
//                // 没人的话 ，设置为null;   比如角色和部门 需要获取对应的用户id,但是没获取到， 就要清除permissionFlag
//                nextta
//                nextNode.setPermissionFlag(null);
//            }else{
//                nextNode.setDynamicPermissionFlagList(userIds);
////                variable.setNodePermissionList(nodePermissionList);
//            }
        }



    }

    private List<String> convertUserIds(List<String> permissionList, String createBy) {

        if(ObjectUtil.isEmpty(permissionList)) return null;
        List<String> list = new ArrayList<>();


//        String[] split = permissionFlag.split("@@");

        for(String permission : permissionList){
            // 流程发起人  转为用户id
            if(ObjectUtil.equal(permission, FlowCons.WARMFLOWINITIATOR)) {
                list.add(createBy) ;
            }else{
                String[] split1 = permission.split(":");
                if(ObjectUtil.equal(split1[0],"role")){
                    // 获取角色下的用户
                    List<String> roleIds = Arrays.asList(split1[1].split(","));
                    List<Long> userIdsByRole = SpringUtil.getBean(SysUserApi.class).getUserIdsByRole(roleIds);
                    list.addAll(userIdsByRole.stream().map(String::valueOf).collect(Collectors.toList()));  //.toList());
                }
                if(ObjectUtil.equal(split1[0],"dept")){
                    // 获取角色下的用户
                    List<String> deptIds = Arrays.asList(split1[1].split(","));
                    List<Long> userIdsByDept = SpringUtil.getBean(SysUserApi.class).getUserIdsByDept(deptIds);
                    if(ObjectUtil.isNotEmpty(userIdsByDept)){
                        list.addAll(userIdsByDept.stream().map(String::valueOf).collect(Collectors.toList()));  //.toList());
                    }
                }
                if(ObjectUtil.equal(split1[0], "user")){
                    list.addAll(Arrays.asList(split1[1].split(",")));
                }
            }


        };

        // 去重
        return list.stream().distinct().collect(Collectors.toList());// .toList();
    }
}
