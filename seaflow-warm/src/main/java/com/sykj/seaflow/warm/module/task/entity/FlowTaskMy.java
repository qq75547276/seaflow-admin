package com.sykj.seaflow.warm.module.task.entity;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.sykj.seaflow.warm.module.instance.entity.SeaFlowUser;
import lombok.Getter;
import lombok.Setter;
import org.noear.snack.ONode;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class FlowTaskMy {
    @TableId
    private Long id;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 删除标记
     */
    private String delFlag;

    /**
     * 对应flow_definition表的id
     */
    private Long definitionId;

    /**
     * 流程名称
     */
    private String flowName;

    private String flowCode;
    private String version;


    /**
     * 业务id
     */
    private String businessId;

    /**
     * 节点类型（0开始节点 1中间节点 2结束节点 3互斥网关 4并行网关）
     */
    private Integer nodeType;

    /**
     * 流程节点编码   每个流程的nodeCode是唯一的,即definitionId+nodeCode唯一,在数据库层面做了控制
     */
    private String nodeCode;

    /**
     * 流程节点名称
     */
    private String nodeName;

    /**
     * 流程变量
     */
    private String variable;

    /**
     * 流程状态（0待提交 1审批中 2 审批通过 3自动通过 8已完成 9已退回 10失效）
     */
    private Integer flowStatus;

    /**
     * 创建者
     */
    private Long createBy;

    private String initiatorName;

    /**
     * 审批表单是否自定义（Y是 2否）
     */
    private String formCustom;

    /**
     * 审批表单是否自定义（Y是 2否）
     */
    private String formPath;

    /**
     * 扩展字段
     */
    private String ext;

    /**
     * 流程变量
     */
    private Map<String ,Object> formData;
    /**
     * 流程json配置
     */
    private String flowJson;

    /**
     * 表单json
     */
    private String formJson;

    public Map<String ,Object> getFormData() {
        if (ObjectUtil.isNotEmpty(variable)) {
//            formData =  ONode.deserialize(variable);
            Map<String,Object >  data =  ONode.deserialize(variable);
            formData = (Map<String, Object>) data.get("formData");
        }
        return formData;
    }
    private List<SeaFlowUser> assigneeList;

    public String getAssigneeStr() {
        if (ObjectUtil.isNotEmpty(assigneeList)) {
            StringBuilder sb = new StringBuilder();
            for (SeaFlowUser seaflowUser : assigneeList) {
                if(ObjectUtil.isNotEmpty(seaflowUser.getName())){
                    sb.append(seaflowUser.getName()).append(",");
                }else{
                    sb.append(seaflowUser.getId()).append(", ");
                }

            }
            return sb.substring(0, sb.length() - 1);
        }
        return null;
    }



}

