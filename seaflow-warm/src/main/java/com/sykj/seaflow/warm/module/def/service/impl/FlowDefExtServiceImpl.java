package com.sykj.seaflow.warm.module.def.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sykj.seaflow.warm.module.def.entity.FlowDefExt;
import com.sykj.seaflow.warm.module.def.mapper.FlowDefExtMapper;
import com.sykj.seaflow.warm.module.def.service.FlowDefExtService;
import org.springframework.stereotype.Service;

@Service
public class FlowDefExtServiceImpl extends ServiceImpl<FlowDefExtMapper, FlowDefExt> implements FlowDefExtService {

}
