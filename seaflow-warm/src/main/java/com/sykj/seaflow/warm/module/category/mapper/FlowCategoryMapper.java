package com.sykj.seaflow.warm.module.category.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sykj.seaflow.warm.module.category.entity.FlowCategory;

/**
* @author 75547
* @description 针对表【flow_category(流程分类)】的数据库操作Mapper
* @createDate 2024-07-07 09:08:10
* @Entity genera.com.sykj.seaflow.warm.module.category.entity.FlowCategory
*/
public interface FlowCategoryMapper extends BaseMapper<FlowCategory> {

}




