package com.sykj.seaflow.warm.module.instance.entity;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SeaFlowUser {
    /**
     * 用户给id
     */
    private Long id;
    /**
     * 用户名
     */
    private String name;
    /**
     * 流程实例id
     */
    private Long instanceId;
}
