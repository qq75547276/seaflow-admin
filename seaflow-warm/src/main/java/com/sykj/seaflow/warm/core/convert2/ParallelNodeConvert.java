package com.sykj.seaflow.warm.core.convert2;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener;
import com.sykj.seaflow.warm.core.listener.GlobalCreateListener;
import com.sykj.seaflow.warm.module.def.bean.SeaFlowNode;
import org.dromara.warm.flow.core.entity.Node;
import org.dromara.warm.flow.core.entity.Skip;
import org.dromara.warm.flow.core.enums.NodeType;
import org.dromara.warm.flow.core.enums.SkipType;
import org.dromara.warm.flow.core.listener.Listener;
import org.dromara.warm.flow.orm.entity.FlowSkip;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ParallelNodeConvert extends NodeConvertAbstract {

    @Override
    public List<Node> convert(SeaFlowNode parentNode,JSONObject jsonNode, String startNodeId, String endNodeId,String lastLineNodeId){
        List<Node> seaflowNodeList = new ArrayList<>();
        JSONObject childNode = jsonNode.getJSONObject("childNode");

        // 网关节点处理完， 最后一个节点要跳转的id
//        String parellerEndNodeId = childNode == null ? endNodeId : childNode.getStr("nodeId");

        // 网关节点前加一个空节点， 使用当前网关节点（jsonNode）的id
        String emptyNodeId = jsonNode.getStr("nodeId");

        //  网关节点（入） id
        String startParallelNodeId = UUID.randomUUID().toString();

        // 创建入网关前，需要加一个空节点，不然会出现多条历史记录, 所以当前节点的id 就要给空节点用，
        SeaFlowNode emptyNode = getEmptyNode(emptyNodeId, startParallelNodeId);

        seaflowNodeList.add(emptyNode);

        // 创建入网关， 直接进去. 不需要判断前面是否完成
        SeaFlowNode serialNode = new SeaFlowNode();
        serialNode.setNodeCode(startParallelNodeId);
        serialNode.setNodeType(NodeType.PARALLEL.getKey());
//        serialNode.setDirection("1");  //网关（入）
        //加一个并行网关
        seaflowNodeList.add(serialNode);

        // 条件节点 ，
        JSONArray conditionNodes = jsonNode.getJSONArray("conditionNodes");
        //  网关（出） 的 id , 条件节点最后一个连接此节点
        String parellerEndNodeId  = UUID.randomUUID().toString();
        for(Object jsonObject : conditionNodes ){
            JSONObject  conditionNode =  (JSONObject) jsonObject;
            //  最后一个节点时，应该连接当前node的id
            List<Node> convert = NodeConvertHandler.convert(serialNode, conditionNode, startNodeId, endNodeId,parellerEndNodeId);

            // 网关（入） 添加连线
            FlowSkip flowSkip = new FlowSkip();
            flowSkip.setSkipType(SkipType.PASS.getKey());
            flowSkip.setNextNodeCode(conditionNode.getStr("nodeId"));
            serialNode.getSkipList().add(flowSkip);
            seaflowNodeList.addAll(convert);
        }

        // 网关出
        SeaFlowNode endNode = getNode(jsonNode);
        endNode.setNodeCode(parellerEndNodeId);

        seaflowNodeList.add(endNode);


        FlowSkip parallelOutSkip = new FlowSkip();
        parallelOutSkip.setSkipType(SkipType.PASS.getKey());
        parallelOutSkip.setNextNodeCode(lastLineNodeId);
        endNode.setSkipList(Arrays.asList(parallelOutSkip));

        List<Node> nodes = nextNode(endNode, childNode, startNodeId, endNodeId, lastLineNodeId);
        seaflowNodeList.addAll(nodes);



        return seaflowNodeList;
    }

    @Override
    public String getType() {
        return "parallel";
    }
}
