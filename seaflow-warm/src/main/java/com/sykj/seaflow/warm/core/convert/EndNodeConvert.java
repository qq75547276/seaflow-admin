package com.sykj.seaflow.warm.core.convert;

import cn.hutool.json.JSONObject;
import com.sykj.seaflow.warm.module.def.bean.SeaFlowNode;
import org.dromara.warm.flow.core.entity.Node;

import java.util.Arrays;
import java.util.List;

public class EndNodeConvert extends NodeConvertAbstract {
    @Override
    public String getType() {
        return "end";
    }

    @Override
    public List<Node> convert(JSONObject jsonObject, String startNodeId, String endNodeId, String nextNodeId){

        SeaFlowNode node = getNode(jsonObject);

        return Arrays.asList(node);  //List.of(node);
    }
}
