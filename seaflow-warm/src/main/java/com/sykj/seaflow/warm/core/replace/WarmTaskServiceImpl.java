package com.sykj.seaflow.warm.core.replace;

import org.dromara.warm.flow.core.dto.FlowParams;
import org.dromara.warm.flow.core.entity.Node;
import org.dromara.warm.flow.core.enums.NodeType;
import org.dromara.warm.flow.core.service.impl.TaskServiceImpl;

import java.util.List;

/**
 * 先保留， 这里没有执行， 先看看源码是否还需要调整
 */
public class WarmTaskServiceImpl extends TaskServiceImpl {


    /**
     * 覆盖方法,  如果 网关下面还是网关, 继续递归查询
     * @param flowParams
     * @param node
     * @return
     */
//    @Override
    public List<Node> getNextByCheckGateWay(FlowParams flowParams, Node node) {
        List<Node> nodeList = getNextByCheckGateWay(flowParams, node);
//        List<Node> actualNextNode = new ArrayList<>();
//        nextNode(flowParams, nodeList, actualNextNode);
        return nodeList;

    }

    public void nextNode(FlowParams flowParams,  List<Node> result, List<Node> actualNextNode ){
        for(Node nextNode : result){
            if(NodeType.isGateWay(nextNode.getNodeType())){
                List<Node> netNode = getNextByCheckGateWay(flowParams, nextNode);
                nextNode(flowParams, netNode, actualNextNode);
            }else{
                actualNextNode.add(nextNode);
            }
        }
    }





}
