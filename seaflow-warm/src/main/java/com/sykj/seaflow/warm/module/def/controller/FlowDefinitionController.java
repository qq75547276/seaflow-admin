package com.sykj.seaflow.warm.module.def.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.common.base.bean.Result;
import com.sykj.seaflow.warm.module.def.entity.FlowApproval;
import com.sykj.seaflow.warm.module.def.entity.SeaFlowDefinition;
import com.sykj.seaflow.warm.module.def.params.SeaFlowDefinitionPageParam;
import com.sykj.seaflow.warm.module.def.service.FlowDefinitionService;
import org.dromara.warm.flow.core.entity.Definition;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Validated
@AllArgsConstructor
@RestController
@RequestMapping("/flow/def")
public class FlowDefinitionController {

    private final FlowDefinitionService flowDefinitionService;

//    @PostMapping("/add")
//    public void add(@RequestBody seaflowDefinitionAddParam flowDefinitionAddParam){
//
//        flowDefinitionService.add(flowDefinitionAddParam);
//
//    }
//
//    @PostMapping("/edit")
//    public void edit(@RequestBody seaflowDefinitionEditParam flowDefinitionEditParam){
//        flowDefinitionService.edit(flowDefinitionEditParam);
//    }

    /**
     * 只是查询了下流程定义， 先不使用
     * @param flowDefinitionPageParam
     * @param pageQuery
     * @return
     */
    @GetMapping("/page")
    public Result<Definition> page(SeaFlowDefinitionPageParam flowDefinitionPageParam, PageQuery<SeaFlowDefinition> pageQuery){
        IPage<Definition> page = flowDefinitionService.page(flowDefinitionPageParam, pageQuery);
        return Result.build(page);
    }

    /**
     * 流程列表，全部分类展示
     * @param flowDefinitionPageParam
     * @return
     */
    @GetMapping("/list")
    public Result<FlowApproval> list(SeaFlowDefinitionPageParam flowDefinitionPageParam){
        List<FlowApproval> list = flowDefinitionService.list(flowDefinitionPageParam);
        return Result.build(list);
    }
}
