package com.sykj.seaflow.warm.core.condition;


import org.dromara.warm.flow.core.condition.ConditionStrategyAbstract;
import org.dromara.warm.flow.core.constant.FlowCons;
import org.dromara.warm.flow.core.utils.MathUtil;

/**
 * 回退 / 拒绝 处理
 */
public class ConditionStrategyReject extends ConditionStrategyAbstract {


    @Override
    public Boolean afterEval(String[] split, String value) {
        if (MathUtil.isNumeric(split[2].trim())) {
              return  MathUtil.determineSize(value, split[2].trim()) == 0;
        } else {
            return false;
        }
    }

    @Override
    public String getType() {
        return FlowCons.splitAt + "reject" + FlowCons.splitAt;
    }
}
