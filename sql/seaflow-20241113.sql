-- 资源授权
CREATE TABLE `sys_resource` (
                                `id` BIGINT(20) NOT NULL,
                                `create_user` BIGINT(20) NULL DEFAULT NULL,
                                `update_user` BIGINT(20) NULL DEFAULT NULL,
                                `tenant_id` BIGINT(20) NULL DEFAULT NULL,
                                `create_time` DATETIME NULL DEFAULT NULL,
                                `update_time` DATETIME NULL DEFAULT NULL,
                                `resource_type` CHAR(1) NULL DEFAULT NULL COMMENT '资源类型：1 菜单 2: 权限标识' COLLATE 'utf8_bin',
                                `from_type` CHAR(1) NULL DEFAULT NULL COMMENT '来源类别： 1:用户， 2：角色，3职位' COLLATE 'utf8_bin',
                                `from_id` BIGINT(20) NULL DEFAULT NULL COMMENT '来源id(用户id、角色id、职位id)',
                                `resource_id` BIGINT(20) NULL DEFAULT NULL COMMENT '1：菜单、2：标识',
                                PRIMARY KEY (`id`) USING BTREE
)
    COMMENT='授权信息'
COLLATE='utf8_bin'
ENGINE=InnoDB
;


INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187650, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1846765869433847809);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187651, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845755275595427842);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187652, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845755572162080770);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187653, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845755764395421698);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187654, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845756324355977218);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187655, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845756550873559042);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187656, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '2', '2', 1812054642967375874, 1855965844009041921);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187657, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '2', '2', 1812054642967375874, 1856270198398218241);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187658, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '2', '2', 1812054642967375874, 1856273410949849090);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187659, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '2', '2', 1812054642967375874, 1856273510182887425);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187660, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845756690459996162);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187661, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845757038041968641);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187662, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845757201661767682);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187663, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845757511306260481);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187664, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845757313775513601);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187665, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845757662183763969);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187666, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845456303395930113);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187667, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845456482161360897);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187668, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845665743562813442);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187669, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845665865403150337);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187670, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1847133611022307329);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187671, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1847471973935570946);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187672, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845757880480509954);
INSERT INTO `sys_resource` (`id`, `create_user`, `update_user`, `tenant_id`, `create_time`, `update_time`, `resource_type`, `from_type`, `from_id`, `resource_id`) VALUES (1856664796396187673, 1, NULL, 1, '2024-11-13 19:45:57', NULL, '1', '2', 1812054642967375874, 1845758451329478657);


INSERT INTO `sys_role_user` (`id`, `tenant_id`, `create_user`, `update_user`, `create_time`, `update_time`, `role_id`, `user_id`) VALUES (1812695992825999361, 1, 1, NULL, '2024-07-15 11:49:38', NULL, 1812054642967375874, 1);
