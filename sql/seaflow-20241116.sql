-- 新增表单配置json字段
ALTER TABLE `flow_design`
    ADD COLUMN `form_json` TEXT NULL DEFAULT NULL COMMENT '表单json数据' AFTER `flow_json`;

ALTER TABLE `flow_def_ext`
    ADD COLUMN `form_json` TEXT NULL DEFAULT NULL COMMENT '表单json配置' AFTER `flow_json`;
ALTER TABLE `flow_node_ext`
    ADD COLUMN `field_setting` TEXT NULL DEFAULT NULL COMMENT '字段显示编辑控制' AFTER `button_json`;
