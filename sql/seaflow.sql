-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.7.39 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- 导出  表 seaflow.flow_category 结构
CREATE TABLE IF NOT EXISTS `flow_category` (
  `id` bigint(20) NOT NULL,
  `tenant_id` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `sort_num` int(11) DEFAULT NULL,
  `del_flag` char(50) COLLATE utf8_bin DEFAULT NULL,
  `icon` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='流程分类';

-- 正在导出表  seaflow.flow_category 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `flow_category` DISABLE KEYS */;
INSERT INTO `flow_category` (`id`, `tenant_id`, `create_user`, `update_user`, `create_time`, `update_time`, `code`, `name`, `sort_num`, `del_flag`, `icon`) VALUES
	(1810191677402050562, 1, 1, 1, '2024-07-08 13:58:22', '2024-10-20 14:31:48', 'finance', '财务', 2, '0', 'Connection'),
	(1811200484240461826, 1, 1, NULL, '2024-07-11 08:47:01', NULL, 'administration', '行政', 1, '0', '');
/*!40000 ALTER TABLE `flow_category` ENABLE KEYS */;

-- 导出  表 seaflow.flow_definition 结构
CREATE TABLE IF NOT EXISTS `flow_definition` (
  `id` bigint(20) unsigned NOT NULL COMMENT '主键id',
  `flow_code` varchar(40) COLLATE utf8_bin NOT NULL COMMENT '流程编码',
  `flow_name` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '流程名称',
  `version` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '流程版本',
  `is_publish` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否发布（0未发布 1已发布 9失效）',
  `form_custom` char(1) CHARACTER SET utf8mb4 DEFAULT 'N' COMMENT '审批表单是否自定义（Y是 N否）',
  `form_path` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '审批表单路径',
  `activity_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '流程激活状态（0挂起 1激活）',
  `listener_type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '监听器类型',
  `listener_path` varchar(400) COLLATE utf8_bin DEFAULT NULL COMMENT '监听器路径',
  `ext` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '扩展字段，预留给业务系统使用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志',
  `tenant_id` varchar(40) COLLATE utf8_bin DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='流程定义表';

-- 正在导出表  seaflow.flow_definition 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `flow_definition` DISABLE KEYS */;
INSERT INTO `flow_definition` (`id`, `flow_code`, `flow_name`, `version`, `is_publish`, `form_custom`, `form_path`, `activity_status`, `listener_type`, `listener_path`, `ext`, `create_time`, `update_time`, `del_flag`, `tenant_id`) VALUES
	(1852714563165454338, 'leave', '请假流程', '1', 1, 'Y', 'leave.vue', 1, NULL, NULL, NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1');
/*!40000 ALTER TABLE `flow_definition` ENABLE KEYS */;

-- 导出  表 seaflow.flow_def_ext 结构
CREATE TABLE IF NOT EXISTS `flow_def_ext` (
  `id` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `flow_json` text COLLATE utf8_bin,
  `tenant_id` bigint(20) DEFAULT NULL,
  `manager_id` bigint(20) DEFAULT NULL COMMENT '流程管理员id',
  `design_id` bigint(20) DEFAULT NULL COMMENT '流程设计id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 正在导出表  seaflow.flow_def_ext 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `flow_def_ext` DISABLE KEYS */;
INSERT INTO `flow_def_ext` (`id`, `create_user`, `update_user`, `create_time`, `update_time`, `flow_json`, `tenant_id`, `manager_id`, `design_id`) VALUES
	(1852714563165454338, 1, NULL, '2024-11-02 22:09:08', NULL, '{"nodeId":"801e3c73-e665-4f6f-8ea7-d4de0927214c","nodeType":"start","nodeName":"开始节点","value":"所有人","properties":{},"childNode":{"nodeId":"f1c63412-5710-446d-a5ac-f441f37a22c2","nodeName":"审批人1","nodeType":"between","value":"seven","type":"between","properties":{"nodeRatioType":"1","backType":"1","nodeRatio":0,"permissions":["user"],"combination":{"role":[],"user":[{"id":"1814307624207470594","name":"seven"}]},"emptyApprove":{"type":"AUTO","value":[]},"value":"seven","buttons":[{"type":"aggren","checked":true,"text":"同意"},{"type":"reject","checked":true,"text":"拒绝"},{"type":"back","checked":false,"text":"回退"},{"type":"transfer","checked":false,"text":"转办"},{"type":"depute","checked":false,"text":"委派"},{"type":"signAdd","checked":false,"text":"加签"},{"type":"signRedu","checked":false,"text":"减签"}],"backTypeNode":"801e3c73-e665-4f6f-8ea7-d4de0927214c"},"childNode":{"nodeId":"3483a491-0765-4b54-8c35-dbacb29e5bbf","nodeType":"serial","nodeName":"分支选择","conditionNodes":[{"nodeId":"b1d40308-9bab-4aaa-a1b0-7a362e24844f","nodeType":"serial-node","type":"serial-node","nodeName":"条件","sortNum":0,"value":"days 大于等于 3","properties":{"conditions":{"simple":true,"group":"and","simpleData":[{"key":"days","cond":"ge","value":"3","next":"and"}]},"value":"days 大于等于 3"},"levelValue":"优先级1","childNode":{"nodeId":"f36a09c6-08ab-4027-85f9-db1f8a55f57c","nodeType":"parallel","nodeName":"并行分支","properties":{},"conditionNodes":[{"nodeId":"a4095f0e-1950-4d4c-b886-4a3035b645c2","nodeName":"审批人1","nodeType":"between","type":"between","properties":{"nodeRatioType":"1","backType":"1","nodeRatio":0,"permissions":["user"],"combination":{"role":[],"user":[{"id":"1","name":"超管"}]},"emptyApprove":{"type":"AUTO","value":[]},"value":"超管","buttons":[{"type":"aggren","checked":true,"text":"同意"},{"type":"reject","checked":true,"text":"拒绝"},{"type":"back","checked":false,"text":"回退"},{"type":"transfer","checked":false,"text":"转办"},{"type":"depute","checked":false,"text":"委派"},{"type":"signAdd","checked":false,"text":"加签"},{"type":"signRedu","checked":false,"text":"减签"}],"backTypeNode":"801e3c73-e665-4f6f-8ea7-d4de0927214c"},"value":"超管"},{"nodeId":"091944fb-4f6f-4eeb-82d9-1ea3da22bd52","nodeName":"审批人2","nodeType":"between","type":"between","properties":{"nodeRatioType":"1","backType":"1","nodeRatio":0,"permissions":["user"],"combination":{"role":[],"user":[{"id":"2","name":"测试"}]},"emptyApprove":{"type":"AUTO","value":[]},"value":"测试","buttons":[{"type":"aggren","checked":true,"text":"同意"},{"type":"reject","checked":true,"text":"拒绝"},{"type":"back","checked":false,"text":"回退"},{"type":"transfer","checked":false,"text":"转办"},{"type":"depute","checked":false,"text":"委派"},{"type":"signAdd","checked":false,"text":"加签"},{"type":"signRedu","checked":false,"text":"减签"}],"backTypeNode":"801e3c73-e665-4f6f-8ea7-d4de0927214c"},"value":"测试"}]}},{"nodeId":"1f678933-91b3-43c2-814c-26722a25d8f4","nodeType":"serial-node","type":"serial-node","nodeName":"条件","value":"其他条件默认走此流程","sortNum":1,"default":true,"properties":{},"levelValue":"优先级2","enableDestory":true,"childNode":{"nodeId":"32610c7f-21f1-43c5-821b-f65e3fcb917b","nodeName":"审批人1","nodeType":"between","value":"测试","type":"between","properties":{"nodeRatioType":"1","backType":"1","nodeRatio":0,"permissions":["user"],"combination":{"role":[],"user":[{"id":"2","name":"测试"}]},"emptyApprove":{"type":"AUTO","value":[]},"value":"测试","buttons":[{"type":"aggren","checked":true,"text":"同意"},{"type":"reject","checked":true,"text":"拒绝"},{"type":"back","checked":false,"text":"回退"},{"type":"transfer","checked":false,"text":"转办"},{"type":"depute","checked":false,"text":"委派"},{"type":"signAdd","checked":false,"text":"加签"},{"type":"signRedu","checked":false,"text":"减签"}],"backTypeNode":"801e3c73-e665-4f6f-8ea7-d4de0927214c"}}}],"childNode":{"nodeId":"fa13aeeb-f794-43c8-a2aa-27da340b9696","nodeType":"end","nodeName":"结束","properties":{}}}}}', 1, 1, 1852714097983586305);
/*!40000 ALTER TABLE `flow_def_ext` ENABLE KEYS */;

-- 导出  表 seaflow.flow_design 结构
CREATE TABLE IF NOT EXISTS `flow_design` (
  `id` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `del_flag` int(11) DEFAULT NULL,
  `tenant_id` bigint(20) DEFAULT NULL,
  `flow_code` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `flow_name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `flow_version` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '版本号',
  `icon` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `flow_json` text COLLATE utf8_bin,
  `def_id` bigint(20) DEFAULT NULL COMMENT '当前使用的流程定义',
  `category_id` bigint(20) DEFAULT NULL COMMENT '业务分类',
  `status` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `sort_num` int(11) DEFAULT NULL COMMENT '排序',
  `form_custom` char(50) COLLATE utf8_bin DEFAULT NULL COMMENT 'Y：自定义表单，N 非自定义表单',
  `form_path` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '表单路径',
  `manager_id` bigint(20) NOT NULL COMMENT '流程管理员'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 正在导出表  seaflow.flow_design 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `flow_design` DISABLE KEYS */;
INSERT INTO `flow_design` (`id`, `create_time`, `update_time`, `create_user`, `update_user`, `del_flag`, `tenant_id`, `flow_code`, `flow_name`, `flow_version`, `icon`, `flow_json`, `def_id`, `category_id`, `status`, `sort_num`, `form_custom`, `form_path`, `manager_id`) VALUES
	(1852714097983586305, '2024-11-02 22:07:17', '2024-11-02 22:09:03', 1, 1, NULL, 1, 'leave', '请假流程', '1', 'Avatar', '{"nodeId":"801e3c73-e665-4f6f-8ea7-d4de0927214c","nodeType":"start","nodeName":"开始节点","value":"所有人","properties":{},"childNode":{"nodeId":"f1c63412-5710-446d-a5ac-f441f37a22c2","nodeName":"审批人1","nodeType":"between","value":"seven","type":"between","properties":{"nodeRatioType":"1","backType":"1","nodeRatio":0,"permissions":["user"],"combination":{"role":[],"user":[{"id":"1814307624207470594","name":"seven"}]},"emptyApprove":{"type":"AUTO","value":[]},"value":"seven","buttons":[{"type":"aggren","checked":true,"text":"同意"},{"type":"reject","checked":true,"text":"拒绝"},{"type":"back","checked":false,"text":"回退"},{"type":"transfer","checked":false,"text":"转办"},{"type":"depute","checked":false,"text":"委派"},{"type":"signAdd","checked":false,"text":"加签"},{"type":"signRedu","checked":false,"text":"减签"}],"backTypeNode":"801e3c73-e665-4f6f-8ea7-d4de0927214c"},"childNode":{"nodeId":"3483a491-0765-4b54-8c35-dbacb29e5bbf","nodeType":"serial","nodeName":"分支选择","conditionNodes":[{"nodeId":"b1d40308-9bab-4aaa-a1b0-7a362e24844f","nodeType":"serial-node","type":"serial-node","nodeName":"条件","sortNum":0,"value":"days 大于等于 3","properties":{"conditions":{"simple":true,"group":"and","simpleData":[{"key":"days","cond":"ge","value":"3","next":"and"}]},"value":"days 大于等于 3"},"levelValue":"优先级1","childNode":{"nodeId":"f36a09c6-08ab-4027-85f9-db1f8a55f57c","nodeType":"parallel","nodeName":"并行分支","properties":{},"conditionNodes":[{"nodeId":"a4095f0e-1950-4d4c-b886-4a3035b645c2","nodeName":"审批人1","nodeType":"between","type":"between","properties":{"nodeRatioType":"1","backType":"1","nodeRatio":0,"permissions":["user"],"combination":{"role":[],"user":[{"id":"1","name":"超管"}]},"emptyApprove":{"type":"AUTO","value":[]},"value":"超管","buttons":[{"type":"aggren","checked":true,"text":"同意"},{"type":"reject","checked":true,"text":"拒绝"},{"type":"back","checked":false,"text":"回退"},{"type":"transfer","checked":false,"text":"转办"},{"type":"depute","checked":false,"text":"委派"},{"type":"signAdd","checked":false,"text":"加签"},{"type":"signRedu","checked":false,"text":"减签"}],"backTypeNode":"801e3c73-e665-4f6f-8ea7-d4de0927214c"},"value":"超管"},{"nodeId":"091944fb-4f6f-4eeb-82d9-1ea3da22bd52","nodeName":"审批人2","nodeType":"between","type":"between","properties":{"nodeRatioType":"1","backType":"1","nodeRatio":0,"permissions":["user"],"combination":{"role":[],"user":[{"id":"2","name":"测试"}]},"emptyApprove":{"type":"AUTO","value":[]},"value":"测试","buttons":[{"type":"aggren","checked":true,"text":"同意"},{"type":"reject","checked":true,"text":"拒绝"},{"type":"back","checked":false,"text":"回退"},{"type":"transfer","checked":false,"text":"转办"},{"type":"depute","checked":false,"text":"委派"},{"type":"signAdd","checked":false,"text":"加签"},{"type":"signRedu","checked":false,"text":"减签"}],"backTypeNode":"801e3c73-e665-4f6f-8ea7-d4de0927214c"},"value":"测试"}]}},{"nodeId":"1f678933-91b3-43c2-814c-26722a25d8f4","nodeType":"serial-node","type":"serial-node","nodeName":"条件","value":"其他条件默认走此流程","sortNum":1,"default":true,"properties":{},"levelValue":"优先级2","enableDestory":true,"childNode":{"nodeId":"32610c7f-21f1-43c5-821b-f65e3fcb917b","nodeName":"审批人1","nodeType":"between","value":"测试","type":"between","properties":{"nodeRatioType":"1","backType":"1","nodeRatio":0,"permissions":["user"],"combination":{"role":[],"user":[{"id":"2","name":"测试"}]},"emptyApprove":{"type":"AUTO","value":[]},"value":"测试","buttons":[{"type":"aggren","checked":true,"text":"同意"},{"type":"reject","checked":true,"text":"拒绝"},{"type":"back","checked":false,"text":"回退"},{"type":"transfer","checked":false,"text":"转办"},{"type":"depute","checked":false,"text":"委派"},{"type":"signAdd","checked":false,"text":"加签"},{"type":"signRedu","checked":false,"text":"减签"}],"backTypeNode":"801e3c73-e665-4f6f-8ea7-d4de0927214c"}}}],"childNode":{"nodeId":"fa13aeeb-f794-43c8-a2aa-27da340b9696","nodeType":"end","nodeName":"结束","properties":{}}}}}', 1852714563165454338, 1811200484240461826, '1', NULL, 'Y', 'leave.vue', 1);
/*!40000 ALTER TABLE `flow_design` ENABLE KEYS */;

-- 导出  表 seaflow.flow_his_task 结构
CREATE TABLE IF NOT EXISTS `flow_his_task` (
  `id` bigint(20) unsigned NOT NULL COMMENT '主键id',
  `definition_id` bigint(20) NOT NULL COMMENT '对应flow_definition表的id',
  `instance_id` bigint(20) NOT NULL COMMENT '对应flow_instance表的id',
  `task_id` bigint(20) NOT NULL COMMENT '对应flow_task表的id',
  `node_code` varchar(100) DEFAULT NULL COMMENT '开始节点编码',
  `node_name` varchar(100) DEFAULT NULL COMMENT '开始节点名称',
  `node_type` tinyint(1) DEFAULT NULL COMMENT '开始节点类型（0开始节点 1中间节点 2结束节点 3互斥网关 4并行网关）',
  `target_node_code` varchar(100) DEFAULT NULL COMMENT '目标节点编码',
  `target_node_name` varchar(100) DEFAULT NULL COMMENT '结束节点名称',
  `approver` varchar(40) DEFAULT NULL COMMENT '审批者',
  `cooperate_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '协作方式(1审批 2转办 3委派 4会签 5票签 6加签 7减签)',
  `collaborator` varchar(40) DEFAULT NULL COMMENT '协作人',
  `skip_type` varchar(10) DEFAULT NULL COMMENT '流转类型（PASS通过 REJECT退回 NONE无动作）',
  `flow_status` varchar(20) DEFAULT NULL COMMENT '流程状态（0待提交 1审批中 2 审批通过 3自动通过 8已完成 9已退回 10失效）',
  `form_custom` char(1) DEFAULT 'N' COMMENT '审批表单是否自定义（Y是 N否）',
  `form_path` varchar(100) DEFAULT NULL COMMENT '审批表单路径',
  `message` varchar(500) DEFAULT NULL COMMENT '审批意见',
  `ext` varchar(400) DEFAULT NULL COMMENT '业务详情 存业务表对象json字符串',
  `create_time` datetime DEFAULT NULL COMMENT '开始时间',
  `update_time` datetime DEFAULT NULL COMMENT '完成时间',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志',
  `tenant_id` varchar(40) DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='历史任务记录表';

-- 正在导出表  seaflow.flow_his_task 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `flow_his_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `flow_his_task` ENABLE KEYS */;

-- 导出  表 seaflow.flow_instance 结构
CREATE TABLE IF NOT EXISTS `flow_instance` (
  `id` bigint(20) NOT NULL COMMENT '主键id',
  `definition_id` bigint(20) NOT NULL COMMENT '对应flow_definition表的id',
  `business_id` varchar(40) COLLATE utf8_bin NOT NULL COMMENT '业务id',
  `node_type` tinyint(1) NOT NULL COMMENT '结点类型（0开始节点 1中间节点 2结束节点 3互斥网关 4并行网关）',
  `node_code` varchar(40) COLLATE utf8_bin NOT NULL COMMENT '流程节点编码',
  `node_name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '流程节点名称',
  `variable` text COLLATE utf8_bin COMMENT '任务变量',
  `flow_status` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '流程状态（0待提交 1审批中 2 审批通过 3自动通过 8已完成 9已退回 10失效）',
  `activity_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '流程激活状态（0挂起 1激活）',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志',
  `ext` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '扩展字段',
  `tenant_id` varchar(40) COLLATE utf8_bin DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='流程实例表';

-- 正在导出表  seaflow.flow_instance 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `flow_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `flow_instance` ENABLE KEYS */;

-- 导出  表 seaflow.flow_node 结构
CREATE TABLE IF NOT EXISTS `flow_node` (
  `id` bigint(20) unsigned NOT NULL COMMENT '主键id',
  `node_type` tinyint(1) NOT NULL COMMENT '节点类型（0开始节点 1中间节点 2结束结点 3互斥网关 4并行网关）',
  `definition_id` bigint(20) NOT NULL COMMENT '流程定义id',
  `node_code` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '流程节点编码',
  `node_name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '流程节点名称',
  `permission_flag` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '权限标识（权限类型:权限标识，可以多个，用逗号隔开)',
  `node_ratio` decimal(6,3) DEFAULT NULL COMMENT '流程签署比例值',
  `coordinate` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '坐标',
  `skip_any_node` varchar(100) COLLATE utf8_bin DEFAULT 'N' COMMENT '是否可以退回任意节点（Y是 N否）',
  `listener_type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '监听器类型',
  `listener_path` varchar(400) COLLATE utf8_bin DEFAULT NULL COMMENT '监听器路径',
  `handler_type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '处理器类型',
  `handler_path` varchar(400) COLLATE utf8_bin DEFAULT NULL COMMENT '处理器路径',
  `form_custom` char(1) COLLATE utf8_bin DEFAULT 'N' COMMENT '审批表单是否自定义（Y是 N否）',
  `form_path` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '审批表单路径',
  `version` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '版本',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志',
  `tenant_id` varchar(40) COLLATE utf8_bin DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='流程结点表';

-- 正在导出表  seaflow.flow_node 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `flow_node` DISABLE KEYS */;
INSERT INTO `flow_node` (`id`, `node_type`, `definition_id`, `node_code`, `node_name`, `permission_flag`, `node_ratio`, `coordinate`, `skip_any_node`, `listener_type`, `listener_path`, `handler_type`, `handler_path`, `form_custom`, `form_path`, `version`, `create_time`, `update_time`, `del_flag`, `tenant_id`) VALUES
	(1852714563228368897, 1, 1852714563165454338, 'f1c63412-5710-446d-a5ac-f441f37a22c2', '审批人1', 'user:1814307624207470594', 0.000, NULL, 'N', 'assignment,create', 'com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener@@com.sykj.seaflow.warm.core.listener.GlobalCreateListener', NULL, NULL, 'N', NULL, '2', '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368898, 3, 1852714563165454338, '3483a491-0765-4b54-8c35-dbacb29e5bbf', '分支选择', NULL, 0.000, NULL, 'N', NULL, NULL, NULL, NULL, 'N', NULL, '2', '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368899, 1, 1852714563165454338, 'f36a09c6-08ab-4027-85f9-db1f8a55f57c', '自动通过', NULL, 0.000, NULL, 'N', 'assignment,create', 'com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener@@com.sykj.seaflow.warm.core.listener.GlobalCreateListener', NULL, NULL, 'N', NULL, '2', '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368900, 4, 1852714563165454338, '743a2719-639c-4371-bb2e-4e6b22d194ad', NULL, NULL, 0.000, NULL, 'N', NULL, NULL, NULL, NULL, 'N', NULL, '2', '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368901, 1, 1852714563165454338, 'a4095f0e-1950-4d4c-b886-4a3035b645c2', '审批人1', 'user:1', 0.000, NULL, 'N', 'assignment,create', 'com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener@@com.sykj.seaflow.warm.core.listener.GlobalCreateListener', NULL, NULL, 'N', NULL, '2', '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368902, 1, 1852714563165454338, '091944fb-4f6f-4eeb-82d9-1ea3da22bd52', '审批人2', 'user:2', 0.000, NULL, 'N', 'assignment,create', 'com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener@@com.sykj.seaflow.warm.core.listener.GlobalCreateListener', NULL, NULL, 'N', NULL, '2', '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368903, 4, 1852714563165454338, '312c3bf3-cd05-4d89-8d8d-9babadbf4970', '并行分支', NULL, 0.000, NULL, 'N', NULL, NULL, NULL, NULL, 'N', NULL, '2', '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368904, 1, 1852714563165454338, '32610c7f-21f1-43c5-821b-f65e3fcb917b', '审批人1', 'user:2', 0.000, NULL, 'N', 'assignment,create', 'com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener@@com.sykj.seaflow.warm.core.listener.GlobalCreateListener', NULL, NULL, 'N', NULL, '2', '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368905, 2, 1852714563165454338, 'fa13aeeb-f794-43c8-a2aa-27da340b9696', '结束', NULL, 0.000, NULL, 'N', NULL, NULL, NULL, NULL, 'N', NULL, '2', '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368906, 0, 1852714563165454338, '801e3c73-e665-4f6f-8ea7-d4de0927214c', '开始节点', 'warmFlowInitiator', 0.000, NULL, 'N', 'assignment,create', 'com.sykj.seaflow.warm.core.listener.GlobalAssignmentListener@@com.sykj.seaflow.warm.core.listener.GlobalCreateListener', NULL, NULL, 'N', NULL, '2', '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1');
/*!40000 ALTER TABLE `flow_node` ENABLE KEYS */;

-- 导出  表 seaflow.flow_node_ext 结构
CREATE TABLE IF NOT EXISTS `flow_node_ext` (
  `id` varchar(40) COLLATE utf8_bin DEFAULT NULL COMMENT '节点id, 和流程的节点id一致',
  `tenant_id` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `empty_approve` text COLLATE utf8_bin,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `node_code` varchar(40) COLLATE utf8_bin DEFAULT NULL COMMENT '节点id',
  `node_type` tinyint(4) DEFAULT NULL,
  `def_id` bigint(20) DEFAULT NULL COMMENT '流程定义id',
  `del_flag` char(50) COLLATE utf8_bin DEFAULT NULL,
  `back_type` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `back_type_node` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `direction` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '并行网关，入出方向',
  `button_json` varchar(512) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='流程节点配置扩展';

-- 正在导出表  seaflow.flow_node_ext 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `flow_node_ext` DISABLE KEYS */;
INSERT INTO `flow_node_ext` (`id`, `tenant_id`, `create_user`, `update_user`, `empty_approve`, `create_time`, `update_time`, `node_code`, `node_type`, `def_id`, `del_flag`, `back_type`, `back_type_node`, `direction`, `button_json`) VALUES
	('1852714563807182850', 1, 1, NULL, '{"type":"AUTO","value":[]}', '2024-11-02 22:09:08', NULL, 'f1c63412-5710-446d-a5ac-f441f37a22c2', 1, 1852714563165454338, '0', '1', '801e3c73-e665-4f6f-8ea7-d4de0927214c', NULL, '[{"type":"aggren","checked":true,"text":"同意"},{"type":"reject","checked":true,"text":"拒绝"},{"type":"back","checked":false,"text":"回退"},{"type":"transfer","checked":false,"text":"转办"},{"type":"depute","checked":false,"text":"委派"},{"type":"signAdd","checked":false,"text":"加签"},{"type":"signRedu","checked":false,"text":"减签"}]'),
	('1852714563870097409', 1, 1, NULL, NULL, '2024-11-02 22:09:08', NULL, '3483a491-0765-4b54-8c35-dbacb29e5bbf', 3, 1852714563165454338, '0', NULL, NULL, NULL, NULL),
	('1852714563870097410', 1, 1, NULL, '{"type":"EMPTY","value":[]}', '2024-11-02 22:09:08', NULL, 'f36a09c6-08ab-4027-85f9-db1f8a55f57c', 1, 1852714563165454338, '0', NULL, NULL, NULL, NULL),
	('1852714563870097411', 1, 1, NULL, NULL, '2024-11-02 22:09:08', NULL, '743a2719-639c-4371-bb2e-4e6b22d194ad', 4, 1852714563165454338, '0', NULL, NULL, '1', NULL),
	('1852714563870097412', 1, 1, NULL, '{"type":"AUTO","value":[]}', '2024-11-02 22:09:08', NULL, 'a4095f0e-1950-4d4c-b886-4a3035b645c2', 1, 1852714563165454338, '0', '1', '801e3c73-e665-4f6f-8ea7-d4de0927214c', NULL, '[{"type":"aggren","checked":true,"text":"同意"},{"type":"reject","checked":true,"text":"拒绝"},{"type":"back","checked":false,"text":"回退"},{"type":"transfer","checked":false,"text":"转办"},{"type":"depute","checked":false,"text":"委派"},{"type":"signAdd","checked":false,"text":"加签"},{"type":"signRedu","checked":false,"text":"减签"}]'),
	('1852714563870097413', 1, 1, NULL, '{"type":"AUTO","value":[]}', '2024-11-02 22:09:08', NULL, '091944fb-4f6f-4eeb-82d9-1ea3da22bd52', 1, 1852714563165454338, '0', '1', '801e3c73-e665-4f6f-8ea7-d4de0927214c', NULL, '[{"type":"aggren","checked":true,"text":"同意"},{"type":"reject","checked":true,"text":"拒绝"},{"type":"back","checked":false,"text":"回退"},{"type":"transfer","checked":false,"text":"转办"},{"type":"depute","checked":false,"text":"委派"},{"type":"signAdd","checked":false,"text":"加签"},{"type":"signRedu","checked":false,"text":"减签"}]'),
	('1852714563870097414', 1, 1, NULL, NULL, '2024-11-02 22:09:08', NULL, '312c3bf3-cd05-4d89-8d8d-9babadbf4970', 4, 1852714563165454338, '0', NULL, NULL, NULL, NULL),
	('1852714563870097415', 1, 1, NULL, '{"type":"AUTO","value":[]}', '2024-11-02 22:09:08', NULL, '32610c7f-21f1-43c5-821b-f65e3fcb917b', 1, 1852714563165454338, '0', '1', '801e3c73-e665-4f6f-8ea7-d4de0927214c', NULL, '[{"type":"aggren","checked":true,"text":"同意"},{"type":"reject","checked":true,"text":"拒绝"},{"type":"back","checked":false,"text":"回退"},{"type":"transfer","checked":false,"text":"转办"},{"type":"depute","checked":false,"text":"委派"},{"type":"signAdd","checked":false,"text":"加签"},{"type":"signRedu","checked":false,"text":"减签"}]'),
	('1852714563870097416', 1, 1, NULL, NULL, '2024-11-02 22:09:08', NULL, 'fa13aeeb-f794-43c8-a2aa-27da340b9696', 2, 1852714563165454338, '0', NULL, NULL, NULL, NULL),
	('1852714563937206273', 1, 1, NULL, NULL, '2024-11-02 22:09:08', NULL, '801e3c73-e665-4f6f-8ea7-d4de0927214c', 0, 1852714563165454338, '0', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `flow_node_ext` ENABLE KEYS */;

-- 导出  表 seaflow.flow_skip 结构
CREATE TABLE IF NOT EXISTS `flow_skip` (
  `id` bigint(20) unsigned NOT NULL COMMENT '主键id',
  `definition_id` bigint(20) NOT NULL COMMENT '流程定义id',
  `now_node_code` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '当前流程节点的编码',
  `now_node_type` tinyint(1) DEFAULT NULL COMMENT '当前节点类型（0开始节点 1中间节点 2结束节点 3互斥网关 4并行网关）',
  `next_node_code` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '下一个流程节点的编码',
  `next_node_type` tinyint(1) DEFAULT NULL COMMENT '下一个节点类型（0开始节点 1中间节点 2结束节点 3互斥网关 4并行网关）',
  `skip_name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '跳转名称',
  `skip_type` varchar(40) COLLATE utf8_bin DEFAULT NULL COMMENT '跳转类型（PASS审批通过 REJECT退回）',
  `skip_condition` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '跳转条件',
  `coordinate` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '坐标',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志',
  `tenant_id` varchar(40) COLLATE utf8_bin DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='结点跳转关联表';

-- 正在导出表  seaflow.flow_skip 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `flow_skip` DISABLE KEYS */;
INSERT INTO `flow_skip` (`id`, `definition_id`, `now_node_code`, `now_node_type`, `next_node_code`, `next_node_type`, `skip_name`, `skip_type`, `skip_condition`, `coordinate`, `create_time`, `update_time`, `del_flag`, `tenant_id`) VALUES
	(1852714563228368908, 1852714563165454338, 'f1c63412-5710-446d-a5ac-f441f37a22c2', 1, '3483a491-0765-4b54-8c35-dbacb29e5bbf', 3, NULL, 'PASS', NULL, NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368909, 1852714563165454338, 'f1c63412-5710-446d-a5ac-f441f37a22c2', 1, '801e3c73-e665-4f6f-8ea7-d4de0927214c', 0, NULL, 'REJECT', NULL, NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368910, 1852714563165454338, '3483a491-0765-4b54-8c35-dbacb29e5bbf', 3, 'f36a09c6-08ab-4027-85f9-db1f8a55f57c', 1, '条件', 'PASS', '@@simple_and@@|[{"key":"days","cond":"ge","value":"3","next":"and"}]', NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368911, 1852714563165454338, '3483a491-0765-4b54-8c35-dbacb29e5bbf', 3, '32610c7f-21f1-43c5-821b-f65e3fcb917b', 1, '条件', 'PASS', '@@default@@|true', NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368912, 1852714563165454338, 'f36a09c6-08ab-4027-85f9-db1f8a55f57c', 1, '743a2719-639c-4371-bb2e-4e6b22d194ad', 4, NULL, 'PASS', NULL, NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368913, 1852714563165454338, '743a2719-639c-4371-bb2e-4e6b22d194ad', 4, 'a4095f0e-1950-4d4c-b886-4a3035b645c2', 1, NULL, 'PASS', NULL, NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368914, 1852714563165454338, '743a2719-639c-4371-bb2e-4e6b22d194ad', 4, '091944fb-4f6f-4eeb-82d9-1ea3da22bd52', 1, NULL, 'PASS', NULL, NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368915, 1852714563165454338, 'a4095f0e-1950-4d4c-b886-4a3035b645c2', 1, '312c3bf3-cd05-4d89-8d8d-9babadbf4970', 4, NULL, 'PASS', NULL, NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368916, 1852714563165454338, 'a4095f0e-1950-4d4c-b886-4a3035b645c2', 1, '801e3c73-e665-4f6f-8ea7-d4de0927214c', 0, NULL, 'REJECT', NULL, NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368917, 1852714563165454338, '091944fb-4f6f-4eeb-82d9-1ea3da22bd52', 1, '312c3bf3-cd05-4d89-8d8d-9babadbf4970', 4, NULL, 'PASS', NULL, NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368918, 1852714563165454338, '091944fb-4f6f-4eeb-82d9-1ea3da22bd52', 1, '801e3c73-e665-4f6f-8ea7-d4de0927214c', 0, NULL, 'REJECT', NULL, NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368919, 1852714563165454338, '312c3bf3-cd05-4d89-8d8d-9babadbf4970', 4, 'fa13aeeb-f794-43c8-a2aa-27da340b9696', 2, NULL, 'PASS', NULL, NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368920, 1852714563165454338, '32610c7f-21f1-43c5-821b-f65e3fcb917b', 1, 'fa13aeeb-f794-43c8-a2aa-27da340b9696', 2, NULL, 'PASS', NULL, NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368921, 1852714563165454338, '32610c7f-21f1-43c5-821b-f65e3fcb917b', 1, '801e3c73-e665-4f6f-8ea7-d4de0927214c', 0, NULL, 'REJECT', NULL, NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1'),
	(1852714563228368922, 1852714563165454338, '801e3c73-e665-4f6f-8ea7-d4de0927214c', 0, 'f1c63412-5710-446d-a5ac-f441f37a22c2', 1, NULL, 'PASS', NULL, NULL, '2024-11-02 22:09:08', '2024-11-02 22:09:08', '0', '1');
/*!40000 ALTER TABLE `flow_skip` ENABLE KEYS */;

-- 导出  表 seaflow.flow_task 结构
CREATE TABLE IF NOT EXISTS `flow_task` (
  `id` bigint(20) NOT NULL COMMENT '主键id',
  `definition_id` bigint(20) NOT NULL COMMENT '对应flow_definition表的id',
  `instance_id` bigint(20) NOT NULL COMMENT '对应flow_instance表的id',
  `node_code` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '节点编码',
  `node_name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '节点名称',
  `node_type` tinyint(1) NOT NULL COMMENT '节点类型（0开始节点 1中间节点 2结束节点 3互斥网关 4并行网关）',
  `form_custom` char(1) COLLATE utf8_bin DEFAULT 'N' COMMENT '审批表单是否自定义（Y是 N否）',
  `form_path` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '审批表单路径',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志',
  `tenant_id` varchar(40) COLLATE utf8_bin DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='待办任务表';

-- 正在导出表  seaflow.flow_task 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `flow_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `flow_task` ENABLE KEYS */;

-- 导出  表 seaflow.flow_user 结构
CREATE TABLE IF NOT EXISTS `flow_user` (
  `id` bigint(20) unsigned NOT NULL COMMENT '主键id',
  `type` char(1) COLLATE utf8_bin NOT NULL COMMENT '人员类型（1代办任务的审批人权限 2代办任务的转办人权限 3待办任务的委托人权限）',
  `processed_by` varchar(80) COLLATE utf8_bin DEFAULT NULL COMMENT '权限人',
  `associated` bigint(20) NOT NULL COMMENT '关联表id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(80) COLLATE utf8_bin DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志',
  `tenant_id` varchar(40) COLLATE utf8_bin DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_processed_type` (`processed_by`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='流程用户表';

-- 正在导出表  seaflow.flow_user 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `flow_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `flow_user` ENABLE KEYS */;

-- 导出  表 seaflow.sys_dept 结构
CREATE TABLE IF NOT EXISTS `sys_dept` (
  `id` bigint(20) DEFAULT NULL,
  `tenant_id` bigint(20) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '部门名称',
  `sort_num` int(11) DEFAULT NULL COMMENT '序号',
  `director` bigint(20) DEFAULT NULL COMMENT '主管',
  `level_code` varchar(50) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='部门';

-- 正在导出表  seaflow.sys_dept 的数据：~6 rows (大约)
/*!40000 ALTER TABLE `sys_dept` DISABLE KEYS */;
INSERT INTO `sys_dept` (`id`, `tenant_id`, `pid`, `create_user`, `update_user`, `create_time`, `update_time`, `name`, `sort_num`, `director`, `level_code`) VALUES
	(1, 1, 0, NULL, NULL, '2024-10-19 09:25:08', '2024-10-19 09:25:08', '武汉数演科技有限公司', 0, NULL, 'cmp'),
	(1847454723123961858, 1, 1, 1, NULL, '2024-10-19 09:48:25', NULL, '研发部', 1, 1, 'dept'),
	(1847460642243174402, 1, 1, 1, NULL, '2024-10-19 10:11:56', NULL, '技术部', 0, NULL, 'dept'),
	(1847460705468112898, 1, 1, 1, NULL, '2024-10-19 10:12:11', NULL, '‌市场部‌', 0, NULL, 'dept'),
	(1847460795972804610, 1, 1, 1, NULL, '2024-10-19 10:12:33', NULL, '‌财务部', 0, NULL, 'dept'),
	(1847461001728581634, 1, 1, 1, NULL, '2024-10-19 10:13:22', NULL, '人力资源部', 0, NULL, 'dept'),
	(1850202931080081409, 1, 1847454723123961858, 1, NULL, '2024-10-26 23:48:49', NULL, '研发测试部', 0, NULL, 'dept');
/*!40000 ALTER TABLE `sys_dept` ENABLE KEYS */;

-- 导出  表 seaflow.sys_dept_user 结构
CREATE TABLE IF NOT EXISTS `sys_dept_user` (
  `id` bigint(20) NOT NULL,
  `tenant_id` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `dept_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `unique` (`dept_id`,`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户部门信息表';

-- 正在导出表  seaflow.sys_dept_user 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `sys_dept_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_dept_user` ENABLE KEYS */;

-- 导出  表 seaflow.sys_identifier 结构
CREATE TABLE IF NOT EXISTS `sys_identifier` (
  `id` bigint(20) NOT NULL,
  `tenant_id` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单名称',
  `type` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '类型： 1 接口权限，2按钮权限',
  `code` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '标识符编码',
  `name` varchar(125) COLLATE utf8_bin DEFAULT NULL COMMENT '标识符名称',
  `sort_num` int(11) DEFAULT NULL COMMENT '排序号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='权限标识符';

-- 正在导出表  seaflow.sys_identifier 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `sys_identifier` DISABLE KEYS */;
INSERT INTO `sys_identifier` (`id`, `tenant_id`, `create_time`, `update_time`, `create_user`, `update_user`, `menu_id`, `type`, `code`, `name`, `sort_num`) VALUES
	(1851123328583704577, 1, '2024-10-29 12:46:08', NULL, 1, NULL, 1846765869433847809, '1', 'test', '测试', 0);
/*!40000 ALTER TABLE `sys_identifier` ENABLE KEYS */;

-- 导出  表 seaflow.sys_menu 结构
CREATE TABLE IF NOT EXISTS `sys_menu` (
  `id` bigint(20) DEFAULT NULL,
  `tenant_id` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '菜单名称',
  `type` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '菜单类型',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父id',
  `path` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '路由地址,其实就是组件名称',
  `component` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '组件地址',
  `params` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT '路由参数，根据参数不同，默认显示不同的数据',
  `icon` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '图表',
  `sort_num` int(11) DEFAULT NULL COMMENT '排序',
  `status` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '显示隐藏状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 正在导出表  seaflow.sys_menu 的数据：~18 rows (大约)
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` (`id`, `tenant_id`, `create_user`, `update_user`, `create_time`, `update_time`, `name`, `type`, `parent_id`, `path`, `component`, `params`, `icon`, `sort_num`, `status`) VALUES
	(1845456303395930113, 1, 1, 1, '2024-10-13 21:27:24', '2024-10-14 17:15:36', '系统管理', '1', 0, '/sys', 'sys', NULL, 'Tools', 5, '1'),
	(1845456482161360897, 1, 1, 1, '2024-10-13 21:28:07', '2024-10-21 12:48:02', '用户管理', '2', 1845456303395930113, '/sys/user', 'sys/user/index', NULL, NULL, 1, '1'),
	(1845665743562813442, 1, 1, NULL, '2024-10-14 11:19:39', NULL, '角色管理', '2', 1845456303395930113, '/sys/role', 'sys/role/index', NULL, NULL, 2, '1'),
	(1845665865403150337, 1, 1, 1, '2024-10-14 11:20:08', '2024-10-16 21:41:01', '菜单管理', '2', 1845456303395930113, '/sys/menu', 'sys/menu/index', NULL, NULL, 3, '1'),
	(1845755275595427842, 1, 1, NULL, '2024-10-14 17:15:25', NULL, '示例页面', '1', 0, '/demo', NULL, NULL, 'HelpFilled', 1, '1'),
	(1845755572162080770, 1, 1, NULL, '2024-10-14 17:16:36', NULL, '表格页面', '2', 1845755275595427842, '/table', 'table/index', NULL, NULL, 1, '1'),
	(1845755764395421698, 1, 1, 1, '2024-10-14 17:17:21', '2024-10-29 11:24:13', '树表页面', '2', 1845755275595427842, '/treeTable', 'treeTable/index', NULL, NULL, 2, '1'),
	(1845756324355977218, 1, 1, NULL, '2024-10-14 17:19:35', NULL, '流程管理', '1', 0, '/flow', NULL, NULL, 'Share', 2, '1'),
	(1845756550873559042, 1, 1, NULL, '2024-10-14 17:20:29', NULL, '流程分类', '2', 1845756324355977218, '/flow/category', 'flow/category/index', NULL, NULL, 1, '1'),
	(1845756690459996162, 1, 1, 1, '2024-10-14 17:21:02', '2024-10-16 21:41:36', '流程设计', '2', 1845756324355977218, '/flow/design', 'flow/design/index', NULL, NULL, 2, '1'),
	(1845757038041968641, 1, 1, NULL, '2024-10-14 17:22:25', NULL, 'OA办公', '1', 0, '/oa', NULL, NULL, 'Promotion', 3, '1'),
	(1845757201661767682, 1, 1, NULL, '2024-10-14 17:23:04', NULL, '发起申请', '2', 1845757038041968641, '/flow/approval', 'flow/approval/index', NULL, NULL, 1, '1'),
	(1845757313775513601, 1, 1, 1, '2024-10-14 17:23:31', '2024-10-14 17:24:25', '我的待办', '2', 1845757038041968641, '/flow/todo', 'flow/todo/index', NULL, NULL, 3, '1'),
	(1845757511306260481, 1, 1, NULL, '2024-10-14 17:24:18', NULL, '我发起的', '2', 1845757038041968641, '/flow/my', 'flow/my/index', NULL, NULL, 2, '1'),
	(1845757662183763969, 1, 1, NULL, '2024-10-14 17:24:54', NULL, '我的已办', '2', 1845757038041968641, '/flow/done', 'flow/done/index', NULL, NULL, 4, '1'),
	(1845757880480509954, 1, 1, NULL, '2024-10-14 17:25:46', NULL, '设计器', '1', 0, '/design', NULL, NULL, 'View', 6, '1'),
	(1845758451329478657, 1, 1, NULL, '2024-10-14 17:28:02', NULL, '表单设计', '2', 1845757880480509954, '/design/formcreate', 'design/formcreate/index', NULL, NULL, 0, '1'),
	(1846765869433847809, 1, 1, 1, '2024-10-17 12:11:09', '2024-10-17 14:42:16', '首页', '2', 0, '/home', 'home/index', NULL, 'HomeFilled', 0, '1'),
	(1847133611022307329, 1, 1, 1, '2024-10-18 12:32:26', '2024-10-18 12:32:43', '部门管理', '2', 1845456303395930113, '/sys/dept', 'sys/dept/index', NULL, NULL, 4, '1'),
	(1847471973935570946, 1, 1, NULL, '2024-10-19 10:56:58', NULL, '职位管理', '2', 1845456303395930113, '/sys/position', 'sys/position/index', NULL, NULL, 5, '1');
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;

-- 导出  表 seaflow.sys_position 结构
CREATE TABLE IF NOT EXISTS `sys_position` (
  `id` bigint(20) NOT NULL,
  `tenant_id` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `dept_id` bigint(20) NOT NULL,
  `position_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `sort_num` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='职位信息表';

-- 正在导出表  seaflow.sys_position 的数据：~4 rows (大约)
/*!40000 ALTER TABLE `sys_position` DISABLE KEYS */;
INSERT INTO `sys_position` (`id`, `tenant_id`, `create_user`, `update_user`, `create_time`, `update_time`, `dept_id`, `position_name`, `sort_num`) VALUES
	(1847476043295461378, 1, 1, NULL, '2024-10-19 11:13:08', NULL, 1, '总经理', 0),
	(1848747642384527362, 1, 1, NULL, '2024-10-22 23:26:01', NULL, 1847460795972804610, '会计', 0),
	(1850202760359325698, 1, 1, NULL, '2024-10-26 23:48:08', NULL, 1847454723123961858, '研发工程师', 0),
	(1850202970619785217, 1, 1, NULL, '2024-10-27 00:34:17', NULL, 1850202931080081409, '测试员', 0);
/*!40000 ALTER TABLE `sys_position` ENABLE KEYS */;

-- 导出  表 seaflow.sys_position_user 结构
CREATE TABLE IF NOT EXISTS `sys_position_user` (
  `id` bigint(20) NOT NULL,
  `tenant_id` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `position_id` bigint(20) DEFAULT NULL,
  `dept_ids` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `cmp_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户职位表';

-- 正在导出表  seaflow.sys_position_user 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `sys_position_user` DISABLE KEYS */;
INSERT INTO `sys_position_user` (`id`, `tenant_id`, `create_user`, `create_time`, `update_user`, `update_time`, `user_id`, `position_id`, `dept_ids`, `cmp_id`) VALUES
	(1850486503074660354, 1, 1, '2024-10-27 18:35:37', NULL, NULL, 1, 1847476043295461378, NULL, 1),
	(1851447341956644865, 1, 1, '2024-10-30 10:13:39', NULL, NULL, 1814307624207470594, 1850202760359325698, '1847454723123961858', 1),
	(1851447342019559425, 1, 1, '2024-10-30 10:13:39', NULL, NULL, 1814307624207470594, 1850202970619785217, '1847454723123961858,1850202931080081409', 1);
/*!40000 ALTER TABLE `sys_position_user` ENABLE KEYS */;

-- 导出  表 seaflow.sys_role 结构
CREATE TABLE IF NOT EXISTS `sys_role` (
  `id` bigint(20) DEFAULT NULL,
  `tenant_id` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `role_code` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `role_name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `role_desc` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `role_status` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `sort_num` int(11) DEFAULT NULL,
  `deleted` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '系统默认，不允许删除的'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='角色信息表';

-- 正在导出表  seaflow.sys_role 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` (`id`, `tenant_id`, `create_user`, `update_user`, `create_time`, `update_time`, `role_code`, `role_name`, `role_desc`, `role_status`, `sort_num`, `deleted`) VALUES
	(1812054642967375874, 1, 1, 1, '2024-07-13 17:21:08', '2024-07-13 18:04:39', 'admin', '超级管理员', '超管，不能删除', '1', 0, '1'),
	(1813120366464937986, 1, 1, NULL, '2024-07-16 15:55:56', NULL, 'common', '普通人员', '', '1', 0, '0'),
	(1813509485149945858, 1, 1, NULL, '2024-07-17 17:42:09', NULL, 'emptyUser', '空角色', '', '1', 0, '0');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;

-- 导出  表 seaflow.sys_role_user 结构
CREATE TABLE IF NOT EXISTS `sys_role_user` (
  `id` bigint(20) NOT NULL,
  `tenant_id` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`role_id`,`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户角色信息表';

-- 正在导出表  seaflow.sys_role_user 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `sys_role_user` DISABLE KEYS */;
INSERT INTO `sys_role_user` (`id`, `tenant_id`, `create_user`, `update_user`, `create_time`, `update_time`, `role_id`, `user_id`) VALUES
	(1812695992825999361, 1, 1, NULL, '2024-07-15 11:49:38', NULL, 1812054642967375874, 1),
	(1847940655908331521, 1, 1, NULL, '2024-10-20 17:59:20', NULL, 1813120366464937986, 1814307624207470594);
/*!40000 ALTER TABLE `sys_role_user` ENABLE KEYS */;

-- 导出  表 seaflow.sys_user 结构
CREATE TABLE IF NOT EXISTS `sys_user` (
  `id` bigint(20) DEFAULT NULL,
  `tenant_id` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `update_user` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `username` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `status` char(1) COLLATE utf8_bin DEFAULT NULL,
  `nick` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户表';

-- 正在导出表  seaflow.sys_user 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` (`id`, `tenant_id`, `create_user`, `update_user`, `create_time`, `update_time`, `username`, `password`, `status`, `nick`, `phone`, `email`) VALUES
	(1, 1, 1, 1, '2024-07-11 21:13:40', '2024-10-27 18:35:37', 'admin', 'admin', '1', '超管', '18888888', '123456@qq.com'),
	(2, 1, NULL, NULL, '2024-07-11 21:14:09', '2024-07-11 21:14:08', 'test', 'test', '1', '测试', NULL, NULL),
	(1814307624207470594, 1, 1, 1, '2024-07-19 22:33:40', '2024-10-30 10:13:39', 'seven', 'seven', '1', 'seven', '188888888', '');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
