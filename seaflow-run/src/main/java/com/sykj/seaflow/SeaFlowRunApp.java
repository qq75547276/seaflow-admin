package com.sykj.seaflow;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@MapperScan("com.sykj.seaflow.**.mapper")
@EnableAspectJAutoProxy(exposeProxy=true,proxyTargetClass=true)
@SpringBootApplication(exclude = { RedisAutoConfiguration.class })
@Controller
public class SeaFlowRunApp {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(SeaFlowRunApp.class, args);
        ConfigurableEnvironment environment = run.getEnvironment();
        String port = environment.getProperty("server.port");
        System.out.println("访问地址： http://localhost:" + port);
        System.out.println("seaflow start success!");
    }

    @RequestMapping("/vue/index")
    public String frontPage() {
        return "/vue/index";
    }

}