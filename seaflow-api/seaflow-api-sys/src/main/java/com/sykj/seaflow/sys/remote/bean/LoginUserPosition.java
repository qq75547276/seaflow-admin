package com.sykj.seaflow.sys.remote.bean;

import lombok.Data;

@Data
public class LoginUserPosition {
    private String deptIds;

    private Long deptId;

    private String deptName;

    private String orgName;
    public String getOrgName(){
        return orgName == null? orgName: orgName.replace(",", ">");
    }

    // 如果职位直接挂在公司下面， 公司也是部门
    public Long getDeptId(){
        if(deptIds != null && !deptIds.trim().isEmpty()){
            String[] split = deptIds.split(",");
            return Long.parseLong(split[split.length-1].trim());
        }
        return cmpId;
    }

    public String getDeptName(){
        if(deptName != null){
            String[] split = deptName.split(",");
            return split[split.length - 1];
        }
        return cmpName;
    }

    private String cmpName;

    private Long cmpId;
    private Long positionId;
    /**
     * 职位名称
     */
    private String positionName;

    /**
     * 排序号
     */
    private int sortNum;
}
