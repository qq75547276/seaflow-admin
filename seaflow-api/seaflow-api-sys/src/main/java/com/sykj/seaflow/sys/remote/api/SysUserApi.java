package com.sykj.seaflow.sys.remote.api;

import com.sykj.seaflow.sys.remote.bean.LoginUserInfo;
import com.sykj.seaflow.sys.remote.bean.SysUserBean;

import java.util.List;

/**
 * 用户相关
 */
public interface SysUserApi {


    public List<Long> getUserIdsByRole(List<String> roleCodes) ;

    public List<Long> getUserIdsByRole(String roleCode);

    public List<Long> getUserIdsByDept(List<String> deptIds) ;

    public List<SysUserBean> getUsers(List<Long> userIds) ;

    LoginUserInfo getUserByUserName(String username);

    List<SysUserBean> selectUWithOutUsers(List<String> userIds);
}

