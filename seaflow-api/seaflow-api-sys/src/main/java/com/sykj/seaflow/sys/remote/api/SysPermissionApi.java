package com.sykj.seaflow.sys.remote.api;

import com.sykj.seaflow.sys.remote.bean.LoginUserInfo;

public interface SysPermissionApi {

    public LoginUserInfo loadPermission();
}
