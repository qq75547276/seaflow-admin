package com.sykj.seaflow.sys.remote.bean;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class LoginUserInfo {
    private Long id;

    private String username;

    private String password;
    private String email;
    private String phone;
    private String status;
    private String nick;

    private List<String> roleList;

    private List<Long> deptList;

    private List<Long> posList;

    /**
     * 按钮权限
     */
    private List<String> btnList;

    /**
     * 接口权限
     */
    private List<String> permissionList;

    private List<LoginUserPosition> userPositions;
}
