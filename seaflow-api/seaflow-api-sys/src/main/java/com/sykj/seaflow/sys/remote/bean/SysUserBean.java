package com.sykj.seaflow.sys.remote.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysUserBean  {

    private Long id;

    private String nick;

    private String username;
    private String phone;
}
