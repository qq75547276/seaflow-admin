package com.sykj.seaflow.sys.remote.bean;

import lombok.Data;

@Data
public class Dept {


    private Long  id;
    /**
     * 部门/ 公司名称 名称
     */
    private String name;
    /**
     *  级别   cmp： 公司： dept: 部门
     */
    private String levelCode;
    /**
     * 父id
     */
    private Long pid;
    /**
     * 排序
     */
    private Integer sortNum;


    /**
     *  部门主管
     */
    private Long director;

    private String directorName;

}
