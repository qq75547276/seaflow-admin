package com.sykj.seaflow.common.base.holder;

public class TenantContextHolder {
    private static final ThreadLocal<Long> contextHolder = new ThreadLocal<Long>();

    private TenantContextHolder() {}



    public static void setTenantId(Long tenantId) {
        contextHolder.set(tenantId);
    }
    public static Long getTenantId() {
        Long l = contextHolder.get();
        return l == null ?  1L : l ;
    }
    public static void clearContext() {
        contextHolder.remove();
    }

}
