package com.sykj.seaflow.common.base.exception;

public class CommonException extends RuntimeException{
    private Integer code = 500;

    private String msg;
    public CommonException(String message, Throwable throwable) {
        super(message,throwable);
        this.msg = message;
    }
    public CommonException() {
        super();
    }
    public CommonException(String message) {
        super(message);
        this.msg = message;
    }
    public CommonException(Integer code, String message) {
        super(message);
        this.code = code;
        this.msg = message;
    }

    public CommonException(String message, Object ... args) {
        super(String.format(message, args));
        this.msg = String.format(message, args);
    }

    public String getMsg() {
        return msg;
    }

    public Integer getCode() {
        return code;
    }
}
