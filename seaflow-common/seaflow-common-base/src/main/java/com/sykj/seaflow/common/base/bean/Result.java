package com.sykj.seaflow.common.base.bean;


import cn.hutool.http.HttpStatus;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 表格分页数据对象
 *
 * @author Lion Li
 */

@Data
@NoArgsConstructor
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 总记录数
     */
    private long total;

    /**
     * 列表数据
     */
    private List<T> rows;

    /**
     * 消息状态码
     */
    private int code;

    /**
     * 消息内容
     */
    private String msg;

    private Object data;

    /**
     * 分页
     *
     * @param list  列表数据
     * @param total 总记录数
     */
    public Result(List<T> list, long total) {
        this.rows = list;
        this.total = total;
    }

    public static <T> Result<T> build(IPage<T> page) {
        Result<T> rspData = new Result<>();
        rspData.setCode(HttpStatus.HTTP_OK);
        rspData.setRows(page.getRecords());
        rspData.setTotal(page.getTotal());
        return rspData;
    }

    public static <T> Result<T> build(List<T> list) {
        Result<T> rspData = new Result<>();
        rspData.setCode(HttpStatus.HTTP_OK);
        rspData.setRows(list);
        rspData.setTotal(list == null? 0 : list.size());
        return rspData;
    }
    public static <T> Result<T> build(T t, String msg) {
        Result<T> rspData = new Result<>();
        rspData.setCode(HttpStatus.HTTP_OK);
        rspData.setMsg(msg);
        rspData.setData(t);
        return rspData;
    }
    public static <T> Result<T> build(T t) {
        Result<T> rspData = new Result<>();
        rspData.setCode(HttpStatus.HTTP_OK);
        rspData.setData(t);
        return rspData;
    }

    public static <T> Result<T> build() {
        Result<T> rspData = new Result<>();
        rspData.setCode(HttpStatus.HTTP_OK);
        return rspData;
    }

    public static <T> Result<T> error(int code ,String msg) {
        Result<T> rspData = new Result<>();
        rspData.setCode(code);
        rspData.setMsg(msg);
        return rspData;
    }
    public static <T> Result<T> success( String msg) {
        Result<T> rspData = new Result<>();
        rspData.setCode(HttpStatus.HTTP_OK);
        rspData.setMsg(msg);
        return rspData;
    }

}
