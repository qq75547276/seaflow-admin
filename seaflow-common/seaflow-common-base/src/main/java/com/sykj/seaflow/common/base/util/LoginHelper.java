package com.sykj.seaflow.common.base.util;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.json.JSONObject;

public class LoginHelper {
    public static final String LOGIN_INFO = "loginInfo";
//
//    public static Long getUserName() {
//        JSONObject extra = (JSONObject) StpUtil.getExtra(LOGIN_INFO);
//        if (extra != null) {
//            return extra.getLong("username");
//        }
//        return null;
//    }

    public static Long getUserId() {
        return StpUtil.getLoginIdAsLong();
    }

    public static String getUserIdAsString() {
        return StpUtil.getLoginIdAsString();
    }


}
