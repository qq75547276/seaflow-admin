package com.sykj.seaflow.common.base.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.constant.TransType;
import com.fhs.core.trans.vo.TransPojo;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Date;
@Getter
@Setter
@Slf4j
public class BaseEntity implements Serializable, TransPojo {

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    /**
     * 租户id
     */
    private Long tenantId;
    /**
     *  创建人
     */
    @TableField(fill = FieldFill.INSERT)
    @Trans(type = TransType.AUTO_TRANS, key="userTransCreate")
    private Long createUser;

//    @TableField(exist = false)
//    private String createUserName;

    /**
     * 更新人
     */
    @TableField(fill = FieldFill.UPDATE)
    @Trans(type = TransType.AUTO_TRANS, key="userTransUpdate")
    private Long updateUser;
    /**
     *  创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    /**
     *  更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

}
