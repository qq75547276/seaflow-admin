package com.sykj.seaflow.common.base.bean;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import java.util.*;
import java.util.Collections;
import java.util.stream.Collectors;
@Data
public class PageQuery<T> {

    private Integer current = 1;
    private Integer size = 10;
    /**
     * 排序字段
     */
    private TreeMap<String, String> orderBy = new TreeMap<>();


    public Page<T>  getPage(){
       return this.getPageAndSort(false);
    }

    public Page<T> getPageAndSort(){
        return getPageAndSort(true);
    }
    private   Page<T> getPageAndSort(boolean defaultOrder){
        Page<T> page = new Page<>(current, size);

        if(!orderBy.isEmpty()){
            Set<Map.Entry<String, String>> entries = orderBy.entrySet();
            List<OrderItem> collect = entries.stream().map(r -> new OrderItem().setColumn(r.getKey()).setAsc("asc".equals(r.getValue())))
                    .collect(Collectors.toList());
            page.setOrders(collect);
        }else{
            // 如果指定默认排序， 就是id 倒叙
            if(defaultOrder){
                OrderItem orderItem = new OrderItem().setColumn("id").setAsc(false);
                page.setOrders(Collections.singletonList(orderItem));
            }
        }
        return page;
    }


}
