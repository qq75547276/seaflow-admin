package com.sykj.seaflow.common.base.handler;

import cn.dev33.satoken.exception.NotLoginException;
import cn.hutool.http.HttpStatus;
import lombok.extern.slf4j.Slf4j;
import com.sykj.seaflow.common.base.bean.Result;
import com.sykj.seaflow.common.base.exception.CommonException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@ResponseBody
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 更多异常 后续再添加， 也不知道还有多少需要添加的
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    public Result<String> golbalException(Exception e ) {
        e.printStackTrace();
        log.error(e.getMessage(),e);
        if(e instanceof NotLoginException){
            return Result.error(HttpStatus.HTTP_UNAUTHORIZED, "token已过期，请重新登录");
        }
        else if(e instanceof CommonException){
            return Result.error(((CommonException) e).getCode(), ((CommonException) e).getMsg());
        }

        return Result.error(500, e.getMessage());
    }
}
