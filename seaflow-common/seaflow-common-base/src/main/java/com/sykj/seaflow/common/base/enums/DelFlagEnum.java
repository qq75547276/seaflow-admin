package com.sykj.seaflow.common.base.enums;

import lombok.Getter;

@Getter
public enum DelFlagEnum {

    NORMAL("0", "正常"),
    DELETE("2", "删除");

    private final String value;
    private final String name;

    DelFlagEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }



}
