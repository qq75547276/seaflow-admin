package com.sykj.seaflow.common.mp.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.*;
import com.sykj.seaflow.common.mp.handler.SeaFlowTenantHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MybatisPlusConfig {

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(TenantLineInnerInterceptor tenantLineInnerInterceptor) {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();

        // 添加租户插件
        interceptor.addInnerInterceptor(tenantLineInnerInterceptor);
        // 添加非法SQL拦截器
//        interceptor.addInnerInterceptor(new IllegalSQLInnerInterceptor());
        //防全表更新与删除插件
        interceptor.addInnerInterceptor(new BlockAttackInnerInterceptor());
        //乐观锁插件
        interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        // 添加分页插件  放在最后
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor());

        return interceptor;
    }
//    @Bean
//    public seaflowTenantHandler seaflowTenantHandler() {
//        return new seaflowTenantHandler();
//    }

    @Bean
    public TenantLineInnerInterceptor tenantInterceptor(SeaFlowTenantHandler seaflowTenantHandler) {
        TenantLineInnerInterceptor interceptor = new TenantLineInnerInterceptor();
        interceptor.setTenantLineHandler(seaflowTenantHandler);
        return interceptor;
    }

}

