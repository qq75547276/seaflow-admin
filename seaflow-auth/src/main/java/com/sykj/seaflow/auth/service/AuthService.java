package com.sykj.seaflow.auth.service;

import com.sykj.seaflow.auth.bean.LoginRequest;
import com.sykj.seaflow.auth.bean.RegisterRequest;
import com.sykj.seaflow.sys.remote.bean.LoginUserInfo;

import javax.xml.transform.Result;
import java.util.List;

/**
 * 认证
 */
public interface AuthService {

    String login(LoginRequest loginRequest);

    Result register(RegisterRequest registerRequest);

    LoginUserInfo getUserInfo();
}
