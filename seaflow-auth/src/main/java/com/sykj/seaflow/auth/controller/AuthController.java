package com.sykj.seaflow.auth.controller;
import com.sykj.seaflow.common.base.util.LoginHelper;
import com.sykj.seaflow.sys.remote.bean.LoginUserInfo;
import lombok.AllArgsConstructor;
import com.sykj.seaflow.auth.service.AuthService;
import com.sykj.seaflow.auth.bean.LoginRequest;
import com.sykj.seaflow.auth.bean.RegisterRequest;
import com.sykj.seaflow.common.base.bean.Result;
import org.springframework.web.bind.annotation.*;


@AllArgsConstructor
@RestController
@RequestMapping("/auth")
public class AuthController {
    private final AuthService authService;

    @PostMapping("/doLogin")
    public Result<String> login(@RequestBody LoginRequest loginRequest) {
        String tip  = "欢迎回来";
        return Result.build(authService.login(loginRequest), tip);
    }

    @PostMapping("/register")
    public Result<String> register(@RequestBody RegisterRequest registerRequest) {
//        return Result.build(authService.register(registerRequest));
        return Result.build();
    }
    @GetMapping("/getUserInfo")
    public Result<LoginUserInfo> getUserInfo(){
        return Result.build(authService.getUserInfo());
    }



}
