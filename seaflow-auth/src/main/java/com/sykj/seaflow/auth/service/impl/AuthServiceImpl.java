package com.sykj.seaflow.auth.service.impl;

import cn.dev33.satoken.stp.SaLoginConfig;
import cn.dev33.satoken.stp.SaLoginModel;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.ObjectUtil;
import com.sykj.seaflow.auth.service.AuthService;
import com.sykj.seaflow.auth.bean.LoginRequest;
import com.sykj.seaflow.auth.bean.RegisterRequest;
import com.sykj.seaflow.common.base.exception.CommonException;
import com.sykj.seaflow.common.base.util.LoginHelper;
import com.sykj.seaflow.sys.remote.api.SysPermissionApi;
import com.sykj.seaflow.sys.remote.api.SysUserApi;
import com.sykj.seaflow.sys.remote.bean.LoginUserInfo;
import com.sykj.seaflow.sys.remote.bean.SysUserBean;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.transform.Result;

import static com.sykj.seaflow.common.base.util.LoginHelper.LOGIN_INFO;

@AllArgsConstructor
@Service
public class AuthServiceImpl implements AuthService {

    private final SysUserApi sysUserApi;
    private final SysPermissionApi sysPermissionApi;

    @Override
    public String login(LoginRequest loginRequest) {

        LoginUserInfo userInfo = sysUserApi.getUserByUserName(loginRequest.getUsername());
        if(ObjectUtil.isEmpty(loginRequest.getUsername())
        || ObjectUtil.isEmpty(loginRequest.getPassword())) {
            throw new CommonException("用户名或密码不能为空");
        }

        if(ObjectUtil.isNull(userInfo)) {
            throw new CommonException("用户不存在");
        }
        if(ObjectUtil.notEqual(userInfo.getStatus(),"1") ) {
            throw new CommonException("用户已停用");
        }
        if(ObjectUtil.isNotNull(userInfo) &&
                ObjectUtil.equal(userInfo.getPassword(),loginRequest.getPassword())){

            StpUtil.login(userInfo.getId());
            
           return StpUtil.getTokenInfo().getTokenValue();
        }

        throw new CommonException("用户名或密码错误");
    }

    @Override
    public LoginUserInfo getUserInfo() {
        return sysPermissionApi.loadPermission();
    }



    @Override
    public Result register(RegisterRequest registerRequest) {
        return null;
    }
}
