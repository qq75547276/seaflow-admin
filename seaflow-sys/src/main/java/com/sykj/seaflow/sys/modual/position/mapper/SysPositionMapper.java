package com.sykj.seaflow.sys.modual.position.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sykj.seaflow.sys.modual.position.entity.SysPosition;

public interface SysPositionMapper extends BaseMapper<SysPosition> {

}
