package com.sykj.seaflow.sys.modual.roleuser.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysRoleUserQuery {
    private Long roleId;

    private String keywords;
    private String phone;
}
