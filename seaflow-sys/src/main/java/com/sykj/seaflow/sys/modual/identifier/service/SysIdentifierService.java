package com.sykj.seaflow.sys.modual.identifier.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sykj.seaflow.sys.modual.identifier.entity.SysIdentifier;
import com.sykj.seaflow.sys.modual.identifier.params.SysIdentifierAddParam;
import com.sykj.seaflow.sys.modual.identifier.params.SysIdentifierEditParam;
import com.sykj.seaflow.sys.modual.identifier.params.SysIdentifierIdParam;
import com.sykj.seaflow.sys.modual.identifier.params.SysIdentifierQueryParam;

import java.util.List;

public interface SysIdentifierService extends IService<SysIdentifier> {

    public void add(SysIdentifierAddParam identifierAddParam);
    public void edit(SysIdentifierEditParam identifierEditParam);

    public void delete(List<SysIdentifierIdParam> ids);

    public SysIdentifier detail(SysIdentifierIdParam sysIdentifierIdParam);

    public List<SysIdentifier> list(SysIdentifierQueryParam identifierQueryParam);

    List<SysIdentifier> myIdentifer();
}
