package com.sykj.seaflow.sys.modual.positionuser.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysPositionUserQueryParam {
    private Long id;

    private Long userId;

    private Long positionId;
}
