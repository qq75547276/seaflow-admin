package com.sykj.seaflow.sys.modual.role.params;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysRoleQueryParam {
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色编码
     */
    private String roleCode;
    /**
     * 角色状态
     */
    private String roleStatus;

}
