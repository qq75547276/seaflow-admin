package com.sykj.seaflow.sys.modual.roleuser.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysRoleUserIdParam {
    private Long id;
}
