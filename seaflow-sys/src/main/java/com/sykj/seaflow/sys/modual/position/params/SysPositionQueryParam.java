package com.sykj.seaflow.sys.modual.position.params;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SysPositionQueryParam {

    private Long deptId;

    /**
     * 职位id
     */
    private Long id;

    /**
     * 职位名称
     */
    private String positionName;


}
