package com.sykj.seaflow.sys.modual.menu.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sykj.seaflow.sys.modual.menu.entity.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysMenuMapper extends BaseMapper<SysMenu>{


    List<SysMenu> selectMenus(@Param(Constants.WRAPPER) QueryWrapper<Object> query);
}
