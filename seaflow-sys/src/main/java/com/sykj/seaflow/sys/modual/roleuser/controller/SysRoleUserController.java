package com.sykj.seaflow.sys.modual.roleuser.controller;

import lombok.AllArgsConstructor;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.common.base.bean.Result;
import com.sykj.seaflow.sys.modual.roleuser.params.SysRoleUserIdParam;
import com.sykj.seaflow.sys.modual.roleuser.params.SysRoleUserQuery;
import com.sykj.seaflow.sys.modual.roleuser.service.SysRoleUserService;
import com.sykj.seaflow.sys.modual.user.entity.SysUser;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping({"/sys/role-user", "/sys/roleUser"})
public class SysRoleUserController {

    private final SysRoleUserService sysRoleUserService;

    @GetMapping("/page")
    public Result<SysUser> page(SysRoleUserQuery sysRoleUserQuery, PageQuery pageQuery) {
        return Result.build(sysRoleUserService.grantedGpage(sysRoleUserQuery, pageQuery));
    }
    @GetMapping("/canGrantPage")
    public Result<SysUser> canGrantPage(SysRoleUserQuery sysRoleUserQuery, PageQuery pageQuery) {
        return Result.build(sysRoleUserService.canGrantPage(sysRoleUserQuery, pageQuery));
    }

    @PostMapping("/revoke")
    public Result<Void> revoke(@RequestBody List<SysRoleUserIdParam> roleUserIdParams) {
        sysRoleUserService.revoke(roleUserIdParams);
        return Result.build();
    }

}
