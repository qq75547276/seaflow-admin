package com.sykj.seaflow.sys.modual.positionuser.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.sykj.seaflow.sys.modual.positionuser.entity.SysPositionUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PositionUserMapper extends BaseMapper<SysPositionUser> {

    public List<Long> getUsersByDeptIds(@Param(Constants.WRAPPER) Wrapper<SysPositionUser> wrapper);

}
