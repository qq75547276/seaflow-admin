package com.sykj.seaflow.sys.modual.dept.params;

import lombok.Data;

@Data
public class SysDeptPageParam {

    private String type;

    private String name;
}
