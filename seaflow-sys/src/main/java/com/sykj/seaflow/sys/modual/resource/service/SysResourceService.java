package com.sykj.seaflow.sys.modual.resource.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.sys.modual.position.entity.SysPosition;
import com.sykj.seaflow.sys.modual.position.params.SysPositionAddParam;
import com.sykj.seaflow.sys.modual.position.params.SysPositionEditParam;
import com.sykj.seaflow.sys.modual.position.params.SysPositionIdParam;
import com.sykj.seaflow.sys.modual.position.params.SysPositionQueryParam;
import com.sykj.seaflow.sys.modual.resource.entity.SysResource;
import com.sykj.seaflow.sys.modual.resource.params.SysResourceAddParam;
import com.sykj.seaflow.sys.modual.resource.params.SysResourceQueryParam;
import com.sykj.seaflow.sys.modual.role.entity.SysRole;

import java.util.List;

public interface SysResourceService extends IService<SysResource> {

    /**
     *
     */
    public void addBatch(SysResourceAddParam sysResourceAddParam);

    /**
     * 我所拥有的资源
     * @param sysRoles
     * @param sysPositions
     * @param userId
     * @return
     */
    List<SysResource> myResource(List<Long> sysRoles, List<Long> sysPositions, Long userId);

    List<SysResource> list(SysResourceQueryParam sysResourceQueryParam);
}
