package com.sykj.seaflow.sys.modual.resource.enums;

import lombok.Getter;

@Getter
public enum FromType {
    user("1"),role("2"),position("3");

    private final String value;

    FromType(String value){
        this.value = value;
    }

}
