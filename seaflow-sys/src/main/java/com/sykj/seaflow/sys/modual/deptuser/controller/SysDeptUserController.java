package com.sykj.seaflow.sys.modual.deptuser.controller;

import com.sykj.seaflow.sys.modual.deptuser.service.SysDeptUserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping({"/sys/dept-user", "/sys/deptUser"})
public class SysDeptUserController {

    private final SysDeptUserService SysDeptUserService;


    // 暂无
}
