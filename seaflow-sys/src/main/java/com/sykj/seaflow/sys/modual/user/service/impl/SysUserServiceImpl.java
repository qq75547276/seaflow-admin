package com.sykj.seaflow.sys.modual.user.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sykj.seaflow.common.base.exception.CommonException;
import com.sykj.seaflow.sys.modual.position.entity.SysPosition;
import com.sykj.seaflow.sys.modual.position.service.SysPositionService;
import com.sykj.seaflow.sys.modual.positionuser.entity.SysPositionUser;
import com.sykj.seaflow.sys.modual.positionuser.params.SysPositionUserQueryParam;
import com.sykj.seaflow.sys.modual.positionuser.service.PositionUserService;
import com.sykj.seaflow.sys.modual.roleuser.service.SysRoleUserService;
import com.sykj.seaflow.sys.modual.user.entity.SysUser;
import com.sykj.seaflow.sys.modual.user.mapper.SysUserMapper;
import com.sykj.seaflow.sys.modual.user.params.SysUserAddParam;
import com.sykj.seaflow.sys.modual.user.params.SysUserEditParam;
import com.sykj.seaflow.sys.modual.user.params.SysUserIdParam;
import com.sykj.seaflow.sys.modual.user.service.SysUserService;
import com.sykj.seaflow.sys.remote.bean.SysUserBean;
import lombok.AllArgsConstructor;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.sys.modual.user.params.SysUserQueryParam;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import static java.util.Arrays.stream;

@AllArgsConstructor
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    private final SysRoleUserService roleUserService;
    private final PositionUserService positionUserService;
    private final SysPositionService positionService;

    @Override
    public List<SysUser> list(SysUserQueryParam queryParam) {
        QueryWrapper<SysUser> queryWrapper = buildQueryWrapper(queryParam);
        return  this.list(queryWrapper);
    }

    @Override
    public IPage<SysUser> page(SysUserQueryParam queryParam, PageQuery<SysUser> pageQuery) {
        QueryWrapper<SysUser> query = Wrappers.query();
        query.like(ObjectUtil.isNotEmpty(queryParam.getUsername()), "user.user_name", queryParam.getUsername());
        query.like(ObjectUtil.isNotEmpty(queryParam.getNick()), "user.nick", queryParam.getNick());
        query.like(ObjectUtil.isNotEmpty(queryParam.getPhone()), "user.phone", queryParam.getPhone());
        query.eq(ObjectUtil.isNotEmpty(queryParam.getDeptId()), "pos.dept_id", queryParam.getDeptId());
        IPage<SysUser> sysUserIPage = this.getBaseMapper().selectPage(pageQuery.getPageAndSort(), query);
        List<SysUser> records = sysUserIPage.getRecords();
        records.forEach(r-> {
            SysPositionUserQueryParam sysPositionUserQueryParam = new SysPositionUserQueryParam();
            sysPositionUserQueryParam.setUserId(r.getId());
           List<SysPositionUser> positionUsers =   positionUserService.list(sysPositionUserQueryParam);
           r.setPositionList(positionUsers);
        });
        return sysUserIPage;
    }

    @Override
    public void add(SysUserAddParam sysUserAddParam) {
        SysUser sysUser = BeanUtil.copyProperties(sysUserAddParam, SysUser.class);

        this.save(sysUser);
        List<SysPositionUser> positionList = sysUser.getPositionList();
        if(ObjectUtil.isNotEmpty(positionList)){
            positionList.forEach(r-> {
                r.setUserId(sysUser.getId());
            });
            positionUserService.resave(sysUser.getId(), positionList);
        }
    }

    @Override
    public void edit(SysUserEditParam sysUserEditParam) {
        SysUser sysUser = BeanUtil.copyProperties(sysUserEditParam, SysUser.class);
        this.updateById(sysUser);
        List<SysPositionUser> positionList = sysUser.getPositionList();
        if(ObjectUtil.isNotEmpty(positionList)){
            positionList.forEach(r-> {
                r.setUserId(sysUser.getId());
            });
            positionUserService.resave(sysUser.getId(),positionList);
        }
    }

    @Transactional
    @Override
    public void delete(List<SysUserIdParam> idParams) {
        List<Long> ids = CollectionUtil.map(idParams, SysUserIdParam::getId, true);
        this.removeBatchByIds(ids);

        roleUserService.delByUserIds(ids);
    }

    @Override
    public List<SysUserBean> selectUWithOutUsers(List<String> userIds) {
        LambdaQueryWrapper<SysUser> queryWrapper = Wrappers.<SysUser>lambdaQuery().notIn(SysUser::getId, userIds);
        List<SysUser> list = this.list(queryWrapper);
        return BeanUtil.copyToList(list, SysUserBean.class);
    }

    /**
     *  查询用户信息 ，用于前端选择用户
     * @param queryParam
     * @return
     */
    @Override
    public List<SysUser> selectUsers(SysUserQueryParam queryParam) {
        QueryWrapper<SysUser> queryWrapper = Wrappers.query();
        queryWrapper.lambda().select(SysUser::getId, SysUser::getNick);
        queryWrapper.lambda().eq(SysUser::getStatus, "1");
        return this.list(queryParam);
    }

    @Override
    public SysUser detail(SysUserIdParam sysUserIdParam) {
        SysUser sysUser = this.getById(sysUserIdParam.getId());
        if(sysUser == null){
            throw new CommonException("用户id:{} 不存在", sysUserIdParam.getId());
        }
        SysPositionUserQueryParam sysPositionUserQueryParam = new SysPositionUserQueryParam();
        sysPositionUserQueryParam.setUserId(sysUser.getId());
        List<SysPositionUser> positionUsers =   positionUserService.list(sysPositionUserQueryParam);
        sysUser.setPositionList(positionUsers);
        return sysUser;
    }

    private QueryWrapper<SysUser> buildQueryWrapper(SysUserQueryParam queryParam) {
        QueryWrapper<SysUser> query = Wrappers.query();
        query.lambda().like(ObjectUtil.isNotEmpty(queryParam.getUsername()), SysUser::getUsername, queryParam.getUsername());
        query.lambda().like(ObjectUtil.isNotEmpty(queryParam.getNick()), SysUser::getNick, queryParam.getNick());
        return query;

    }
}

