package com.sykj.seaflow.sys.modual.user.params;


import lombok.Getter;
import lombok.Setter;
import java.util.List;
@Getter
@Setter
public class SysUserAddParam {

    private String username;

    private String password;

    private String email;

    private String phone;

    private String status;

    private String nick;

    private List<UserPosition> positionList;
}
