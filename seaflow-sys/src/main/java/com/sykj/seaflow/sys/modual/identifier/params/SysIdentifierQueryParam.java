package com.sykj.seaflow.sys.modual.identifier.params;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SysIdentifierQueryParam {

    private Long id;

    private Long menuId;
}
