package com.sykj.seaflow.sys.modual.resource.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.sykj.seaflow.common.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@TableName(value = "sys_resource")
public class SysResource extends BaseEntity {

    /**
     * 资源类别
     */
    private String resourceType;

    /**
     * 资源id
     */
    private Long resourceId;

    /**
     * 来源类别： 1:用户， 2：角色，3职位
     */
    private String fromType;
    /**
     * 来源类别对应id
     */
    private Long fromId;

}
