package com.sykj.seaflow.sys.modual.deptuser.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sykj.seaflow.sys.modual.deptuser.entity.SysDeptUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysDeptUserMapper extends BaseMapper<SysDeptUser>{


}
