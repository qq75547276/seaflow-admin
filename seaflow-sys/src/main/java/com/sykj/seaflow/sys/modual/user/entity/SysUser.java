package com.sykj.seaflow.sys.modual.user.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fhs.core.trans.vo.TransPojo;
import com.sykj.seaflow.sys.modual.positionuser.entity.SysPositionUser;
import lombok.Getter;
import lombok.Setter;
import com.sykj.seaflow.common.base.entity.BaseEntity;

import java.io.Serializable;
import java.util.List;
/**
 * 用户表
 */
@Setter
@Getter
@TableName(value = "sys_user", autoResultMap = true)
public class SysUser extends BaseEntity {
    private String username;
    private String password;
    private String email;
    private String phone;
    private String status;
    private String nick;

    @TableField(exist = false)
    private Long roleUserId;

    @TableField(exist = false)
    private List<SysPositionUser> positionList;

}
