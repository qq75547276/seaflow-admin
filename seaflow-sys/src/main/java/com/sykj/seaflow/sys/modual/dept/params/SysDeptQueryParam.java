package com.sykj.seaflow.sys.modual.dept.params;

import lombok.Data;

@Data
public class SysDeptQueryParam {

    private String type;

    private String name;
}
