package com.sykj.seaflow.sys.modual.dept.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sykj.seaflow.sys.modual.dept.entity.SysDept;

public interface SysDeptMapper extends BaseMapper<SysDept>{
}
