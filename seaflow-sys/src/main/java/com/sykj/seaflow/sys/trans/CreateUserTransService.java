package com.sykj.seaflow.sys.trans;

import com.fhs.core.trans.anno.AutoTrans;
import com.fhs.trans.service.AutoTransable;
import com.sykj.seaflow.sys.modual.user.entity.SysUser;
import com.sykj.seaflow.sys.modual.user.service.SysUserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
@AutoTrans(namespace = "userTransCreate",fields = {"nick"}, defaultAlias = "createUser")
public class CreateUserTransService implements AutoTransable<SysUser>{
    private final SysUserService sysUserService;

    @Override
    public SysUser selectById(Object primaryValue) {
        return  sysUserService.getById((Long) primaryValue);
    }
    @Override
    public List<SysUser> selectByIds(List<? extends Object> ids){
        return sysUserService.listByIds((List<Long>) ids);
    }
}
