package com.sykj.seaflow.sys.modual.deptuser.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.sykj.seaflow.common.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@TableName("sys_dept_user")
public class SysDeptUser extends BaseEntity {
    private Long deptId;

    private Long roleId;



}
