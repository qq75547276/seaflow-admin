package com.sykj.seaflow.sys.modual.menu.params;

import lombok.Data;

@Data
public class SysMenuIdParam {
    private Long id;
}
