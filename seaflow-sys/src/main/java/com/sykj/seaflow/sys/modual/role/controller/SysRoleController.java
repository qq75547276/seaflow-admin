package com.sykj.seaflow.sys.modual.role.controller;

import com.sykj.seaflow.sys.modual.role.params.*;
import com.sykj.seaflow.sys.modual.role.service.SysRoleService;
import lombok.AllArgsConstructor;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.common.base.bean.Result;
import com.sykj.seaflow.sys.modual.role.entity.SysRole;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/sys/role")
public class SysRoleController {
    private final SysRoleService sysRoleService;

    @PostMapping("/add")
    public void add(@RequestBody SysRoleAddParam sysRoleAddParam){
        sysRoleService.add(sysRoleAddParam);
    }

    @PostMapping("/grant/user")
    public void grantUser(@RequestBody List<SysRoleGrantUserParam> roleGrantUserParams){
        sysRoleService.grantUser(roleGrantUserParams);
    }

    @PostMapping("/revoke/user")
    public void revoke(@RequestBody List<SysRoleRevokeUserParam> revokeUserParams){
        sysRoleService.revokeUser(revokeUserParams);
    }

    @PostMapping("/edit")
    public void edit(@RequestBody SysRoleEditParam sysRoleEditParam){
        sysRoleService.edit(sysRoleEditParam);
    }

    @PostMapping("/disable")
    public void disable(@RequestBody List<SysRoleIdParam> sysRoleIdParams){
        sysRoleService.disable(sysRoleIdParams);
    }

    @PostMapping("/enable")
    public void enable(@RequestBody List<SysRoleIdParam> sysRoleIdParams){
        sysRoleService.enable(sysRoleIdParams);
    }
    @GetMapping("/page")
    public Result<SysRole> page(SysRoleQueryParam roleQueryParam, PageQuery<SysRole> pageQuery){
        return Result.build(sysRoleService.page(roleQueryParam, pageQuery));
    }

    @GetMapping("/list")
    public Result<SysRole> list(SysRoleQueryParam roleQueryParam){
        return Result.build(sysRoleService.list(roleQueryParam));
    }
    @PostMapping("/delete")
    public void delete(@RequestBody List<SysRoleIdParam> sysRoleIdParams){
        sysRoleService.delete(sysRoleIdParams);
    }
    @Deprecated() //"一直未使用，获取角色下的用户id,命名不正确"
    @GetMapping("/roleUserList")
    public Result<Long> roleUserList(SysRoleIdParam roleIdParam){
        return Result.build(sysRoleService.roleUserList(roleIdParam));
    }

}
