package com.sykj.seaflow.sys.modual.position.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.sys.modual.dept.entity.SysDept;
import com.sykj.seaflow.sys.modual.position.entity.SysPosition;
import com.sykj.seaflow.sys.modual.position.params.SysPositionAddParam;
import com.sykj.seaflow.sys.modual.position.params.SysPositionEditParam;
import com.sykj.seaflow.sys.modual.position.params.SysPositionIdParam;
import com.sykj.seaflow.sys.modual.position.params.SysPositionQueryParam;

import java.util.List;

public interface SysPositionService extends IService<SysPosition> {

    /**
     * 新增
     * @param sysPositionAddParam
     */
    public void add(SysPositionAddParam sysPositionAddParam);

    /**
     * 批量新增
     * @param positionAddParamList
     */
    public void addBatch(List<SysPositionAddParam> positionAddParamList);

    public List<SysPosition> list(SysPositionQueryParam sysPositionQueryParam);

    public IPage<SysPosition> page(PageQuery<SysPosition> pageQuery, SysPositionQueryParam sysPositionQueryParam);

    public void edit(SysPositionEditParam sysPositionEditParam);

    SysPosition detail(SysPositionIdParam sysPositionIdParam);

    public void deletes(List<SysPositionIdParam> sysPositionIdParams);

    List<SysPosition> myPosition();
}
