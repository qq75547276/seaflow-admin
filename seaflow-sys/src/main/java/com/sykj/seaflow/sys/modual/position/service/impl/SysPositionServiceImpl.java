package com.sykj.seaflow.sys.modual.position.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.common.base.util.LoginHelper;
import com.sykj.seaflow.sys.modual.position.entity.SysPosition;
import com.sykj.seaflow.sys.modual.position.mapper.SysPositionMapper;
import com.sykj.seaflow.sys.modual.position.params.SysPositionAddParam;
import com.sykj.seaflow.sys.modual.position.params.SysPositionEditParam;
import com.sykj.seaflow.sys.modual.position.params.SysPositionIdParam;
import com.sykj.seaflow.sys.modual.position.params.SysPositionQueryParam;
import com.sykj.seaflow.sys.modual.position.service.SysPositionService;
import com.sykj.seaflow.sys.modual.positionuser.entity.SysPositionUser;
import com.sykj.seaflow.sys.modual.positionuser.service.PositionUserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *   职位管理 service
 */
@AllArgsConstructor
@Service
public class SysPositionServiceImpl extends ServiceImpl<SysPositionMapper, SysPosition> implements SysPositionService {
    private final PositionUserService positionUserService;

    @Override
    public void add(SysPositionAddParam sysPositionAddParam) {
        SysPosition sysPosition = BeanUtil.copyProperties(sysPositionAddParam, SysPosition.class);
        this.save(sysPosition);
    }

    @Override
    public void addBatch(List<SysPositionAddParam> positionAddParamList) {
//            this.saveBatch()
    }

    @Override
    public List<SysPosition> list(SysPositionQueryParam sysPositionQueryParam) {
        QueryWrapper<SysPosition> queryWrapper = buildQueryWrapper(sysPositionQueryParam);
        return this.list(queryWrapper);
    }

    @Override
    public IPage<SysPosition> page(PageQuery<SysPosition> pageQuery, SysPositionQueryParam sysPositionQueryParam) {
        QueryWrapper<SysPosition> queryWrapper = buildQueryWrapper(sysPositionQueryParam);
        return this.page(pageQuery.getPage(), queryWrapper);
    }

    @Override
    public void edit(SysPositionEditParam sysPositionEditParam) {
        SysPosition sysPosition = BeanUtil.copyProperties(sysPositionEditParam, SysPosition.class);
        this.updateById(sysPosition);
    }

    @Override
    public SysPosition detail(SysPositionIdParam sysPositionIdParam) {

        return this.getById(sysPositionIdParam.getId());
    }

    @Transactional
    @Override
    public void deletes(List<SysPositionIdParam> sysPositionIdParams) {
        List<Long> list = CollStreamUtil.toList(sysPositionIdParams, SysPositionIdParam::getId);
        Assert.isFalse(positionUserService.exists(Wrappers.<SysPositionUser>lambdaQuery().in(SysPositionUser::getPositionId,list)),
                "职位下存在用户，无法删除");

        if(ObjectUtil.isNotEmpty(list)){
            this.removeBatchByIds(list);
        }
    }

    @Override
    public List<SysPosition> myPosition() {
        List<SysPositionUser> list = positionUserService.list(Wrappers.<SysPositionUser>lambdaQuery().eq(SysPositionUser::getUserId, LoginHelper.getUserId()));
        if(ObjectUtil.isNotEmpty(list)){
            List<Long> positionIds = CollStreamUtil.toList(list, SysPositionUser::getPositionId);
            return this.listByIds(positionIds);
        }
        return ListUtil.empty();
    }

    private QueryWrapper<SysPosition> buildQueryWrapper(SysPositionQueryParam sysPositionQueryParam){
        QueryWrapper<SysPosition> queryWrapper = Wrappers.query();
        queryWrapper.lambda().eq(ObjectUtil.isNotNull(sysPositionQueryParam.getDeptId()), SysPosition::getDeptId, sysPositionQueryParam.getDeptId());
        queryWrapper.lambda().eq(ObjectUtil.isNotNull(sysPositionQueryParam.getId()), SysPosition::getId, sysPositionQueryParam.getId());
        queryWrapper.lambda().eq(ObjectUtil.isNotEmpty(sysPositionQueryParam.getPositionName()), SysPosition::getPositionName, sysPositionQueryParam.getPositionName());
        return queryWrapper;
    }
}
