package com.sykj.seaflow.sys.modual.user.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysUserIdParam {
    private Long id;
}
