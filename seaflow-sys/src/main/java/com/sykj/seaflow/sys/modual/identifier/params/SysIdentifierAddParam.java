package com.sykj.seaflow.sys.modual.identifier.params;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class SysIdentifierAddParam {

    @NotNull(message = "menuId 不能为空")
    /**
     * 菜单Id
     */
    private Long menuId;


    /**
     * 权限编码
     */
    @NotEmpty(message = "编码 code 能为空")
    private String code;

    /**
     * 权限名称
     */
    @NotEmpty(message = "名称 name 能为空")
    private String name;

    /**
     * 排序号
     */
    private Integer sortNum;

    /**
     * 权限标识符类型： 1: 接口， 2： 按钮
     */
    @Pattern(regexp = "^[12]+$",message = "类型 type 只能为1或者2")
    private String type;
}
