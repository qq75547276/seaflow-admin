package com.sykj.seaflow.sys.modual.resource.enums;

import lombok.Getter;

@Getter
public enum ResourceType {

    menu("1"),identifier("2");


    private final String value;

    ResourceType(String value) {
        this.value = value;
    }
}
