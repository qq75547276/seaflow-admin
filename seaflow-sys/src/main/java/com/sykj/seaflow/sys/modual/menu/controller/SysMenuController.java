package com.sykj.seaflow.sys.modual.menu.controller;


import cn.hutool.core.lang.tree.Tree;
import com.sykj.seaflow.common.base.bean.Result;
import com.sykj.seaflow.sys.modual.menu.entity.SysMenu;
import com.sykj.seaflow.sys.modual.menu.params.*;
import com.sykj.seaflow.sys.modual.menu.service.SysMenuService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController {
    private SysMenuService sysMenuService;

    /**
     * 新增
     */
    @PostMapping("/add")
    public Result<Void> add(@RequestBody @Valid SysMenuAddParam sysMenuAddParam) {
        sysMenuService.add(sysMenuAddParam);
        return Result.build();
    }

    @PostMapping("/edit")
    public Result<Void> edit(@RequestBody @Valid SysMenuEditParam sysMenuAddParam) {
        sysMenuService.edit(sysMenuAddParam);
        return Result.build();
    }
    @GetMapping("/detail")
    public Result<SysMenu> detail(@Valid SysMenuIdParam sysMenuIdParam) {
        return Result.build(sysMenuService.detail(sysMenuIdParam));
    }

    @PostMapping("/delete")
    public Result<Void> delete(@RequestBody @Valid List<SysMenuIdParam> sysMenuAddParams) {
        sysMenuService.deletes(sysMenuAddParams);
        return Result.build();
    }

    /**
     * 菜单列表
     * @param sysMenuPageParam
     * @return
     */
    @GetMapping("/tree")
    public Result<Tree<String>> tree(SysMenuPageParam sysMenuPageParam) {
        List<Tree<String>> tree = sysMenuService.tree(sysMenuPageParam);
        return Result.build(tree);
    }

    /**
     * 用户 可以使用的路由 ，先直接返回
     * @param sysMenuPageParam
     * @return
     */
    @GetMapping("/menuRouter")
    public Result<Tree<String>> menuRouter(SysMenuPageParam sysMenuPageParam) {
        List<Tree<String>> tree = sysMenuService.tree(sysMenuPageParam);
        return Result.build(tree);
    }

    @GetMapping("/allMenus")
    public Result<Tree<String>> allMenus() {
        List<Tree<String>> trees = sysMenuService.allMenus();
        return Result.build(trees);
    }
//    @GetMapping("/list")
//    public Result<SysMenu> list(SysMenuQueryParam sysMenuPageParam) {
//        List<Tree<String>> tree = sysMenuService.list(sysMenuPageParam);
//        return Result.build(tree);
//    }
}
