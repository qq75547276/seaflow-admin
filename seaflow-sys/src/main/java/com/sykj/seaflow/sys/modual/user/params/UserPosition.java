package com.sykj.seaflow.sys.modual.user.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPosition {

    private Long userId;

    // 部门id(可能有多个)
    private String deptIds;

    // 公司id
    private Long cmpId;

    private Long positionId;

    private String orgName;

    private String cmpName;
}
