package com.sykj.seaflow.sys.modual.identifier.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.sykj.seaflow.common.base.entity.BaseEntity;
import lombok.Data;

@Data
@TableName("SYS_IDENTIFIER")
public class SysIdentifier extends BaseEntity {

    /**
     * 菜单Id
     */
    private Long menuId;


    /**
     * 权限编码
     */
    private String code;

    /**
     * 权限名称
     */
    private String name;

    /**
     * 排序号
     */
    private Integer sortNum;

    /**
     * 权限标识符类型： 1: 接口， 2： 按钮
     */
    private String type;
}
