package com.sykj.seaflow.sys.modual.dept.params;

import lombok.Data;

@Data
public class SysDeptIdParam {
    private Long id;
}
