package com.sykj.seaflow.sys.modual.dept.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fhs.core.trans.vo.TransPojo;
import com.sykj.seaflow.common.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@TableName(value = "sys_dept", autoResultMap = true)
public class SysDept extends BaseEntity implements TransPojo, Serializable {
    /**
     * 部门/ 公司名称 名称
     */
    private String name;
    /**
     *  级别   cmp： 公司： dept: 部门
     */
    private String levelCode;
    /**
     * 父id
     */
    private Long pid;
    /**
     * 排序
     */
    private Integer sortNum;


    /**
     *  部门主管
     */
    private Long director;

    @TableField(exist = false)
    private String directorName;



}
