package com.sykj.seaflow.sys.trans;

import com.fhs.core.trans.vo.VO;
import lombok.Data;

@Data
public class UserVo implements VO {

    private String createUserName;
}
