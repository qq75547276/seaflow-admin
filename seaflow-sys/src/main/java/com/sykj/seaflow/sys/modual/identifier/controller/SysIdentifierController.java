package com.sykj.seaflow.sys.modual.identifier.controller;


import cn.hutool.core.lang.tree.Tree;
import com.sykj.seaflow.common.base.bean.Result;
import com.sykj.seaflow.sys.modual.dept.entity.SysDept;
import com.sykj.seaflow.sys.modual.dept.params.*;
import com.sykj.seaflow.sys.modual.dept.service.SysDeptService;
import com.sykj.seaflow.sys.modual.identifier.entity.SysIdentifier;
import com.sykj.seaflow.sys.modual.identifier.params.SysIdentifierAddParam;
import com.sykj.seaflow.sys.modual.identifier.params.SysIdentifierEditParam;
import com.sykj.seaflow.sys.modual.identifier.params.SysIdentifierIdParam;
import com.sykj.seaflow.sys.modual.identifier.params.SysIdentifierQueryParam;
import com.sykj.seaflow.sys.modual.identifier.service.SysIdentifierService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/sys/identifier")
public class SysIdentifierController {
    private SysIdentifierService sysIdentifierService;

    /**
     * 新增
     */
    @PostMapping("/add")
    public Result<Void> add(@RequestBody @Valid SysIdentifierAddParam sysIdentifierAddParam) {
        sysIdentifierService.add(sysIdentifierAddParam);
        return Result.build();
    }

    @PostMapping("/edit")
    public Result<Void> edit(@RequestBody @Valid SysIdentifierEditParam sysIdentifierEditParam) {
        sysIdentifierService.edit(sysIdentifierEditParam);
        return Result.build();
    }
    @GetMapping("/detail")
    public Result<SysIdentifier> detail(@Valid SysIdentifierIdParam sysIdentifierIdParams) {
        return Result.build(sysIdentifierService.detail(sysIdentifierIdParams));
    }

    @GetMapping("/list")
    public Result<SysIdentifier> list(@Valid SysIdentifierQueryParam sysIdentifierQueryParam) {
        return Result.build(sysIdentifierService.list(sysIdentifierQueryParam));
    }

    @PostMapping("/delete")
    public Result<Void> delete(@RequestBody @Valid List<SysIdentifierIdParam> sysIdentifierIdParams) {
        sysIdentifierService.delete(sysIdentifierIdParams);
        return Result.build();
    }


}
