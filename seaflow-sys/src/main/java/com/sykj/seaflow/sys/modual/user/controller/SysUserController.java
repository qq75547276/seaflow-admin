package com.sykj.seaflow.sys.modual.user.controller;

import com.sykj.seaflow.sys.modual.user.entity.SysUser;
import com.sykj.seaflow.sys.modual.user.service.SysUserService;
import lombok.AllArgsConstructor;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.common.base.bean.Result;
import com.sykj.seaflow.sys.modual.user.params.SysUserAddParam;
import com.sykj.seaflow.sys.modual.user.params.SysUserEditParam;
import com.sykj.seaflow.sys.modual.user.params.SysUserIdParam;
import com.sykj.seaflow.sys.modual.user.params.SysUserQueryParam;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/sys/user")
public class SysUserController {
    private final SysUserService sysUserService;

    @GetMapping("list")
    public Result<SysUser> list(SysUserQueryParam queryParam){
        return Result.build( sysUserService.list(queryParam));
    }

    @GetMapping("page")
    public Result<SysUser> page(SysUserQueryParam queryParam, PageQuery<SysUser> pageQuery){
        return Result.build( sysUserService.page(queryParam, pageQuery));
    }
    @PostMapping("/add")
    public Result<Void> add(@RequestBody SysUserAddParam sysUserAddParam){
        sysUserService.add(sysUserAddParam);
        return Result.build();
    }

    @PostMapping("/edit")
    public Result<Void> edit(@RequestBody SysUserEditParam sysUserEditParam){
        sysUserService.edit(sysUserEditParam);
        return Result.build();
    }
    @PostMapping("/delete")
    public Result<Void> delete(@RequestBody List<SysUserIdParam> idParams){
        sysUserService.delete(idParams);
        return Result.build();
    }

    @GetMapping("/detail")
    public Result<SysUser> detail(SysUserIdParam  sysUserIdParam){
         SysUser sysUser = sysUserService.detail(sysUserIdParam);
        return Result.build(sysUser);
    }


    /**
     * 查询所有员工，  后续根据session的当前用户所在公司下的人员
     * @param queryParam
     * @return
     */
    @GetMapping("selectUsers")
    public Result<SysUser> selectUsers(SysUserQueryParam queryParam){
        List<SysUser> list = sysUserService.selectUsers(queryParam);
        return Result.build(list);
    }
}
