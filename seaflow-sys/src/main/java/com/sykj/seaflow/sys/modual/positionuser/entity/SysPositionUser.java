package com.sykj.seaflow.sys.modual.positionuser.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.constant.TransType;
import com.sykj.seaflow.common.base.entity.BaseEntity;
import com.sykj.seaflow.sys.modual.dept.entity.SysDept;
import com.sykj.seaflow.sys.modual.position.entity.SysPosition;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@TableName(value = "sys_position_user", autoResultMap = true)
public class SysPositionUser extends BaseEntity {

    private Long userId;
    @Trans(type =TransType.SIMPLE, target = SysPosition.class, fields = "positionName", ref = "positionName")
    private Long positionId;
    @TableField(exist = false)
    private String positionName;
    /**
     * 多级部门使用逗号分隔
     */
    @Trans(type = TransType.SIMPLE, target = SysDept.class,  fields = "name", ref = "orgName")

    private String deptIds;
    @Trans(type = TransType.SIMPLE, target = SysDept.class,  fields = "name", ref = "cmpName")
    private Long cmpId;
    @TableField(exist = false)
    private String orgName;
    public String getOrgName(){
        return orgName == null? orgName: orgName.replace(",", ">");
    }

    @TableField(exist = false)
    private String cmpName;

}
