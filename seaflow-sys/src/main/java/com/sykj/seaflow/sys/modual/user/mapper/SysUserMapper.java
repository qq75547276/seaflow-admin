package com.sykj.seaflow.sys.modual.user.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sykj.seaflow.sys.modual.user.entity.SysUser;
import org.apache.ibatis.annotations.Param;

public interface SysUserMapper extends BaseMapper<SysUser>{

    IPage<SysUser> selectPage(Page<SysUser> page, @Param(Constants.WRAPPER) QueryWrapper<SysUser> query);
}
