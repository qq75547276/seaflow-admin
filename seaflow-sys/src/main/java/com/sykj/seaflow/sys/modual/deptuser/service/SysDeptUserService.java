package com.sykj.seaflow.sys.modual.deptuser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.sys.modual.deptuser.entity.SysDeptUser;
import com.sykj.seaflow.sys.modual.role.params.SysRoleGrantUserBatchParam;
import com.sykj.seaflow.sys.modual.role.params.SysRoleGrantUserParam;
import com.sykj.seaflow.sys.modual.role.params.SysRoleRevokeUserParam;
import com.sykj.seaflow.sys.modual.user.entity.SysUser;

import java.util.List;

public interface SysDeptUserService extends IService<SysDeptUser> {



    List<SysDeptUser> listByDeptId(Long id);




    public List<SysDeptUser> listbyDeptIds(List<String> deptIds);
}
