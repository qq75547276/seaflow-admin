package com.sykj.seaflow.sys.modual.menu.params;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Setter
@Getter
public class SysMenuEditParam {
    private Long id;
    /**
     * 菜单名称
     */
    @NotEmpty(message = "菜单名称 不能为空")
    private String name;
    /**
     * 菜单类型
     */
    @NotEmpty(message = "菜单类型 不能为空")
    private String type;
    /**
     * 父id
     */
    private Long parentId;
    /**
     * 排序
     */
    private Integer sortNum;
    /**
     * 路由地址， 可以理解为路由时组件name
     */
    @NotEmpty(message = "路由地址 不能为空")
    private String path;

    /**
     * 组件地址  全部再src/views/ 下
     */
    private String component;
    /**
     * 参数
     */
    private String params;
    /**
     * 图标
     */
    private String icon;
    /**
     * 显示 隐藏 状态
     */
    private String status;


}
