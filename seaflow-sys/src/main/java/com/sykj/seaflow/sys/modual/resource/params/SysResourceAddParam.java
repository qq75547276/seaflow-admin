package com.sykj.seaflow.sys.modual.resource.params;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class SysResourceAddParam {
    @NotNull(message = "来源id 不能为null")
    private Long fromId;

    @NotEmpty(message = "来源类型不能为空")
    private String fromType;


    private List<ResourceParam> resourceList;
}


