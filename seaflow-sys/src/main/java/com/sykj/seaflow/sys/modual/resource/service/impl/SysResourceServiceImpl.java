package com.sykj.seaflow.sys.modual.resource.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sykj.seaflow.sys.modual.position.entity.SysPosition;
import com.sykj.seaflow.sys.modual.position.params.SysPositionAddParam;
import com.sykj.seaflow.sys.modual.resource.entity.SysResource;
import com.sykj.seaflow.sys.modual.resource.enums.FromType;
import com.sykj.seaflow.sys.modual.resource.enums.ResourceType;
import com.sykj.seaflow.sys.modual.resource.mapper.SysResourceMapper;
import com.sykj.seaflow.sys.modual.resource.params.ResourceParam;
import com.sykj.seaflow.sys.modual.resource.params.SysResourceAddParam;
import com.sykj.seaflow.sys.modual.resource.params.SysResourceQueryParam;
import com.sykj.seaflow.sys.modual.resource.service.SysResourceService;
import com.sykj.seaflow.sys.modual.role.entity.SysRole;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
@Service
public class SysResourceServiceImpl extends ServiceImpl<SysResourceMapper, SysResource> implements SysResourceService {


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addBatch(SysResourceAddParam sysResourceAddParam) {
        LambdaQueryWrapper<SysResource> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(SysResource::getFromType, sysResourceAddParam.getFromType());
        queryWrapper.eq(SysResource::getFromId, sysResourceAddParam.getFromId());
        this.getBaseMapper().delete(queryWrapper);
        if(ObjectUtil.isNotEmpty(sysResourceAddParam.getResourceList())){
            List<SysResource> list = new ArrayList<>();
            for (ResourceParam r : sysResourceAddParam.getResourceList()) {
                SysResource sysResource = new SysResource();
                sysResource.setResourceId(r.getResourceId());
                sysResource.setResourceType(r.getResourceType());
                sysResource.setFromType(sysResourceAddParam.getFromType());
                sysResource.setFromId(sysResourceAddParam.getFromId());
                list.add(sysResource);
            }
            this.saveBatch(list);
        }

    }

    /**
     * 获取标识信息
     * @param sysRoles
     * @param sysPositions
     * @param userId
     * @return
     */
    @Override
    public List<SysResource> myResource(List<Long> sysRoles, List<Long> sysPositions, Long userId) {
        QueryWrapper<SysResource> queryWrapper = Wrappers.query();
        queryWrapper.select("distinct resource_id, resource_type");
        queryWrapper.lambda().and(r->
                r.and(q-> q.in(ObjectUtil.isNotEmpty(sysRoles), SysResource::getFromId,sysRoles).eq(SysResource::getFromType, FromType.role.getValue()))
                        .or().and( q-> q.in(ObjectUtil.isNotEmpty(sysPositions), SysResource::getFromId,sysPositions).eq(SysResource::getFromType, FromType.position.getValue()))
                        .or().and(q-> q.eq(SysResource::getFromId,userId).eq(SysResource::getFromType, FromType.user.getValue())));

        queryWrapper.lambda().eq(SysResource::getResourceType, ResourceType.identifier.getValue());
        return this.list(queryWrapper);
    }

    @Override
    public List<SysResource> list(SysResourceQueryParam sysResourceQueryParam) {
        QueryWrapper<SysResource> queryWrapper = Wrappers.query();
        queryWrapper.select("distinct resource_id, resource_type");
        queryWrapper.lambda()
                .eq(SysResource::getFromType, sysResourceQueryParam.getFromType())
                .eq(SysResource::getFromId, sysResourceQueryParam.getFromId());
        return this.list(queryWrapper);
    }
}
