package com.sykj.seaflow.sys.modual.positionuser.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sykj.seaflow.sys.modual.positionuser.entity.SysPositionUser;
import com.sykj.seaflow.sys.modual.positionuser.mapper.PositionUserMapper;
import com.sykj.seaflow.sys.modual.positionuser.params.SysPositionUserQueryParam;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
public class PositionUserServiceImpl extends ServiceImpl<PositionUserMapper, SysPositionUser> implements PositionUserService {


    /**
     *  按条件查询
     * @param sysPositionUserQueryParam
     * @return
     */
    @Override
    public List<SysPositionUser> list(SysPositionUserQueryParam sysPositionUserQueryParam) {
        LambdaQueryWrapper<SysPositionUser> lambdaQuery = Wrappers.lambdaQuery();
        lambdaQuery.eq(ObjectUtil.isNotNull(sysPositionUserQueryParam.getUserId()),
                SysPositionUser::getUserId, sysPositionUserQueryParam.getUserId());
        lambdaQuery.eq(ObjectUtil.isNotNull(sysPositionUserQueryParam.getPositionId()),
                SysPositionUser::getPositionId, sysPositionUserQueryParam.getPositionId());
        return this.list(lambdaQuery);
    }

    @Transactional
    @Override
    public void resave(Long userId, List<SysPositionUser> positionList) {
        this.remove(Wrappers.<SysPositionUser>lambdaQuery().eq(SysPositionUser::getUserId, userId));
        this.saveBatch(positionList);
    }

    @Override
    public List<Long> getUsersByDeptIds(List<Long> deptIds) {
        return getBaseMapper().getUsersByDeptIds(Wrappers.<SysPositionUser>query().in("pos.dept_id", deptIds));
    }
}
