package com.sykj.seaflow.sys.modual.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sykj.seaflow.sys.modual.role.entity.SysRole;

public interface SysRoleMapper extends BaseMapper<SysRole>{
}
