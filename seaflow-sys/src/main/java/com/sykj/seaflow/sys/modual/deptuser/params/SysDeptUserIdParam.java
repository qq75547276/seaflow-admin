package com.sykj.seaflow.sys.modual.deptuser.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysDeptUserIdParam {
    private Long id;
}
