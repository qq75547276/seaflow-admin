package com.sykj.seaflow.sys.modual.user.params;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SysUserQueryParam {
    private String username;

    private String nick;


    private String phone;

    /**
     * 部门id
     */
    private String deptId;
}
