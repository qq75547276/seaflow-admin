package com.sykj.seaflow.sys.modual.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.sys.modual.user.entity.SysUser;
import com.sykj.seaflow.sys.modual.user.params.SysUserAddParam;
import com.sykj.seaflow.sys.modual.user.params.SysUserEditParam;
import com.sykj.seaflow.sys.modual.user.params.SysUserIdParam;
import com.sykj.seaflow.sys.modual.user.params.SysUserQueryParam;
import com.sykj.seaflow.sys.remote.bean.SysUserBean;

import java.util.List;

public interface SysUserService extends IService<SysUser> {

    public List<SysUser> list(SysUserQueryParam  queryParam);

    public IPage<SysUser> page(SysUserQueryParam queryParam, PageQuery<SysUser> pageQuery);

    public void add(SysUserAddParam sysUserAddParam);

    public void edit(SysUserEditParam sysUserEditParam);

    void delete(List<SysUserIdParam> idParams);

    List<SysUserBean> selectUWithOutUsers(List<String> userIds);

    List<SysUser> selectUsers(SysUserQueryParam queryParam);

    SysUser detail(SysUserIdParam sysUserIdParam);
}
