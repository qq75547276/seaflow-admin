package com.sykj.seaflow.sys.modual.role.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sykj.seaflow.common.base.util.LoginHelper;
import com.sykj.seaflow.sys.modual.role.mapper.SysRoleMapper;
import com.sykj.seaflow.sys.modual.role.params.*;
import com.sykj.seaflow.sys.modual.role.service.SysRoleService;
import lombok.AllArgsConstructor;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sykj.seaflow.sys.modual.role.entity.SysRole;
import com.sykj.seaflow.sys.modual.roleuser.entity.SysRoleUser;
import com.sykj.seaflow.sys.modual.roleuser.service.SysRoleUserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    private final SysRoleUserService roleUserService;

    @Override
    public void add(SysRoleAddParam sysRoleAddParam) {
        SysRole sysRole = BeanUtil.copyProperties(sysRoleAddParam, SysRole.class);
        sysRole.setDeleted("0");
        this.save(sysRole);
    }

    @Override
    public void edit(SysRoleEditParam sysRoleEditParam) {
        SysRole sysRole = BeanUtil.copyProperties(sysRoleEditParam, SysRole.class);
        this.updateById(sysRole);
    }

    @Override
    public void delete(List<SysRoleIdParam> roleIdParams) {
        List<Long> ids = CollectionUtil.map(roleIdParams, SysRoleIdParam::getId, true);
        UpdateWrapper<SysRole> update = Wrappers.update();
        // 不允许删除的角色
        update.lambda().ne(SysRole::getDeleted, "1");
        update.lambda().in(SysRole::getId, ids);
        this.removeByIds(ids);
    }

    @Override
    public void disable(List<SysRoleIdParam> roleIdParams) {
        List<Long> ids = CollectionUtil.map(roleIdParams, SysRoleIdParam::getId, true);
        LambdaUpdateWrapper<SysRole> updateWrapper = Wrappers.lambdaUpdate();
        updateWrapper.set(SysRole::getRoleStatus, 0);
        updateWrapper.in(SysRole::getId, ids);
        this.update(updateWrapper);
    }

    @Override
    public void enable(List<SysRoleIdParam> roleIdParams) {
        List<Long> ids = CollectionUtil.map(roleIdParams, SysRoleIdParam::getId, true);
        LambdaUpdateWrapper<SysRole> updateWrapper = Wrappers.lambdaUpdate();
        updateWrapper.set(SysRole::getRoleStatus, 1);
        updateWrapper.in(SysRole::getId, ids);
        this.update(updateWrapper);
    }

    @Override
    public IPage<SysRole> page(SysRoleQueryParam sysRoleQueryParam, PageQuery<SysRole> pageQuery) {
        LambdaQueryWrapper<SysRole> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(ObjectUtil.isNotEmpty(sysRoleQueryParam.getRoleName()),SysRole::getRoleName, sysRoleQueryParam.getRoleName());
        queryWrapper.like(ObjectUtil.isNotEmpty(sysRoleQueryParam.getRoleCode()),SysRole::getRoleCode, sysRoleQueryParam.getRoleCode());
        queryWrapper.eq(ObjectUtil.isNotEmpty(sysRoleQueryParam.getRoleStatus()), SysRole::getRoleStatus, sysRoleQueryParam.getRoleStatus());
        return this.page(pageQuery.getPageAndSort(), queryWrapper);
    }

    @Override
    public void grantUser(List<SysRoleGrantUserParam> roleGrantUserParams) {
        roleUserService.batchAdd(roleGrantUserParams);
    }

    @Override
    public void revokeUser(List<SysRoleRevokeUserParam> revokeUserParams) {
        roleUserService.delete(revokeUserParams);
    }

    @Override
    public List<Long> roleUserList(SysRoleIdParam roleIdParam) {
        List<SysRoleUser>  roleUsers = roleUserService.listByRoleId(roleIdParam.getId());
        return roleUsers.stream().map(SysRoleUser::getUserId).collect(Collectors.toList());//.toList();
    }

    @Override
    public List<SysRole> list(SysRoleQueryParam roleQueryParam) {
        LambdaQueryWrapper<SysRole> queryWrapper = Wrappers.lambdaQuery();
        return this.list(queryWrapper);
    }

    @Override
    public List<SysRole> myRoles() {
        Long userId = LoginHelper.getUserId();
        List<SysRoleUser> roleUsers = roleUserService.listByUserId(userId);
        if(ObjectUtil.isNotEmpty(roleUsers)){
            List<Long> list = CollStreamUtil.toList(roleUsers, SysRoleUser::getRoleId);
            return this.listByIds(list);
        };
        return ListUtil.empty();
    }
}
