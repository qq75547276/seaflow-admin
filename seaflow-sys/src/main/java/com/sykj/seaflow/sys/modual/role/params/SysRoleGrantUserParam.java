package com.sykj.seaflow.sys.modual.role.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysRoleGrantUserParam {

    private Long roleId;
    private Long userId;
}
