package com.sykj.seaflow.sys.modual.roleuser.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import com.sykj.seaflow.common.base.entity.BaseEntity;

@Setter
@Getter
@TableName("sys_role_user")
public class SysRoleUser extends BaseEntity {
    private Long userId;

    private Long roleId;



}
