package com.sykj.seaflow.sys.modual.identifier.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sykj.seaflow.sys.modual.identifier.entity.SysIdentifier;

import java.util.List;

public interface SysIdentifierMapper extends BaseMapper<SysIdentifier> {

    List<SysIdentifier> myIdentifer(QueryWrapper<SysIdentifier> wrapper);
}
