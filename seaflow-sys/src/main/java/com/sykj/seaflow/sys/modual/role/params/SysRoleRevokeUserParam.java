package com.sykj.seaflow.sys.modual.role.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysRoleRevokeUserParam {
    private Long roleUserId;
}
