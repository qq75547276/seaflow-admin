package com.sykj.seaflow.sys.modual.menu.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.sys.modual.menu.entity.SysMenu;
import com.sykj.seaflow.sys.modual.menu.params.SysMenuAddParam;
import com.sykj.seaflow.sys.modual.menu.params.SysMenuEditParam;
import com.sykj.seaflow.sys.modual.menu.params.SysMenuIdParam;
import com.sykj.seaflow.sys.modual.menu.params.SysMenuPageParam;
import com.sykj.seaflow.sys.modual.role.entity.SysRole;

import java.util.List;

public interface SysMenuService extends IService<SysMenu> {

    /**
     * 新增
     * @param sysMenuAddParam
     */
    public void add(SysMenuAddParam sysMenuAddParam);

    /**
     * 修改
     * @param sysMenuEditParam
     */
    public void edit(SysMenuEditParam sysMenuEditParam);

    /**
     * 分页
     * @param sysMenuPageParam
     * @param pageQuery
     * @return
     */
    public IPage<SysMenu> page(SysMenuPageParam sysMenuPageParam, PageQuery<SysMenu> pageQuery);

    /**
     * 根据id 查询
     */
    public SysMenu detail(SysMenuIdParam sysMenuIdParam);

    public void deletes(List<SysMenuIdParam> ids);

    public void delete(SysMenuIdParam sysMenuIdParam);

    public List<Tree<String>> tree(SysMenuPageParam sysMenuPageParam);


    List<Tree<String>> allMenus();
}
