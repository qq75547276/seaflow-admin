package com.sykj.seaflow.sys.modual.dept.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.sys.modual.dept.entity.SysDept;
import com.sykj.seaflow.sys.modual.dept.params.*;

import java.util.List;

public interface SysDeptService extends IService<SysDept> {

    /**
     * 新增
     * @param sysMenuAddParam
     */
    public void add(SysDeptAddParam sysMenuAddParam);

    /**
     * 修改
     * @param
     */
    public void edit(SysDeptEditParam sysDeptEditParam);

    /**
     * 分页
     * @param sysMenuPageParam
     * @param pageQuery
     * @return
     */
    public IPage<SysDept> page(SysDeptPageParam sysMenuPageParam, PageQuery<SysDept> pageQuery);

    /**
     * 根据id 查询
     */
    public SysDept detail(SysDeptIdParam sysDeptIdParam);

    public void deletes(List<SysDeptIdParam> ids);

    public void delete(SysDeptIdParam sysMenuIdParam);

    public List<Tree<String>> tree(SysDeptPageParam sysMenuPageParam);

    public List<SysDept> list(SysDeptQueryParam sysMenuQueryParam);


}
