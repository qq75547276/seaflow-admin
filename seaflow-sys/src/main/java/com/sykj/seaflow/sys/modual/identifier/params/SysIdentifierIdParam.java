package com.sykj.seaflow.sys.modual.identifier.params;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SysIdentifierIdParam {

    @NotNull(message = "主键id 不能为空")
    private Long id;
}
