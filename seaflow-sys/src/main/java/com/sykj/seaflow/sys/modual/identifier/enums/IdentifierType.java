package com.sykj.seaflow.sys.modual.identifier.enums;

import lombok.Getter;

@Getter
public enum IdentifierType {

    permission("1"),btn("2");

    private final String value;

    IdentifierType(String value) {
        this.value = value;
    }
}
