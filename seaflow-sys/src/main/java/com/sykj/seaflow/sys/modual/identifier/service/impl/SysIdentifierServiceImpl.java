package com.sykj.seaflow.sys.modual.identifier.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sykj.seaflow.common.base.util.LoginHelper;
import com.sykj.seaflow.sys.modual.identifier.entity.SysIdentifier;
import com.sykj.seaflow.sys.modual.identifier.mapper.SysIdentifierMapper;
import com.sykj.seaflow.sys.modual.identifier.params.SysIdentifierAddParam;
import com.sykj.seaflow.sys.modual.identifier.params.SysIdentifierEditParam;
import com.sykj.seaflow.sys.modual.identifier.params.SysIdentifierIdParam;
import com.sykj.seaflow.sys.modual.identifier.params.SysIdentifierQueryParam;
import com.sykj.seaflow.sys.modual.identifier.service.SysIdentifierService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class SysIdentifierServiceImpl extends ServiceImpl<SysIdentifierMapper, SysIdentifier> implements SysIdentifierService {

    @Override
    public void add(SysIdentifierAddParam identifierAddParam) {
        SysIdentifier sysIdentifier = BeanUtil.copyProperties(identifierAddParam, SysIdentifier.class);
        this.save(sysIdentifier);
    }

    @Override
    public void edit(SysIdentifierEditParam identifierEditParam) {
        SysIdentifier sysIdentifier = BeanUtil.copyProperties(identifierEditParam, SysIdentifier.class);
        this.updateById(sysIdentifier);
    }

    @Override
    public void delete(List<SysIdentifierIdParam> ids) {
        List<Long> list = CollStreamUtil.toList(ids, SysIdentifierIdParam::getId);
        if(ObjectUtil.isNotEmpty(list)){
            this.removeBatchByIds(list);
        }
    }

    @Override
    public SysIdentifier detail(SysIdentifierIdParam sysIdentifierIdParam) {

        return this.getById(sysIdentifierIdParam.getId());
    }

    @Override
    public List<SysIdentifier> list(SysIdentifierQueryParam identifierQueryParam) {
        QueryWrapper<SysIdentifier> queryWrapper = buildQueryWrap(identifierQueryParam);
        return this.list(queryWrapper);
    }

    @Override
    public List<SysIdentifier> myIdentifer() {
        QueryWrapper<SysIdentifier> queryWrapper = Wrappers.<SysIdentifier>query().eq("user_id", LoginHelper.getUserId());
        return this.getBaseMapper().myIdentifer(queryWrapper);
    }

    private QueryWrapper<SysIdentifier> buildQueryWrap(SysIdentifierQueryParam identifierQueryParam) {
        QueryWrapper<SysIdentifier> queryWrapper = Wrappers.query();
        queryWrapper.lambda().eq(ObjectUtil.isNotNull(identifierQueryParam.getMenuId()), SysIdentifier::getMenuId, identifierQueryParam.getMenuId());
        return queryWrapper;
    }
}
