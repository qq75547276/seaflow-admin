package com.sykj.seaflow.sys.modual.position.params;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SysPositionAddParam {
    /**
     * 部门id
     */
    @NotNull(message = "部门id 不能为空")
    private Long deptId;

    /**
     * 职位名称
     */
    @NotEmpty(message = "职位名称不能为空")
    private String positionName;

    /**
     * 排序号
     */
    private int sortNum;

}
