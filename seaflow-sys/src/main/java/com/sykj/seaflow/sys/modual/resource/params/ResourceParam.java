package com.sykj.seaflow.sys.modual.resource.params;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class ResourceParam {
    /**
     * 资源id
     */
    @NotNull(message = "资源id不能为空")
    private Long resourceId;

    /**
     * 资源类别
     */
    @NotEmpty(message = "资源类别不能为空")
    private String resourceType;

}
