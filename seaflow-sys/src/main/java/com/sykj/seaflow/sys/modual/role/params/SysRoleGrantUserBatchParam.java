package com.sykj.seaflow.sys.modual.role.params;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SysRoleGrantUserBatchParam {
    private Long roleId;

    private List<Long> userIds;
}
