package com.sykj.seaflow.sys.modual.resource.controller;


import com.sykj.seaflow.common.base.bean.Result;
import com.sykj.seaflow.sys.modual.resource.entity.SysResource;
import com.sykj.seaflow.sys.modual.resource.params.SysResourceAddParam;
import com.sykj.seaflow.sys.modual.resource.params.SysResourceQueryParam;
import com.sykj.seaflow.sys.modual.resource.service.SysResourceService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/sys/resource")
public class SysResourceController {
    private SysResourceService sysResourceService;

    @PostMapping("/add")
    public Result<Void> add(@RequestBody  @Valid SysResourceAddParam sysResourceAddParam) {
        sysResourceService.addBatch(sysResourceAddParam);
        return Result.build();
    }
    @GetMapping("/list")
    public Result<SysResource> list(@Valid SysResourceQueryParam sysResourceQueryParam) {
        List<SysResource> resourceList =  sysResourceService.list(sysResourceQueryParam);
        return Result.build(resourceList);
    }
}
