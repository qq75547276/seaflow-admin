package com.sykj.seaflow.sys.modual.dept.params;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Setter
@Getter
public class SysDeptAddParam {
    /**
     * 菜单名称
     */
    @NotEmpty(message = "名称 不能为空")
    private String name;
    /**
     * 菜单类型
     */
    @NotEmpty(message = "级别编码 不能为空")
    private String levelCode;
    /**
     * 父id
     */
    private Long pid;
    /**
     * 排序
     */
    private Integer sortNum;

    /**
     * 主管
     */
    private Long director;

}
