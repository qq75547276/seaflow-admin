package com.sykj.seaflow.sys.modual.position.controller;


import cn.hutool.core.lang.tree.Tree;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.common.base.bean.Result;
import com.sykj.seaflow.sys.modual.dept.entity.SysDept;
import com.sykj.seaflow.sys.modual.dept.params.*;
import com.sykj.seaflow.sys.modual.dept.service.SysDeptService;
import com.sykj.seaflow.sys.modual.position.entity.SysPosition;
import com.sykj.seaflow.sys.modual.position.params.SysPositionAddParam;
import com.sykj.seaflow.sys.modual.position.params.SysPositionEditParam;
import com.sykj.seaflow.sys.modual.position.params.SysPositionIdParam;
import com.sykj.seaflow.sys.modual.position.params.SysPositionQueryParam;
import com.sykj.seaflow.sys.modual.position.service.SysPositionService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/sys/position")
public class SysPositionController {
    private SysPositionService sysPositionService;

    /**
     * 新增
     */
    @PostMapping("/add")
    public Result<Void> add(@RequestBody @Valid SysPositionAddParam sysPositionAddParam) {
        sysPositionService.add(sysPositionAddParam);
        return Result.build();
    }

    @PostMapping("/edit")
    public Result<Void> edit(@RequestBody @Valid SysPositionEditParam sysPositionEditParam) {
        sysPositionService.edit(sysPositionEditParam);
        return Result.build();
    }
    @GetMapping("/detail")
    public Result<SysPosition> detail(@Valid SysPositionIdParam sysPositionIdParam) {
        return Result.build(sysPositionService.detail(sysPositionIdParam));
    }

    @GetMapping("/list")
    public Result<SysPosition> list(@Valid SysPositionQueryParam sysPositionQueryParam) {
        return Result.build(sysPositionService.list(sysPositionQueryParam));
    }

    @GetMapping("/page")
    public Result<SysPosition> page(PageQuery<SysPosition> pageQuery, @Valid SysPositionQueryParam sysPositionQueryParam) {
        return Result.build(sysPositionService.page(pageQuery, sysPositionQueryParam));
    }


    @PostMapping("/delete")
    public Result<Void> delete(@RequestBody @Valid List<SysPositionIdParam> sysPositionIdParams) {
        sysPositionService.deletes(sysPositionIdParams);
        return Result.build();
    }




//    @GetMapping("/list")
//    public Result<SysMenu> list(SysMenuQueryParam sysMenuPageParam) {
//        List<Tree<String>> tree = sysMenuService.list(sysMenuPageParam);
//        return Result.build(tree);
//    }
}
