package com.sykj.seaflow.sys.modual.positionuser.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sykj.seaflow.sys.modual.positionuser.entity.SysPositionUser;
import com.sykj.seaflow.sys.modual.positionuser.params.SysPositionUserQueryParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PositionUserService extends IService<SysPositionUser> {



    public List<SysPositionUser> list(SysPositionUserQueryParam sysPositionUserQueryParam);

    /**
     * 重新保存
     * @param positionList
     */
    void resave(Long userId, List<SysPositionUser> positionList);


    public List<Long> getUsersByDeptIds(List<Long> deptIds);
}
