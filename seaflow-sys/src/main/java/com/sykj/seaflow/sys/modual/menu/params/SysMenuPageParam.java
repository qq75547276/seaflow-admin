package com.sykj.seaflow.sys.modual.menu.params;

import lombok.Data;

@Data
public class SysMenuPageParam {

    private String type;

    private String name;
}
