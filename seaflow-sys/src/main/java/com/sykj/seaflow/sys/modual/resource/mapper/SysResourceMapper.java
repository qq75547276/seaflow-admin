package com.sykj.seaflow.sys.modual.resource.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sykj.seaflow.sys.modual.position.entity.SysPosition;
import com.sykj.seaflow.sys.modual.resource.entity.SysResource;

public interface SysResourceMapper extends BaseMapper<SysResource> {

}
