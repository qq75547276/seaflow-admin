package com.sykj.seaflow.sys.modual.menu.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sykj.seaflow.common.base.entity.BaseEntity;
import com.sykj.seaflow.sys.modual.identifier.entity.SysIdentifier;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Setter
@Getter
@TableName("sys_menu")
public class SysMenu extends BaseEntity {
    /**
     * 菜单名称
     */
    private String name;
    /**
     * 菜单类型
     */
    private String type;
    /**
     * 父id
     */
    private Long parentId;
    /**
     * 排序
     */
    private Integer sortNum;
    /**
     * 路由地址， 可以理解为路由时组件name
     */
    private String path;

    /**
     * 组件地址  全部再src/views/ 下
     */
    private String component;
    /**
     * 参数
     */
    private String params;
    /**
     * 图标
     */
    private String icon;
    /**
     * 显示 隐藏 状态
     */
    private String status;

    @TableField(exist = false)
    private List<SysIdentifier> identifierList;

}
