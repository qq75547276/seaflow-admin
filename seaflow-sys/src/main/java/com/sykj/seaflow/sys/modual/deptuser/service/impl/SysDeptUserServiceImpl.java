package com.sykj.seaflow.sys.modual.deptuser.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sykj.seaflow.sys.modual.deptuser.entity.SysDeptUser;
import com.sykj.seaflow.sys.modual.deptuser.mapper.SysDeptUserMapper;
import com.sykj.seaflow.sys.modual.deptuser.service.SysDeptUserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysDeptUserServiceImpl extends ServiceImpl<SysDeptUserMapper, SysDeptUser> implements SysDeptUserService {


    @Override
    public List<SysDeptUser> listByDeptId(Long deptId) {
        if (ObjectUtil.isEmpty(deptId)) {
            return new ArrayList<>();
        }
        return  this.list(Wrappers.lambdaQuery(SysDeptUser.class).eq(SysDeptUser::getDeptId, deptId));
    }

    @Override
    public List<SysDeptUser> listbyDeptIds(List<String> deptIds) {
        if(CollectionUtil.isEmpty(deptIds)){
            return new ArrayList<>();
        }
        return this.list(Wrappers.lambdaQuery(SysDeptUser.class).in(SysDeptUser::getDeptId, deptIds));
    }
}
