package com.sykj.seaflow.sys.modual.role.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.sys.modual.role.entity.SysRole;
import com.sykj.seaflow.sys.modual.role.params.*;

import java.util.List;

public interface SysRoleService extends IService<SysRole> {

    /**
     * 新增
     */
    void add(SysRoleAddParam sysRoleAddParam);

    /**
     * 修改
     */
    void edit(SysRoleEditParam sysRoleEditParam);

    /**
     * 删除
     */
    void delete(List<SysRoleIdParam> ids);

    /**
     * 停用
     */
    void disable(List<SysRoleIdParam> ids);

    /**
     * 启用
     */
    void enable(List<SysRoleIdParam> ids);

    /**
     * 分页查询
     */
    IPage<SysRole> page(SysRoleQueryParam sysRoleQueryParam, PageQuery<SysRole> pageQuery);


    public void grantUser(List<SysRoleGrantUserParam> roleGrantUserParams);

    public void revokeUser(List<SysRoleRevokeUserParam> revokeUserParams);

    /**
     * 角色下的 用户 ids
     * @param roleIdParam
     * @return
     */
    List<Long> roleUserList(SysRoleIdParam roleIdParam);

    List<SysRole> list(SysRoleQueryParam roleQueryParam);

    List<SysRole>  myRoles();
}
