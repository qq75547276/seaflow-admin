package com.sykj.seaflow.sys.modual.roleuser.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.sykj.seaflow.sys.modual.roleuser.entity.SysRoleUser;
import com.sykj.seaflow.sys.modual.user.entity.SysUser;

import java.util.List;

public interface SysRoleUserMapper extends BaseMapper<SysRoleUser>{
    List<SysUser> page(Page page, @Param(Constants.WRAPPER) QueryWrapper<SysUser> query);


    List<SysUser> canGrantPage(Page page, @Param(Constants.WRAPPER) QueryWrapper<SysUser> query);
}
