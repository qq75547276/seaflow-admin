package com.sykj.seaflow.sys.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.stream.CollectorUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sykj.seaflow.sys.modual.dept.service.SysDeptService;
import com.sykj.seaflow.sys.modual.positionuser.entity.SysPositionUser;
import com.sykj.seaflow.sys.modual.positionuser.service.PositionUserService;
import com.sykj.seaflow.sys.remote.bean.LoginUserInfo;
import lombok.AllArgsConstructor;
import com.sykj.seaflow.sys.modual.roleuser.service.SysRoleUserService;
import com.sykj.seaflow.sys.modual.user.entity.SysUser;
import com.sykj.seaflow.sys.modual.user.service.SysUserService;
import com.sykj.seaflow.sys.remote.api.SysUserApi;
import com.sykj.seaflow.sys.remote.bean.SysUserBean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Component
public class SysUserApiProvider implements SysUserApi {
    private final SysRoleUserService roleUserService;
    private final SysUserService sysUserService;
    private final PositionUserService positionUserService;

    @Override
    public List<Long> getUserIdsByRole(List<String> roleIds) {
        return  roleUserService.listbyRoleIds(roleIds);
    }

    @Override
    public List<Long> getUserIdsByRole(String roleCode) {
        return ListUtil.empty();
    }

    @Override
    public List<Long> getUserIdsByDept(List<String> deptIds) {
        if(ObjectUtil.isEmpty(deptIds)) {
            return ListUtil.empty();
        }
        List<Long> list = deptIds.stream().map(Long::valueOf).collect(Collectors.toList());
        return positionUserService.getUsersByDeptIds(list);
    }

    @Override
    public List<SysUserBean> getUsers(List<Long> userIds) {
        List<SysUser> sysUsers = sysUserService.listByIds(userIds);
        return  BeanUtil.copyToList(sysUsers,SysUserBean.class);
    }

    @Override
    public LoginUserInfo getUserByUserName(String username) {
        SysUser one = sysUserService.getOne(Wrappers.<SysUser>lambdaQuery().eq(SysUser::getUsername, username));
        return BeanUtil.toBean(one, LoginUserInfo.class);
    }

    @Override
    public List<SysUserBean> selectUWithOutUsers(List<String> userIds) {
        return sysUserService.selectUWithOutUsers(userIds);
    }
}
