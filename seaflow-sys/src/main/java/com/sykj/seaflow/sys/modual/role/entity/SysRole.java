package com.sykj.seaflow.sys.modual.role.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import com.sykj.seaflow.common.base.entity.BaseEntity;

@Setter
@Getter
@TableName("sys_role")
public class SysRole extends BaseEntity {
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色编码
     */
    private String roleCode;
    /**
     * 角色描述
     */
    private String roleDesc;
    /**
     * 排序
     */
    private Integer sortNum;
    /**
     * 角色状态
     */
    private String roleStatus;

    /**
     * 是否允许删除
     */
    private String deleted;


}
