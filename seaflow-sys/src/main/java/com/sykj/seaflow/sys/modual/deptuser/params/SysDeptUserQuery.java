package com.sykj.seaflow.sys.modual.deptuser.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysDeptUserQuery {
    private Long deptId;

    private String username;
    private String nick;
    private String phone;
}
