package com.sykj.seaflow.sys.modual.dept.controller;


import cn.hutool.core.lang.tree.Tree;
import com.sykj.seaflow.common.base.bean.Result;
import com.sykj.seaflow.sys.modual.dept.entity.SysDept;
import com.sykj.seaflow.sys.modual.dept.params.*;
import com.sykj.seaflow.sys.modual.dept.service.SysDeptService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/sys/dept")
public class SysDeptController {
    private SysDeptService sysDeptService;

    /**
     * 新增
     */
    @PostMapping("/add")
    public Result<Void> add(@RequestBody @Valid SysDeptAddParam sysDeptAddParam) {
        sysDeptService.add(sysDeptAddParam);
        return Result.build();
    }

    @PostMapping("/edit")
    public Result<Void> edit(@RequestBody @Valid SysDeptEditParam sysDeptEditParam) {
        sysDeptService.edit(sysDeptEditParam);
        return Result.build();
    }
    @GetMapping("/detail")
    public Result<SysDept> detail(@Valid SysDeptIdParam sysDeptIdParam) {
        return Result.build(sysDeptService.detail(sysDeptIdParam));
    }

    @GetMapping("/list")
    public Result<SysDept> list(@Valid SysDeptQueryParam sysDeptQueryParam) {
        return Result.build(sysDeptService.list(sysDeptQueryParam));
    }

    @PostMapping("/delete")
    public Result<Void> delete(@RequestBody @Valid List<SysDeptIdParam> sysDeptIdParams) {
        sysDeptService.deletes(sysDeptIdParams);
        return Result.build();
    }

    /**
     * 菜单列表
     * @param
     * @return
     */
    @GetMapping("/tree")
    public Result<Tree<String>> tree(SysDeptPageParam sysDeptPageParam) {
        List<Tree<String>> tree = sysDeptService.tree(sysDeptPageParam);
        return Result.build(tree);
    }

    /**
     * 用户 可以使用的路由 ，先直接返回
     * @param
     * @return
     */
    @GetMapping("/menuRouter")
    public Result<Tree<String>> menuRouter(SysDeptPageParam sysDeptPageParam) {
        List<Tree<String>> tree = sysDeptService.tree(sysDeptPageParam);
        return Result.build(tree);
    }


//    @GetMapping("/list")
//    public Result<SysMenu> list(SysMenuQueryParam sysMenuPageParam) {
//        List<Tree<String>> tree = sysMenuService.list(sysMenuPageParam);
//        return Result.build(tree);
//    }
}
