package com.sykj.seaflow.sys.modual.position.params;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SysPositionIdParam {

    @NotNull(message = "id 不能为空")
    private Long id;
}
