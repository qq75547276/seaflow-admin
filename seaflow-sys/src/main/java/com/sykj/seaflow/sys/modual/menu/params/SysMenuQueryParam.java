package com.sykj.seaflow.sys.modual.menu.params;

import lombok.Data;

@Data
public class SysMenuQueryParam {

    private String type;

    private String name;
}
