package com.sykj.seaflow.sys.api;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fhs.trans.service.impl.TransService;
import com.sykj.seaflow.common.base.util.LoginHelper;
import com.sykj.seaflow.sys.modual.dept.entity.SysDept;
import com.sykj.seaflow.sys.modual.dept.service.SysDeptService;
import com.sykj.seaflow.sys.modual.identifier.entity.SysIdentifier;
import com.sykj.seaflow.sys.modual.identifier.enums.IdentifierType;
import com.sykj.seaflow.sys.modual.identifier.params.SysIdentifierQueryParam;
import com.sykj.seaflow.sys.modual.identifier.service.SysIdentifierService;
import com.sykj.seaflow.sys.modual.position.entity.SysPosition;
import com.sykj.seaflow.sys.modual.position.service.SysPositionService;
import com.sykj.seaflow.sys.modual.positionuser.entity.SysPositionUser;
import com.sykj.seaflow.sys.modual.positionuser.service.PositionUserService;
import com.sykj.seaflow.sys.modual.resource.entity.SysResource;
import com.sykj.seaflow.sys.modual.resource.enums.FromType;
import com.sykj.seaflow.sys.modual.resource.enums.ResourceType;
import com.sykj.seaflow.sys.modual.resource.service.SysResourceService;
import com.sykj.seaflow.sys.modual.role.entity.SysRole;
import com.sykj.seaflow.sys.modual.role.service.SysRoleService;
import com.sykj.seaflow.sys.modual.user.entity.SysUser;
import com.sykj.seaflow.sys.modual.user.service.SysUserService;
import com.sykj.seaflow.sys.remote.api.SysPermissionApi;
import com.sykj.seaflow.sys.remote.bean.LoginUserInfo;
import com.sykj.seaflow.sys.remote.bean.LoginUserPosition;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor
@Component
public class SysPermissionApiProvider implements SysPermissionApi {
    private final SysDeptService sysDeptService;
    private final SysPositionService positionService;
    // 用户职位关联表
    private final PositionUserService positionUserService;
    private final SysUserService userService;
    private final SysRoleService roleService;

    private final SysIdentifierService identifierService;

    private final SysResourceService sysResourceService;
    private final TransService transService;
    // 获取权限信息
    @Override
    public LoginUserInfo loadPermission() {
        LoginUserInfo loginUserInfo = new LoginUserInfo();
        SysUser user = userService.getById(LoginHelper.getUserId());
        loginUserInfo.setNick(user.getNick());
        loginUserInfo.setUsername(user.getUsername());
        loginUserInfo.setId(user.getId());
        loginUserInfo.setEmail(user.getEmail());
        // 获取角色
        List<SysRole> sysRoles = roleService.myRoles();
        loginUserInfo.setRoleList(CollStreamUtil.toList(sysRoles,SysRole::getRoleCode));
        // 获取关联职位信息
        List<SysPositionUser> positionUserList = positionUserService.list(Wrappers.<SysPositionUser>lambdaQuery().eq(SysPositionUser::getUserId, LoginHelper.getUserId()));
        List<Long> positionIds = new ArrayList<>();
        if(ObjectUtil.isNotEmpty(positionUserList)){
            transService.transMore(positionUserList);
            List<LoginUserPosition>  userPositions = new ArrayList<>();
            List<Long> deptIds = new ArrayList<>();
            positionUserList.forEach(r-> {
                // 职位id
                positionIds.add(r.getPositionId());
                LoginUserPosition loginUserPosition = BeanUtil.copyProperties(r, LoginUserPosition.class);
                userPositions.add(loginUserPosition);
                deptIds.add(loginUserPosition.getDeptId());
            });
            loginUserInfo.setPosList(positionIds);
            loginUserInfo.setUserPositions(userPositions);
            loginUserInfo.setDeptList(deptIds);
        }

        // 获取 权限标识 ， 按钮权限和 接口权限
        List<SysResource> resources =  sysResourceService.myResource(CollStreamUtil.toList(sysRoles,SysRole::getId), positionIds, LoginHelper.getUserId());
        if(ObjectUtil.isNotEmpty(resources)){
            List<Long> list = CollStreamUtil.toList(resources, SysResource::getResourceId);
            List<SysIdentifier> identifiers = identifierService.listByIds(list);
            Map<String, List<SysIdentifier>> stringListMap = CollStreamUtil.groupByKey(identifiers, SysIdentifier::getType);
            loginUserInfo.setBtnList(CollStreamUtil.toList(stringListMap.get(IdentifierType.btn.getValue()), SysIdentifier::getCode));
            //TODO
            // 暂时不合适，考虑换种方式
            loginUserInfo.setPermissionList(CollStreamUtil.toList(stringListMap.get(IdentifierType.permission.getValue()), SysIdentifier::getCode));

        }
        return loginUserInfo;
    }

}
