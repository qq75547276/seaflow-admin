package com.sykj.seaflow.sys.modual.position.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.constant.TransType;
import com.fhs.core.trans.vo.TransPojo;
import com.sykj.seaflow.common.base.entity.BaseEntity;
import com.sykj.seaflow.sys.modual.dept.entity.SysDept;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@TableName(value = "sys_position", autoResultMap = true)
public class SysPosition extends BaseEntity implements TransPojo, Serializable {
    /**
     * 部门id
     */
    @Trans(type = TransType.SIMPLE,  target = SysDept.class,fields = "name", ref = "deptName" )
    private Long deptId;

    @TableField(exist = false)
    private String deptName;
    /**
     * 职位名称
     */
    private String positionName;

    /**
     * 排序号
     */
    private int sortNum;



}
