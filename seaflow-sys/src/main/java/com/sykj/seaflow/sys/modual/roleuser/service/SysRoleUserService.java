package com.sykj.seaflow.sys.modual.roleuser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sykj.seaflow.common.base.bean.PageQuery;
import com.sykj.seaflow.sys.modual.role.params.SysRoleGrantUserBatchParam;
import com.sykj.seaflow.sys.modual.role.params.SysRoleGrantUserParam;
import com.sykj.seaflow.sys.modual.role.params.SysRoleRevokeUserParam;
import com.sykj.seaflow.sys.modual.roleuser.entity.SysRoleUser;
import com.sykj.seaflow.sys.modual.roleuser.params.SysRoleUserIdParam;
import com.sykj.seaflow.sys.modual.roleuser.params.SysRoleUserQuery;
import com.sykj.seaflow.sys.modual.user.entity.SysUser;

import java.util.List;

public interface SysRoleUserService extends IService<SysRoleUser> {

    /**
     * 新增
     */
    void batchAdd(List<SysRoleGrantUserParam> grantUserParams);

    /**
     * 删除
     */
    void delete(List<SysRoleRevokeUserParam> revokeUserParams);

    /**
     * 根据用户id 删除关键角色
     * @param ids
     */
    void delByUserIds(List<Long> ids);

    /**
     * 根据角色id 删除关联用户
     * @param ids
     */
    void  delByRoleIds(List<Long> ids);

    List<SysRoleUser> listByRoleId(Long id);

    /**
     * 批量授权， 必须时同一个角色id
     * @param grantUserParams
     */
    void batchGrant(SysRoleGrantUserBatchParam grantUserParams);


    public List<SysUser> grantedGpage(SysRoleUserQuery sysRoleUserQuery, PageQuery pageQuery);

    public List<SysUser> canGrantPage(SysRoleUserQuery sysRoleUserQuery, PageQuery pageQuery);

    boolean revoke(List<SysRoleUserIdParam> roleUserIdParams);

    public List<Long> listbyRoleIds(List<String> roleIds);

    public List<SysRoleUser> listByUserId(Long userId);
}
